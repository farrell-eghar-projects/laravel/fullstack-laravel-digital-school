<?php

namespace App\Http\Livewire\Dashboard\Creator\ManageCourse\Course;

use App\Models\Course;
use Livewire\Component;
use App\Models\CoursePrerequisite;
use App\Models\CourseSuitableTarget;

class FormCreate extends Component
{
	public Course $course;
	public $materials;
	public $prerequisites;
  public $suitableTargets;
	public $studentTargets;
  public $counter = 0;
	
	protected $rules = [
    'course.type_id' => 'required|integer',
    'course.material_id' => 'required|integer',
    'course.title' => 'required|string',
    'course.code' => 'required|string',
		'course.password' => 'required|string',
    'course.description' => 'required|string',
    'course.student_targets' => 'required|string',
		'course.thumbnail' => 'required|string',
		'course.images' => 'required|string',
		'course.started_at' => 'required|date',
		'course.finished_at' => 'required|date',
		'prerequisites.*.prerequisite' => 'required|string',
    'suitableTargets.*.target' => 'required|string'
    ];
	
	protected $listeners = [
    'setImage' => 'setImage',
    'setContent' => 'setContent',
		'contentUpdate' => 'contentUpdate',
		'imageUpdate' => 'imageUpdate',
		'refreshComponent' => '$refresh'
	];
	
	public function mount()
	{
		$this->materials = auth()->user()->materials;
		$this->prerequisites = $this->course->coursePrerequisites;
		$this->suitableTargets = $this->course->courseSuitableTargets;
    $this->studentTargets = $this->course->student_targets;
	}

  public function updatedCourseTitle()
  {
    $this->course->save();
  }

  public function updatedCourseCode()
  {
    $this->course->save();
  }
  
  public function updatedCoursePassword()
  {
    $this->course->save();
  }

  public function updatedPrerequisites($value, $key)
  {
    $this->prerequisites[explode('.', $key)[0]]->save();
  }

  public function updatedSuitableTargets($value, $key)
  {
    $this->suitableTargets[explode('.', $key)[0]]->save();
  }

  public function updatedCourseTypeId()
  {
    $this->course->save();
  }

  public function updatedCourseStartedAt()
  {
    $this->course->save();
  }

  public function updatedCourseFinishedAt()
  {
    $this->course->save();
  }

  public function updatedCourseMaterialId()
  {
    $this->course->save();
  }

	public function setImage($part)
	{
    $this->counter++;
		$part ? $this->dispatchBrowserEvent('fetch-image', ['thumbnail' => $this->counter]) : $this->dispatchBrowserEvent('fetch-image', ['images' => $this->counter]);
	}

	public function setContent()
	{
		$this->dispatchBrowserEvent('fetch-content', ['content' => $this->course->description]);
	}

	public function contentUpdate($description)
	{
		$this->course->description = $description;
    $this->course->save();
	}
	
	public function refreshSuitableTargets()
	{
		$this->course->refresh();
		$this->suitableTargets = $this->course->courseSuitableTargets;
	}
	
	public function addSuitableTarget()
	{
		CourseSuitableTarget::create([
			'course_id' => $this->course->id
		]);
		
		$this->refreshSuitableTargets();
	}
	
	public function deleteSuitableTarget(CourseSuitableTarget $suitableTarget)
	{
		$suitableTarget->delete();
		$this->refreshSuitableTargets();
	}

  public function refreshPrerequisites()
	{
		$this->course->refresh();
		$this->prerequisites = $this->course->coursePrerequisites;
	}

	public function addPrerequisite()
	{
		CoursePrerequisite::create([
			'course_id' => $this->course->id
		]);
		
		$this->refreshPrerequisites();
	}
	
	public function deletePrerequisite(CoursePrerequisite $prerequisite)
	{
		$prerequisite->delete();
		$this->refreshPrerequisites();
	}
	
	public function updateThumbnail($filePath)
	{
		$this->course->thumbnail = $filePath;
		$this->course->save();
		$this->course->refresh();
	}

  public function imageUpdate($filePath)
	{
		$filePath[0] ? $this->updateThumbnail($filePath[1]) : $this->updateImages($filePath[1]);
	}
	
	public function updateImages($filePath)
	{
		$this->course->images = $filePath;
		$this->course->save();
		$this->course->refresh();
	}
	
	public function publishCourse()
  {
    $this->course->student_targets = $this->studentTargets;

    $this->validate();
    $this->course->published = true;
    $this->course->save();
    foreach ($this->course->material->assignments as $index => $assignment) {
      $this->course->courseAssignments()->create([
        'assignment_id' => $assignment->id
      ]);
    }
    return redirect()->route('creator.manage-courses.index');
  }

  public function deleteCourse()
  {
    $this->course->delete();
    return redirect()->route('creator.manage-courses.index');
  }
	
    public function render()
    {
        return view('livewire.dashboard.creator.manage-course.course.form-create');
    }
}
