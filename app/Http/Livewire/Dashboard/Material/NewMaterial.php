<?php

namespace App\Http\Livewire\Dashboard\Material;

use App\Models\Material;
use Illuminate\Validation\Rule;
use Livewire\Component;

class NewMaterial extends Component
{
	public $name;
	
	protected function rules()
    {
        return [
            'name' => ['required', Rule::unique('materials')->where(fn ($query) => $query->where('user_id', auth()->id()))]
        ];
    }
	
	public function save()
	{
		$this->validate();
		
		$material = Material::create([
			'user_id' => auth()->id(),
			'name' => $this->name
		]);
		
		return redirect()->route('creator.manage-courses.material.show', ['material' => $material]);
	}
	
    public function render()
    {
        return view('livewire.dashboard.material.new-material');
    }
}
