<header class="fixed top-0 left-0 w-full flex items-center bg-slate-100 z-10">
	<div class="container">
		<div class="flex justify-between items-center p-4 h-16">
			<div class="block lg:hidden">
				<button type="button" id="hamburger" name="hamburger" class="block">
					<span class="block bg-secondary w-6 h-0.5 my-2 origin-top-left"></span>
					<span class="block bg-secondary w-6 h-0.5 my-2 scale-x-75 origin-left"></span>
					<span class="block bg-secondary w-6 h-0.5 my-2 scale-x-50 origin-bottom-left"></span>
				</button>
				
				<nav id="nav-sm" class="hidden fixed top-0 left-0 h-full w-full bg-transparent backdrop-blur-sm animate-fadeIn z-10">
					<div class="relative bg-white h-full w-3/4 z-20 animate-slideInFromLeft sm:w-2/5 md:w-1/3">
						<div class="flex items-center justify-between p-4">
							<a href="#" class="flex shrink-0 space-x-3 items-center">
								<img src="{{ asset('assets/logo.png') }}" alt="Logo" class="h-12 w-12">
								<div class="font-logo items-center">
									<p class="font-bold text-primary text-lg leading-tight">Serpihan</p>
									<p class="font-medium text-base leading-tight text-secondary">Digital School</p>
								</div>
							</a>
							
							<button type="button" id="hamburger-close" name="hamburger_close" class="block">
								<span class="block bg-secondary w-[29px] h-0.5 my-2 transition duration-75 origin-top-left rotate-45"></span>
								<span class="block bg-secondary w-[29px] h-0.5 my-2 transition duration-75 scale-0"></span>
								<span class="block bg-secondary w-[29px] h-0.5 my-2 transition duration-75 origin-bottom-left -rotate-45"></span>
							</button>
						</div>
						
						<div class="block p-1 divide-y h-3/4 overflow-y-auto scrollbar">
							<div class="block space-y-2 p-3">
								<span class="block font-semibold text-xs text-dark">Student Menu</span>
								
								{{--
								<a href="{{ route('student.dashboard.index') }}" class="flex items-center space-x-4 text-left rounded-lg p-3  {{ request()->routeIs('student.dashboard.*') ? 'text-white bg-primary' : 'text-secondary hover:text-primary active:text-white active:bg-primary' }}">
									<span class="material-symbols-outlined">
										dashboard
									</span>
									
									<span class="font-medium">
										Dashboard
									</span>
								</a>
								 --}}
								
								<a href="{{ route('student.my-courses.index') }}" class="flex items-center space-x-4 text-left rounded-lg p-3 {{ request()->routeIs('student.my-courses.*') ? 'text-white bg-primary' : 'text-secondary hover:text-primary active:text-white active:bg-primary' }}">
									<span class="material-symbols-outlined">
										assignment
									</span>
									
									<span class="font-medium">
										My Courses
									</span>
								</a>
								
								<a href="{{ route('student.all-courses.index') }}" class="flex items-center space-x-4 text-left rounded-lg p-3 {{ request()->routeIs('student.all-courses.*') ? 'text-white bg-primary' : 'text-secondary hover:text-primary active:text-white active:bg-primary' }}">
									<span class="material-symbols-outlined">
										format_list_bulleted
									</span>
									
									<span class="font-medium">
										All Courses
									</span>
								</a>
							</div>
							
							<div class="block space-y-2 p-3">
								<span class="block font-semibold text-xs text-dark">Creator Menu</span>
								{{--
								<a href="{{ route('creator.dashboard.index') }}" class="flex items-center space-x-4 text-left rounded-lg p-3 {{ request()->routeIs('creator.dashboard.*') ? 'text-white bg-primary' : 'text-secondary hover:text-primary active:text-white active:bg-primary' }}">
									<span class="material-symbols-outlined">
										dashboard
									</span>
									
									<span class="font-medium">
										Dashboard
									</span>
								</a>
								--}}
								
								<a href="{{ route('creator.manage-courses.index') }}" class="flex items-center space-x-4 text-left rounded-lg p-3 {{ request()->routeIs('creator.manage-courses.*') ? 'text-white bg-primary' : 'text-secondary hover:text-primary active:text-white active:bg-primary' }}">
									<span class="material-symbols-outlined">
										dynamic_feed
									</span>
									
									<span class="font-medium">
										Manage Courses
									</span>
								</a>
								
								{{--
								<a href="{{ route('creator.revenue.index') }}" class="flex items-center space-x-4 text-left rounded-lg p-3 {{ request()->routeIs('creator.revenue.*') ? 'text-white bg-primary' : 'text-secondary hover:text-primary active:text-white active:bg-primary' }}">
									<span class="material-symbols-outlined">
										paid
									</span>
									
									<span class="font-medium">
										Revenue
									</span>
								</a>
								--}}
							</div>
							
							{{--
							<div class="block space-y-2 p-3">
								<span class="block font-semibold text-xs text-dark">Admin</span>
								
								<a href="{{ route('admin.user-control.index') }}" class="flex items-center space-x-4 text-left rounded-lg p-3 {{ request()->routeIs('admin.user-control.*') ? 'text-white bg-primary' : 'text-secondary hover:text-primary active:text-white active:bg-primary' }}">
									<span class="material-symbols-outlined">
										manage_accounts
									</span>
									
									<span class="font-medium">
										User Control
									</span>
								</a>
								
								<a href="{{ route('admin.transaction-history.index') }}" class="flex items-center space-x-4 text-left rounded-lg p-3 {{ request()->routeIs('admin.transaction-history.*') ? 'text-white bg-primary' : 'text-secondary hover:text-primary active:text-white active:bg-primary' }}">
									<span class="material-symbols-outlined">
										timeline
									</span>
									
									<span class="font-medium">
										Transaction History
									</span>
								</a>
							</div>
							--}}
							
						</div>
						
						<div class="absolute bottom-5 left-0 right-0 w-full px-6">
							<form action="{{ route('logout') }}" method="POST" class="block py-1 bg-red-500 rounded-lg font-medium text-center text-lg text-white">
								@csrf
								<input type="submit" value="Logout">
							</form>
						</div>
					</div>
				</nav>
			</div>
			
			{{-- Navigation Desktop Start --}}
			<div class="hidden lg:flex">
				<a href="#" class="group flex shrink-0 space-x-3 items-center">
					<img src="{{ asset('assets/logo.png') }}" alt="Logo" class="h-12 w-12 grayscale group-hover:grayscale-0">
					<div class="font-logo items-center">
						<p class="font-bold text-dark text-lg leading-tight group-hover:text-primary">Serpihan</p>
						<p class="font-medium text-base leading-tight text-secondary">Digital School</p>
					</div>
				</a>
			</div>
			
			<div class="hidden flex-nowrap items-center relative w-full h-full lg:flex">
				<div id="student-mode" class="{{ request()->routeIs('student.*') ? 'flex' : 'hidden'}} absolute left-1/2 -translate-x-1/2 items-center justify-center space-x-3">
					{{--
					<a href="{{ route('student.dashboard.index') }}" class="p-3 whitespace-nowrap decoration-[3px] text-base text-secondary underline-offset-[12px] decoration-primary {{ request()->routeIs('student.dashboard.*') ? 'text-dark underline' : 'hover:text-primary active:text-dark active:underline' }}">Dashboard</a>
					--}}
					<a href="{{ route('student.my-courses.index') }}" class="p-3 whitespace-nowrap decoration-[3px] text-base text-secondary underline-offset-[12px] decoration-primary {{ request()->routeIs('student.my-courses.*') ? 'text-dark underline' : 'hover:text-primary active:text-dark active:underline' }}">My Courses</a>
					<a href="{{ route('student.all-courses.index') }}" class="p-3 whitespace-nowrap decoration-[3px] text-base text-secondary underline-offset-[12px] decoration-primary {{ request()->routeIs('student.all-courses.*') ? 'text-dark underline' : 'hover:text-primary active:text-dark active:underline' }}">All Courses</a>
				</div>
				
				<div id="creator-mode" class="{{ request()->routeIs('creator.*') ? 'flex' : 'hidden'}} absolute left-1/2 -translate-x-1/2 items-center justify-center space-x-3">
					{{--
					<a href="{{ route('creator.dashboard.index') }}" class="p-3 whitespace-nowrap decoration-[3px] text-base text-secondary underline-offset-[12px] decoration-primary {{ request()->routeIs('creator.dashboard.*') ? 'text-dark underline' : 'hover:text-primary active:text-dark active:underline' }}">Dashboard</a>
					--}}
					<a href="{{ route('creator.manage-courses.index') }}" class="p-3 whitespace-nowrap decoration-[3px] text-base text-secondary underline-offset-[12px] decoration-primary {{ request()->routeIs('creator.manage-courses.*') ? 'text-dark underline' : 'hover:text-primary active:text-dark active:underline' }}">Manage Courses</a>
					{{--
					<a href="{{ route('creator.revenue.index') }}" class="p-3 whitespace-nowrap decoration-[3px] text-base text-secondary underline-offset-[12px] decoration-primary {{ request()->routeIs('creator.revenue.*') ? 'text-dark underline' : 'hover:text-primary active:text-dark active:underline' }}">Revenue</a>
					--}}
				</div>
			</div>
			{{-- Navigation Desktop End --}}
			
			<div class="flex items-center justify-center space-x-3">
				<button type="button" id="switch-mode" name="switch_mode" class="hidden items-center justify-center w-8 h-8 rounded-full lg:flex active:bg-slate-300">
					<span class="material-symbols-outlined w-full text-dark">
						sync
					</span>
				</button>
				
				<button type="button" id="notification" name="notification" class="flex items-center justify-center relative w-8 h-8 rounded-full active:bg-slate-300">
					<span class="material-symbols-outlined w-full text-dark pointer-events-none">
						notifications
					</span>
					
					<span class="absolute right-1 top-1 text-[8px] w-3 h-3 bg-red-500 rounded-full pointer-events-none">
						<p class="text-white text-center">12</p>
					</span>
					
					<div id="notification-menu" name="notification_menu" class="hidden absolute -right-8 top-full mt-3 w-48 rounded-lg bg-white shadow-md divide-y cursor-default md:w-64">
						<a href="#" class="block text-left w-full px-2 md:px-3 hover:bg-slate-200">
							<div class="flex items-center space-x-1 text-secondary">
								<span class="material-symbols-outlined flex items-center justify-center w-12 h-12 rounded-full">
									person
								</span>
								
								<span class="block w-full truncate">
									<span class="font-semibold text-xs text-dark">Bellemond </span>
									<span class="font-semibold text-xs"> joined your course</span>
									<p class="font-medium text-[9px] text-slate-400">36 Minutes ago</p>
								</span>
							</div>
						</a>
						
						<a href="#" class="block text-left w-full px-2 md:px-3 hover:bg-slate-200">
							<div class="flex items-center space-x-1 text-secondary">
								<span class="material-symbols-outlined flex items-center justify-center w-12 h-12 rounded-full">
									person
								</span>
								
								<span class="block w-full truncate">
									<span class="font-semibold text-xs text-dark">Bellemond </span>
									<span class="font-semibold text-xs"> joined your course</span>
									<p class="font-medium text-[9px] text-slate-400">36 Minutes ago</p>
								</span>
							</div>
						</a>
						
						<a href="#" class="block text-left w-full px-2 md:px-3 hover:bg-slate-200">
							<div class="flex items-center space-x-1 text-secondary">
								<span class="material-symbols-outlined flex items-center justify-center w-12 h-12 rounded-full">
									person
								</span>
								
								<span class="block w-full truncate">
									<span class="font-semibold text-xs text-dark">Bellemond </span>
									<span class="font-semibold text-xs"> joined your course</span>
									<p class="font-medium text-[9px] text-slate-400">36 Minutes ago</p>
								</span>
							</div>
						</a>
					</div>
				</button>
				
				<div id="avatar" name="avatar" class="flex items-center justify-center relative w-8 h-8 rounded-full cursor-pointer active:ring active:ring-slate-400">
					<img src="{{ url('https://randomuser.me/api/portraits/men/62.jpg') }}" alt="User Profile" class="w-8 h-8 pointer-events-none rounded-full">
					<span class="absolute right-0 bottom-0 w-[10px] h-[10px] rounded-full bg-green-500 outline outline-2 outline-slate-600 opacity-90 pointer-events-none"></span>
					
					<div id="avatar-menu" name="avatar_menu" class="hidden absolute right-0 top-full mt-3 px-3 w-40 rounded-lg bg-white shadow-md divide-y cursor-default md:w-56 md:px-6">
						<div class="block py-2">
							@auth
								<h4 class="font-semibold text-md text-left text-dark truncate">{{ auth()->user()->name }}</h4>
								<p class="font-medium text-xs text-left text-secondary truncate">{{ auth()->user()->email }}</p>
							@endauth
							
							@guest
								<h4 class="font-semibold text-md text-left text-dark truncate">Mohammed Farrell Eghar Syahputra</h4>
								<p class="font-medium text-xs text-left text-secondary truncate">farrell.eghar@serpihan.id</p>
							@endguest
						</div>
						
						<div class="block space-y-1 text-left py-2">
							{{--
							<a href="#" class="flex items-center space-x-2 text-secondary hover:text-primary">
								<span class="material-symbols-outlined text-sm">
									person
								</span>
								
								<span class="font-medium text-sm">
									My Profile
								</span>
							</a>
							--}}
							
							<a href="#" class="flex items-center space-x-2 text-secondary hover:text-primary">
								<span class="material-symbols-outlined text-sm">
									settings
								</span>
								
								<span class="font-medium text-sm">
									Settings
								</span>
							</a>
							
							<form action="{{ route('logout') }}" method="POST" class="flex items-center">
								@csrf
								<button type="submit" class="flex items-center space-x-2 text-secondary hover:text-primary">
									<span class="material-symbols-outlined text-sm">
										logout
									</span>
									
									<span class="font-medium text-sm">
										Logout
									</span>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>