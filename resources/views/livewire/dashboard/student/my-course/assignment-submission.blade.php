<div class="block bg-white min-h-full mt-2 mb-16 px-8 py-4 rounded-md shadow-lg">
  {{-- Header Page Start --}}
  <div class="block space-y-8">
    <h3 class="font-semibold text-xl text-dark text-center p-4 max-w-xl mx-auto lg:text-2xl">{{ $assignment->name }}</h3>
    
    <div class="grid grid-cols-1 gap-4 sm:grid-cols-2 sm:place-content-evenly sm:place-items-center">
      <div class="w-40 flex flex-nowrap items-center space-x-2 sm:order-1">
        <h4 class="font-medium text-sm text-dark">Questions</h4>
        <span class="flex flex-nowrap items-center space-x-1">
          <h5 class="text-sm text-secondary">:</h5>
          <h5 class="text-sm text-secondary">{{ count($assignment->assignmentQuestions) }}</h5>
        </span>
      </div>
      
      <div class="w-40 flex flex-nowrap items-center space-x-2 sm:order-3">
        <h4 class="font-medium text-sm text-dark">Points</h4>
        <span class="flex flex-nowrap items-center space-x-1">
          <h5 class="text-sm text-secondary">:</h5>
          <h5 class="text-sm text-secondary">{{ $courseAssignment->score }}%</h5>
        </span>
      </div>
      
      <div class="w-40 flex flex-nowrap items-center space-x-2 sm:order-2">
        <h4 class="font-medium text-sm text-dark">Started at</h4>
        <span class="flex flex-nowrap items-center space-x-1">
          <h5 class="text-sm text-secondary">:</h5>
          <h5 class="text-sm text-secondary">{{ explode(' ', $assignmentSubmission->created_at)[1] }}</h5>
        </span>
      </div>
      
      <div class="w-40 flex flex-nowrap items-center space-x-2 sm:order-4">
        <h4 class="font-medium text-sm text-dark">Finished at</h4>
        <span class="flex flex-nowrap items-center space-x-1">
          <h5 class="text-sm text-secondary">:</h5>
          <h5 class="text-sm text-secondary">{{ explode(' ', $assignmentSubmission->duration)[1] }}</h5>
        </span>
      </div>
    </div>
    
    <div class="block w-full mx-auto space-y-1" wire:ignore>
      <h4 class="font-medium text-sm text-dark text-center">Duration</h4>
      <h5 x-data="timer()" x-init="init()" class="flex flex-nowrap justify-center text-secondary text-center">
        <span x-text="time().hours" class="after:content-[':']"></span>
        <span x-text="time().minutes" class="after:content-[':']"></span>
        <span x-text="time().seconds"></span>
      </h5>
    </div>
  </div>
  {{-- Header Page End --}}
  
  {{-- Main Page Start --}}
  <div class="mx-auto my-8 max-w-lg xl:max-w-xl 2xl:max-w-2xl">
    <ol class="list-decimal space-y-36">
      @foreach ($assignment->assignmentQuestions as $index => $assignmentQuestion)
        <li wire:key="assignment-question-{{ $assignmentQuestion->id }}" class="space-y-8">
          <h4 class="text-dark text-justify">{!! $assignmentQuestion->question !!}</h4>
          
          <div class="block my-4" wire:ignore>
            <h5 class="font-medium text-xs text-dark text-right p-4">({{ round(100 / count($assignment->assignmentQuestions), 2) }} points)</h5>
            
            <div id="answer{{ $index }}" class="default-editor overflow-y-auto h-64 p-4 border border-slate-100 scrollbar shadow-md rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-lg lg:col-span-5">
              
            </div>
          </div>
        </li>
      @endforeach
    </ol>
  </div>
  {{-- Main Page End --}}
  
  {{-- Footer Page Start --}}
  <div class="flex items-center justify-end mx-32">
    <button type="button" class="flex flex-nowrap items-center justify-center my-2 px-4 py-2 space-x-1 bg-primary rounded-full transition duration-300 hover:ring hover:ring-primary-200/70 hover:shadow-md">
      <span class="material-symbols-outlined text-white">
        save
      </span>
      
      <span class="text-white">Submit</span>
    </button>
  </div>
  {{-- Footer Page End --}}

  @push('script-customize')
    <script>
      function timer() {
        return {
          counter: 0,
          from: 0,
          duration: 9659938186254,
          status: true,
          remaining: null,
          
          init() {
            this.setRemaining()
            setInterval(() => {
              this.setRemaining();
            }, 1000);
          },
          setRemaining() {
            if (new Date().setTime(this.duration) > new Date().getTime()) {
              const diff = new Date().setTime(this.duration) - new Date().getTime();
              this.remaining =  parseInt(diff / 1000);
            } else if (this.status) {
              this.status = false
              document.querySelectorAll('.default-editor').forEach(element => {
                element.blur()
              });
              
              Livewire.emit('submitAssignment')
            }

            window.addEventListener('duration-update', event => {
              this.counter++
              this.from = event.detail.from
              this.duration = event.detail.duration
              this.init()
            })
          },
          days() {
            return {
              value:this.remaining / 86400,
              remaining:this.remaining % 86400
            };
          },
          hours() {
            return {
              value:this.days().remaining / 3600,
              remaining:this.days().remaining % 3600
            };
          },
          minutes() {
            return {
              value:this.hours().remaining / 60,
              remaining:this.hours().remaining % 60
            };
          },
          seconds() {
            return {
              value:this.minutes().remaining,
            };
          },
          format(value) {
            return ("0" + parseInt(value)).slice(-2)
          },
          time(){
            return {
              days:this.format(this.days().value),
              hours:this.format(this.hours().value),
              minutes:this.format(this.minutes().value),
              seconds:this.format(this.seconds().value),
            }
          },
        }
      }

      
      var editor_config = {
				path_absolute : "/",
				selector: 'div.default-editor',
				relative_urls: false,
				resize: 'both',
				min_height: 500,
				min_width: 300,
				width: 300,
				menubar: false,
				inline: true,
				plugins: [
					'advlist', 'autoresize', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview', 'pagebreak',
					'anchor', 'searchreplace', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'quickbars ', 'codesample',
					'insertdatetime', 'media', 'nonbreaking', 'save', 'table', 'help', 'wordcount', 'directionality',
					'emoticons',  'template'
				],
				codesample_languages: [
					{text: 'HTML/XML', value: 'markup'},
					{text: 'JavaScript', value: 'javascript'},
					{text: 'CSS', value: 'css'},
					{text: 'PHP', value: 'php'},
					{text: 'Ruby', value: 'ruby'},
					{text: 'Python', value: 'python'},
					{text: 'Java', value: 'java'},
					{text: 'C', value: 'c'},
					{text: 'C#', value: 'csharp'},
					{text: 'C++', value: 'cpp'}
				],
				toolbar: 'undo redo codesample code preview | styleselect blocks removeformat emoticons | ' +
					'bold italic backcolor table | alignleft aligncenter ' +
					'alignright alignjustify | bullist numlist outdent indent | ' +
					'link image media help',
				content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
				contextmenu: 'undo redo | inserttable | cell row column deletetable | help',
				quickbars_insert_toolbar: false,
				quickbars_selection_toolbar: 'bold italic underline | styleselect blocks | bullist numlist | backcolor quicklink',
				quickbars_image_toolbar: 'alignleft aligncenter alignright | rotateleft rotateright | imageoptions',
				powerpaste_word_import: 'clean',
				powerpaste_html_import: 'clean',
				
				setup: function (editor) {
          window.addEventListener('fetch-content', event => {
              if (editor.id.replace('answer', '') == event.detail.index) {
                editor.setContent(event.detail.answer.answer)
              }
					})
					
					editor.on('init', function () {
            Livewire.emit('setContent', editor.id.replace('answer', ''));
						editor.save();
					});

					editor.on('change', function () {
						editor.save();
					});
					
					editor.on('blur', function (e) {
            Livewire.emit('contentUpdate', editor.id.replace('answer', ''), editor.getContent());
					});
				},
				
				file_picker_callback : function(callback, value, meta) {
					var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
					var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
	
					var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
					if (meta.filetype == 'image') {
						cmsURL = cmsURL + "&type=Images";
					} else {
						cmsURL = cmsURL + "&type=Files";
					}
	
					tinyMCE.activeEditor.windowManager.openUrl({
						url : cmsURL,
						title : 'Filemanager',
						width : x * 0.8,
						height : y * 0.8,
						resizable : "yes",
						close_previous : "no",
						onMessage: (api, message) => {
						callback(message.content);
						}
					});
				}
			};

      tinymce.init(editor_config);
    </script>
  @endpush
</div>