@extends('layouts.master-dashboard')

@section('title', 'Material Details')

@section('content')
{{-- Breadcrumb Start --}}
@livewire('dashboard.components.breadcrumb', ['routes' => [
[
'link' => route('student.my-courses.index'),
'name' => 'My Courses'
],
[
'link' => route('student.my-courses.course.show', ['course' => $course->id]),
'name' => 'Course Details'
],
[
'link' => route('student.my-courses.course.material.show', ['course' => $course->id, 'session' => $session->id]),
'name' => 'Material Session'
]
]
])
{{-- Breadcrumb End --}}

{{-- Content Start --}}
<div class="lg:px-32">
  <div class="container">
    <div class="block bg-white min-h-full mt-2 mb-16 px-8 py-4 space-y-8 rounded-md shadow-lg">
      <h3 class="font-semibold text-xl text-dark text-center p-4 max-w-xl mx-auto lg:text-2xl">{{ $session->title }}</h3>

      <div class="block">
        {!! $session->content !!}
      </div>
    </div>
  </div>
</div>
{{-- Content End --}}
@endsection