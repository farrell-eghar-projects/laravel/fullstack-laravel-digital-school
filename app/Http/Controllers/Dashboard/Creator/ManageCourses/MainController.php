<?php

namespace App\Http\Controllers\Dashboard\Creator\ManageCourses;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Material;
use App\Models\MaterialSection;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
	{
		$courses = auth()->user()->courses;
		$materials = auth()->user()->materials;
		$sessions = array();
		
		foreach ($materials as $index => $material) {
			array_push($sessions, 0);
			foreach ($material->materialSections as $i => $materialSection) {
				$sessions[$index] += count($materialSection->materialSessions);
			}
		}
		
		return view('pages.dashboard.creator.manage-courses.index', compact('materials', 'sessions', 'courses'));
	}
}
