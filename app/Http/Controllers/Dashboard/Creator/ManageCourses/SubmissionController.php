<?php

namespace App\Http\Controllers\Dashboard\Creator\ManageCourses;

use App\Http\Controllers\Controller;
use App\Models\Assignment;
use App\Models\AssignmentSubmission;
use App\Models\Course;
use Illuminate\Http\Request;

class SubmissionController extends Controller
{
  public function show(Course $course, Assignment $assignment, AssignmentSubmission $assignmentSubmission)
  {
    return view('pages.dashboard.creator.manage-courses.course.assignment.submission.show', compact('course', 'assignment', 'assignmentSubmission'));
  }
}
