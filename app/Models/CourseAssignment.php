<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseAssignment extends Model
{
    use HasFactory;
	
	protected $guarded = [];
	
	public function course()
	{
		return $this->belongsTo(Course::class);
	}
	
	public function assignment()
	{
		return $this->belongsTo(Assignment::class);
	}
	
	public function assignmentSubmissions()
	{
		return $this->hasMany(AssignmentSubmission::class);
	}
}
