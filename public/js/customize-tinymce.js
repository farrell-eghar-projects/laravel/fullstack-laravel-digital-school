var editor_config = {
    path_absolute : "/",
    selector: 'div.default-editor',
    relative_urls: false,
	resize: 'both',
	min_height: 500,
	min_width: 300,
	width: 300,
	menubar: false,
	inline: true,
	plugins: [
		'advlist', 'autoresize', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview', 'pagebreak',
		'anchor', 'searchreplace', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'quickbars ', 'codesample',
		'insertdatetime', 'media', 'nonbreaking', 'save', 'table', 'help', 'wordcount', 'directionality',
		'emoticons',  'template'
	],
	codesample_languages: [
		{text: 'HTML/XML', value: 'markup'},
		{text: 'JavaScript', value: 'javascript'},
		{text: 'CSS', value: 'css'},
		{text: 'PHP', value: 'php'},
		{text: 'Ruby', value: 'ruby'},
		{text: 'Python', value: 'python'},
		{text: 'Java', value: 'java'},
		{text: 'C', value: 'c'},
		{text: 'C#', value: 'csharp'},
		{text: 'C++', value: 'cpp'}
  	],
	toolbar: 'undo redo codesample code preview | styleselect blocks removeformat emoticons | ' +
		'bold italic backcolor table | alignleft aligncenter ' +
		'alignright alignjustify | bullist numlist outdent indent | ' +
		'link image media help',
	content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
	contextmenu: 'undo redo | inserttable | cell row column deletetable | help',
	quickbars_insert_toolbar: false,
	quickbars_selection_toolbar: 'bold italic underline | styleselect blocks | bullist numlist | backcolor quicklink',
	quickbars_image_toolbar: 'alignleft aligncenter alignright | rotateleft rotateright | imageoptions',
	powerpaste_word_import: 'clean',
	powerpaste_html_import: 'clean',
	
	setup: function (editor) {
		editor.on('init change', function () {
			editor.save();
			editor.setContent('<p>Hello world!</p>')
		});
		editor.on('change', function (e) {
			Livewire.emit('contentUpdate', editor.getContent());
		});
	},
	
    file_picker_callback : function(callback, value, meta) {
		var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
		var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

		var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
		if (meta.filetype == 'image') {
			cmsURL = cmsURL + "&type=Images";
		} else {
			cmsURL = cmsURL + "&type=Files";
		}

		tinyMCE.activeEditor.windowManager.openUrl({
			url : cmsURL,
			title : 'Filemanager',
			width : x * 0.8,
			height : y * 0.8,
			resizable : "yes",
			close_previous : "no",
			onMessage: (api, message) => {
			callback(message.content);
			}
		});
    }
};

tinymce.init(editor_config);