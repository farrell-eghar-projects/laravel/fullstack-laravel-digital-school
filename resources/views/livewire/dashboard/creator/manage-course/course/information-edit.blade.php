<div x-data="{edit: false}" x-show="open == 'information'" x-transition class="block space-y-8">
  <div class="flex flex-nowrap items-center justify-end space-x-2">
    <button type="button" x-show="edit == false" x-on:click="edit = !edit" class="flex flex-nowrap items-center p-1 bg-orange-500 rounded-md hover:opacity-80">
      <span class="material-symbols-outlined text-white">
        edit
      </span>
    </button>

    <button type="button" wire:click="refreshCourse" x-show="edit == true" x-on:click="edit = !edit" class="flex flex-nowrap items-center p-1 bg-red-500 rounded-md hover:opacity-80">
      <span class="material-symbols-outlined text-white text-center">
        close
      </span>
    </button>
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Course Code</h4>

    <h5 x-bind:class="{'hidden': edit}" class="text-secondary lg:col-span-3">{{ $course->code }}</h5>

    <input x-bind:class="{'hidden': !edit}" type="text" id="consultation_group" wire:model.defer="course.code" class="default-input w-full lg:col-span-3">
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Course Password</h4>

    <h5 x-bind:class="{'hidden': edit}" class="text-secondary lg:col-span-3">{{ $course->password }}</h5>

    <input x-bind:class="{'hidden': !edit}" type="text" id="consultation_group" wire:model.defer="course.password" class="default-input w-full lg:col-span-3">
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Suitable Target</h4>

    <ul x-bind:class="{'hidden': edit}" class="px-4 list-disc text-secondary lg:col-span-3">
      @foreach ($course->courseSuitableTargets as $index => $courseSuitableTarget)
      <li>{{ $courseSuitableTarget->target }}</li>
      @endforeach
    </ul>

    <div x-bind:class="{'hidden': !edit}" class="space-y-2 lg:col-span-3">
      @foreach ($suitableTargets as $index => $suitableTarget)
      <div wire:key="suitableTarget-field-{{ $index }}" class="space-y-2">
        <div class="flex flex-nowrap items-center space-x-2">
          <input type="text" id="suitable_target{{ $index }}" wire:model.defer="suitableTargets.{{ $index }}" placeholder="Those who prepare to work on international environment" class="basis-11/12 default-input">

          @if ($loop->count > 1)
          <button type="button" wire:click="deleteSuitableTarget({{ $index }})" class="flex items-center p-0 bg-red-500 rounded-full hover:opacity-70">
            <span class="material-symbols-outlined text-white text-center rounded-full">
              cancel
            </span>
          </button>
          @else
          <button type="button" wire:click="deleteSuitableTarget({{ $index }})" disabled class="flex items-center p-0 bg-slate-200 rounded-full hover:opacity-70">
            <span class="material-symbols-outlined text-white text-center rounded-full">
              cancel
            </span>
          </button>
          @endif
        </div>

        @if ($loop->last)
        <div class="flex flex-nowrap items-center space-x-4">
          <button type="button" wire:click="addSuitableTarget" class="my-auto flex items-center px-4 py-1 bg-green-400 rounded-full hover:opacity-70">
            <span class="material-symbols-outlined text-white text-center rounded-full">
              add
            </span>
          </button>
        </div>
        @endif
      </div>
      @endforeach
    </div>
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Prerequisite</h4>

    <ul x-bind:class="{'hidden': edit}" class="px-4 list-disc text-secondary lg:col-span-3">
      @foreach ($course->coursePrerequisites as $index => $coursePrerequisite)
      <li>{{ $coursePrerequisite->prerequisite }}</li>
      @endforeach
    </ul>

    <div x-bind:class="{'hidden': !edit}" class="space-y-2 lg:col-span-3">
      @foreach ($prerequisites as $index => $prerequisite)
      <div wire:key="prerequisite-field-{{ $index }}" class="space-y-2">
        <div class="flex flex-nowrap items-center space-x-2">
          <input type="text" id="prerequisite{{ $index }}" wire:model.defer="prerequisites.{{ $index }}" placeholder="Student should be able to speak" class="basis-11/12 default-input">

          @if ($loop->count > 1)
          <button type="button" wire:click="deletePrerequisite({{ $index }})" class="flex items-center p-0 bg-red-500 rounded-full hover:opacity-70">
            <span class="material-symbols-outlined text-white text-center rounded-full">
              cancel
            </span>
          </button>
          @else
          <button type="button" wire:click="deletePrerequisite({{ $index }})" disabled class="flex items-center p-0 bg-slate-200 rounded-full hover:opacity-70">
            <span class="material-symbols-outlined text-white text-center rounded-full">
              cancel
            </span>
          </button>
          @endif
        </div>

        @if ($loop->last)
        <div class="flex flex-nowrap items-center space-x-4">
          <button type="button" wire:click="addPrerequisite" class="my-auto flex items-center px-4 py-1 bg-green-400 rounded-full hover:opacity-70">
            <span class="material-symbols-outlined text-white text-center rounded-full">
              add
            </span>
          </button>
        </div>
        @endif
      </div>
      @endforeach
    </div>
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Student Target</h4>

    <h5 x-bind:class="{'hidden': edit}" class="text-secondary lg:col-span-3">
      @forelse ($displayStudentTargets as $index => $displayStudentTarget)
      {{ $displayStudentTarget }}@if (!$loop->last), @endif
      @empty
      None
      @endforelse
    </h5>

    <div x-bind:class="{'hidden': !edit}" class="flex flex-wrap items-center lg:col-span-3">
      <span class="basis-1/2 flex flex-nowrap items-center space-x-1 py-1 sm:py-1 sm:basis-1/3">
        <input type="checkbox" wire:model.defer="studentTargets" id="highschool" value="Highschool Student" class="default-input-checkbox">
        <label for="highschool">Highschool</label>
      </span>

      <span class="basis-1/2 flex flex-nowrap items-center space-x-1 py-1 sm:py-1 sm:basis-1/3">
        <input type="checkbox" wire:model.defer="studentTargets" id="college" value="College Student" class="default-input-checkbox">
        <label for="college">College</label>
      </span>

      <span class="basis-1/2 flex flex-nowrap items-center space-x-1 py-1 sm:py-1 sm:basis-1/3">
        <input type="checkbox" wire:model.defer="studentTargets" id="employee" value="Employee" class="default-input-checkbox">
        <label for="employee">Employee</label>
      </span>
    </div>
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Course Type</h4>

    <h5 x-bind:class="{'hidden': edit}" class="text-secondary lg:col-span-3">{{ $course->type->name }}</h5>

    <select x-bind:class="{'hidden': !edit}" id="type" name="type" disabled class="default-input w-full lg:col-span-3 disabled:bg-slate-200/70">
      <option value="Intensive Class" selected>Intensive Class</option>
      <option value="Lifetime Class">Lifetime Class</option>
      <option value="Subscription Class">Subscription Class</option>
    </select>
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Course Material</h4>

    <h5 x-bind:class="{'hidden': edit}" class="text-secondary lg:col-span-3">{{ $course->material->name }}</h5>

    <select x-bind:class="{'hidden': !edit}" id="type" name="type" disabled class="default-input w-full lg:col-span-3 disabled:bg-slate-200/70">
      <option value="Easy Interview with Native Speaker" selected>Easy Interview with Native Speaker</option>
    </select>
  </div>



  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Description</h4>

    <h5 x-bind:class="{'hidden': edit}" class="text-secondary lg:col-span-3">{!! $course->description !!}</h5>

    <div wire:ignore x-bind:class="{'hidden': !edit}" name="description" id="description" class="default-editor default-input overflow-y-auto h-56 p-4 w-full lg:col-span-3"></div>
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Started At</h4>

    <h5 x-bind:class="{'hidden': edit}" class="text-secondary lg:col-span-3">{{ $started_at }}</h5>

    <input x-bind:class="{'hidden': !edit}" type="date" name="started_at" id="started_at" value="{{ $course->started_at }}" disabled class="default-input w-full lg:col-span-3 disabled:bg-slate-200/70 disabled:text-secondary">
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Finished At</h4>

    <h5 x-bind:class="{'hidden': edit}" class="text-secondary lg:col-span-3">{{ $finished_at }}</h5>

    <input x-bind:class="{'hidden': !edit}" type="date" name="finished_at" id="finished_at" value="{{ $course->finished_at }}" disabled class="default-input w-full lg:col-span-3 disabled:bg-slate-200/70 disabled:text-secondary">
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Thumbnail</h4>

    <div id="course_thumbnail" name="course_thumbnail" class="flex flex-wrap mt-4 lg:col-span-3">
      <div class="w-32 max-h-24 rounded-md overflow-hidden shrink-0">
        <img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover">
      </div>
    </div>
  </div>

  <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
    <h4 class="font-medium text-dark">Preview</h4>

    <div id="course_preview" name="course_preview" class="flex flex-wrap mt-4 lg:col-span-3">
      <div class="inline w-32 max-h-24 rounded-md overflow-hidden mr-4 mb-4 shrink-0">
        <img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="inline w-full object-cover">
      </div>

      <div class="inline w-32 max-h-24 rounded-md overflow-hidden mr-4 mb-4 shrink-0">
        <img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="inline w-full object-cover">
      </div>

      <div class="inline w-32 max-h-24 rounded-md overflow-hidden mr-4 mb-4 shrink-0">
        <img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="inline w-full object-cover">
      </div>
    </div>
  </div>

  <div class="flex items-center justify-end space-x-8">
    <button type="button" wire:click="refreshCourse" x-show="edit == true" x-on:click="edit = !edit" class="flex flex-nowrap items-center justify-center w-24 p-2 bg-red-500 rounded-md space-x-1 hover:opacity-80">
      <span class="material-symbols-outlined text-white">
        close
      </span>

      <span class="text-white">Cancel</span>
    </button>

    <button type="button" wire:click="save" x-show="edit == true" x-on:click="edit = !edit" class="flex flex-nowrap items-center justify-center w-24 p-2 bg-blue-500 rounded-md space-x-1 hover:opacity-80">
      <span class="material-symbols-outlined text-white">
        save
      </span>

      <span class="text-white text-center">
        save
      </span>
    </button>
  </div>
  @push('script-customize')
  <script>
    var route_prefix = "/filemanager";

    var editor_config = {
      path_absolute: "/",
      selector: 'div.default-editor',
      relative_urls: false,
      resize: 'both',
      min_height: 500,
      min_width: 300,
      width: 300,
      menubar: false,
      inline: true,
      plugins: [
        'advlist', 'autoresize', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview', 'pagebreak',
        'anchor', 'searchreplace', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'quickbars ', 'codesample',
        'insertdatetime', 'media', 'nonbreaking', 'save', 'table', 'help', 'wordcount', 'directionality',
        'emoticons', 'template'
      ],
      codesample_languages: [{
          text: 'HTML/XML',
          value: 'markup'
        },
        {
          text: 'JavaScript',
          value: 'javascript'
        },
        {
          text: 'CSS',
          value: 'css'
        },
        {
          text: 'PHP',
          value: 'php'
        },
        {
          text: 'Ruby',
          value: 'ruby'
        },
        {
          text: 'Python',
          value: 'python'
        },
        {
          text: 'Java',
          value: 'java'
        },
        {
          text: 'C',
          value: 'c'
        },
        {
          text: 'C#',
          value: 'csharp'
        },
        {
          text: 'C++',
          value: 'cpp'
        }
      ],
      toolbar: 'undo redo codesample code preview | styleselect blocks removeformat emoticons | ' +
        'bold italic backcolor table | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'link image media help',
      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
      contextmenu: 'undo redo | inserttable | cell row column deletetable | help',
      quickbars_insert_toolbar: false,
      quickbars_selection_toolbar: 'bold italic underline | styleselect blocks | bullist numlist | backcolor quicklink',
      quickbars_image_toolbar: 'alignleft aligncenter alignright | rotateleft rotateright | imageoptions',
      powerpaste_word_import: 'clean',
      powerpaste_html_import: 'clean',

      setup: function(editor) {
        window.addEventListener('fetch-content', event => {
          editor.setContent(event.detail.content)
        })

        window.addEventListener('update-content', event => {
          Livewire.emit('contentUpdate', editor.getContent());
        })

        editor.on('init', function() {
          Livewire.emit('setContent');
          editor.save();
        });

        editor.on('change', function() {
          editor.save();
        });
      },

      file_picker_callback: function(callback, value, meta) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
        if (meta.filetype == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.openUrl({
          url: cmsURL,
          title: 'Filemanager',
          width: x * 0.8,
          height: y * 0.8,
          resizable: "yes",
          close_previous: "no",
          onMessage: (api, message) => {
            callback(message.content);
          }
        });
      }
    };

    tinymce.init(editor_config);
  </script>
  @endpush
</div>