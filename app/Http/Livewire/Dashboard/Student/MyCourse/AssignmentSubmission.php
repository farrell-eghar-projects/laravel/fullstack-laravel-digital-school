<?php

namespace App\Http\Livewire\Dashboard\Student\MyCourse;

use App\Models\Assignment;
use App\Models\AssignmentSubmission as ModelsAssignmentSubmission;
use App\Models\Course;
use App\Models\CourseAssignment;
use Carbon\Carbon;
use Livewire\Component;

class AssignmentSubmission extends Component
{
  public Course $course;
  public Assignment $assignment;
  public CourseAssignment $courseAssignment;
  public ModelsAssignmentSubmission $assignmentSubmission;
  public $submissionAnswers;
  public $milliDuration;
  public $counter;

  protected $rules = [
    'submissionAnswers.*.answer' => 'required|string'
  ];

  protected $listeners = [
    'setContent' => 'setContent',
    'contentUpdate' => 'contentUpdate',
    'submitAssignment' => 'submitAssignment',
    'refreshComponent' => '$refresh'
  ];

  public function mount()
  {
    $this->submissionAnswers = $this->assignmentSubmission->submissionAnswers;
    $this->counter = count($this->submissionAnswers);
    $this->milliDuration = Carbon::parse($this->assignmentSubmission->duration)->getTimestampMs();
  }

  public function dehydrate()
  {
    $this->dispatchBrowserEvent('duration-update', ['duration' => $this->milliDuration]);
  }

  public function setContent($index)
  {
    $this->dispatchBrowserEvent('fetch-content', ['answer' => $this->submissionAnswers[$index], 'index' => $index, 'total' => count($this->submissionAnswers)]);
  }

  public function contentUpdate($index, $value)
  {
    $this->submissionAnswers[$index]->answer = $value;
    $this->submissionAnswers->each->save();
  }

  public function submitAssignment()
  {
    $this->assignmentSubmission->status = true;
    $this->assignmentSubmission->save();

    return redirect()->route('student.my-courses.course.show', ['course' => $this->course->id]);
  }

  public function render()
  {
    return view('livewire.dashboard.student.my-course.assignment-submission');
  }
}
