<?php

use App\Models\Material;
use App\Models\Type;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
			$table->foreignIdFor(User::class)->nullable();
			$table->foreignIdFor(Type::class)->nullable();
			$table->foreignIdFor(Material::class)->nullable();
			$table->string('title')->nullable();
			$table->string('code')->nullable();
			$table->string('password')->nullable();
			$table->longText('description')->nullable();
			$table->string('student_targets')->nullable();
			$table->string('thumbnail')->nullable();
			$table->string('images')->nullable();
			$table->boolean('published')->default(false);
			$table->date('started_at')->nullable();
			$table->date('finished_at')->nullable();
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('type_id')->references('id')->on('types');
			$table->foreign('material_id')->references('id')->on('materials');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
};
