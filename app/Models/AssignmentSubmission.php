<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignmentSubmission extends Model
{
  use HasFactory;

  protected $guarded = [];

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function courseAssignment()
  {
    return $this->belongsTo(CourseAssignment::class);
  }

  public function submissionAnswers()
  {
    return $this->hasMany(SubmissionAnswer::class);
  }
}
