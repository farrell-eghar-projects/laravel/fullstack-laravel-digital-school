<?php

namespace App\Http\Controllers\Dashboard\Student\MyCourses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
	{
    $myCourses = auth()->user()->myCourses;
    $sessions = array(array());
    $sessionCount = array();
		
		foreach ($myCourses as $index => $myCourse) {
			array_push($sessions, array());
      
      foreach ($myCourse->course->material->materialSections as $key => $materialSection) {
        array_push($sessions[$index], count($materialSection->materialSessions));
      }

      array_push($sessionCount, 0);
      foreach ($sessions[$index] as $key => $session) {
        $sessionCount[$index] += $session;
      }
		}
    
		return view('pages.dashboard.student.my-courses.index', compact('myCourses', 'sessionCount'));
	}
}
