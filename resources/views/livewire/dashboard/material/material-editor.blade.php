<div x-data="{edit: 'null', part: '', floatInput: false}" class="container">
  {{-- Floating Input Start --}}
  <div x-show="floatInput" x-on:click="floatInput = false" class="fixed inset-0 bg-secondary/70 animate-fadeIn z-10">
    <div x-on:click.stop class="mx-auto bg-white w-3/4 p-8 space-y-12 rounded-md shadow-2xl translate-y-1/2 z-20 md:w-1/2">
      <span x-on:click="floatInput = false" class="material-symbols-outlined block float-right text-dark cursor-pointer">
        close
      </span>

      <div class="space-y-8">
        <div class="space-y-2">
          <label for="name" class="block font-medium text-dark" x-text="part"></label>
          <input type="text" id="name" wire:model="name" autocomplete="off" class="default-input block w-full">
          @error('name')
          <span class="font-medium text-xs text-red-400">{{ $message }}</span>
          @enderror
        </div>

        <div class="flex items-center justify-end">
          <button type="button" wire:click="updateSection" x-show="part == 'Section Topic'" x-on:click="floatInput = false" class="bg-primary text-white px-4 py-2 rounded-md float-right hover:opacity-70">Save</button>

          <button type="button" wire:click="updateSessionTitle" x-show="part == 'Session Title'" x-on:click="floatInput = false" class="bg-primary text-white px-4 py-2 rounded-md float-right hover:opacity-70">Save</button>

          <button type="button" wire:click="updateAssignment" x-show="part == 'Assignment Name'" x-on:click="floatInput = false" class="bg-primary text-white px-4 py-2 rounded-md float-right hover:opacity-70">Save</button>
        </div>
      </div>
    </div>
  </div>
  {{-- Floating Input End --}}

  {{-- Material Menu Start --}}
  <div class="block bg-white my-2 px-8 py-6 rounded-md shadow-lg space-y-8">
    <h3 class="font-semibold text-xl text-dark text-center px-4 py-2 max-w-xl mx-auto lg:text-2xl">{{ $material->name }}</h3>

    <div class="grid grid-cols-1 gap-8 lg:grid-cols-2">
      <div x-data="{open: false}" class="block px-4">
        <button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full py-4">
          <h3 class="font-medium text-dark text-md">Sections</h4>

            <div class="flex items-center space-x-2">
              <span x-on:click.stop wire:click="addSection" class="material-symbols-outlined bg-green-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                add
              </span>

              <span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
                expand_more
              </span>
            </div>
        </button>

        <div x-show="open" x-transition class="px-4 space-y-4">
          @foreach ($material->materialSections as $index => $section)
          <div wire:key="materialSection-field-{{ $section->id }}" x-data="{section: false}" x-bind:class="section ? 'border-primary' : 'border-secondary/70'" class="block border-b py-2 space-y-2">
            <button x-on:click="section = !section" type="button" class="flex items-center justify-between w-full">
              <h4 class="font-medium text-dark text-sm text-left">{{ $section->name }}</h4>

              <div class="flex items-center space-x-2">
                <span x-on:click.stop wire:click="addSession({{ $section->id }})" class="material-symbols-outlined bg-green-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                  add
                </span>

                <span x-on:click.stop wire:click="editSection({{ $section->id }})" x-on:click="floatInput = true; part = 'Section Topic'" class="material-symbols-outlined bg-orange-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                  edit
                </span>

                <span x-on:click.stop wire:click="deleteSection({{ $section->id }})" class="material-symbols-outlined bg-red-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                  delete
                </span>

                <span x-bind:class="{'rotate-180 text-primary': section}" class="material-symbols-outlined text-lg transition duration-300">
                  expand_more
                </span>
              </div>
            </button>

            <div x-show="section" x-transition class="px-4 space-y-2">
              @foreach ($section->materialSessions as $i => $session)
              <button type="button" wire:click="editSessionContent({{ $session->id }})" x-on:click="edit = '{{ $session->title }}'; $nextTick(() => {window.getSelection().removeAllRanges(); $refs.content.focus();});" class="group flex items-center justify-between w-full py-2">
                <h5 class="text-dark text-sm text-left group-hover:text-primary">{{ $session->title }}</h5>

                <div class="flex items-center space-x-2">
                  <span x-on:click.stop wire:click="editSessionTitle({{ $session->id }})" x-on:click="floatInput = true; part = 'Session Title'" class="material-symbols-outlined bg-orange-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                    edit
                  </span>

                  <span x-on:click.stop wire:click="deleteSession({{ $session->id }})" class="material-symbols-outlined bg-red-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                    delete
                  </span>
                </div>
              </button>
              @endforeach
            </div>
          </div>
          @endforeach
        </div>
      </div>

      <div x-data="{open: false}" class="block px-4">
        <button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full py-4">
          <h3 class="font-medium text-dark text-md">Assignments</h4>

            <div class="flex items-center space-x-2">
              <span x-on:click.stop wire:click="addAssignment" class="material-symbols-outlined bg-green-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                add
              </span>

              <span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
                expand_more
              </span>
            </div>
        </button>

        <div x-show="open" x-transition class="px-4 space-y-4">
          @foreach ($material->assignments as $index => $assignment)
          <div wire:key="assignment-field-{{ $assignment->id }}" x-data="{assignment: false}" x-bind:class="assignment ? 'border-primary' : 'border-secondary/70'" class="block border-b py-2 space-y-2">
            <button x-on:click="assignment = !assignment" type="button" class="flex items-center justify-between w-full">
              <h4 class="font-medium text-dark text-sm text-left">{{ $assignment->name }}</h4>

              <div class="flex items-center space-x-2">
                <span x-on:click.stop wire:click="addQuestion({{ $assignment->id }})" class="material-symbols-outlined bg-green-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                  add
                </span>

                <span x-on:click.stop wire:click="editAssignment({{ $assignment->id }})" x-on:click="floatInput = true; part = 'Assignment Name';" class="material-symbols-outlined bg-orange-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                  edit
                </span>

                <span x-on:click.stop wire:click="deleteAssignment({{ $assignment->id }})" class="material-symbols-outlined bg-red-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                  delete
                </span>

                <span x-bind:class="{'rotate-180 text-primary': assignment}" class="material-symbols-outlined text-lg transition duration-300">
                  expand_more
                </span>
              </div>
            </button>

            <div x-show="assignment" x-transition class="px-4 space-y-2">
              @foreach ($assignment->assignmentQuestions as $i => $question)
              <button type="button" wire:click="editQuestion({{ $question->id }})" x-on:click="edit = 'Question {{ $i + 1 }}'; $nextTick(() => {window.getSelection().removeAllRanges(); $refs.content.focus();});" class="group flex items-center justify-between w-full py-2">
                <h5 class="text-dark text-sm text-left group-hover:text-primary">Question {{ $i + 1 }}</h5>

                <span x-on:click.stop wire:click="deleteQuestion({{ $question->id }})" class="material-symbols-outlined bg-red-400 font-medium text-white px-1 rounded-full hover:opacity-70">
                  delete
                </span>
              </button>
              @endforeach
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  {{-- Material Menu End --}}

  {{-- Material Content Start --}}
  <div x-show="edit !='null'" x-transition class="block bg-white mt-16 mb-8 px-8 py-6 rounded-md shadow-lg space-y-4" wire:ignore>
    <h5 x-text="edit" class="font-medium text-dark text-center"></h5>

    <div x-ref="content" name="description" id="description" class="default-editor overflow-y-auto h-80 p-4 border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5"></div>
  </div>
  {{-- Material Content End --}}

  @push('script-customize')
  <script>
    var editor_config = {
      path_absolute: "/",
      selector: 'div.default-editor',
      relative_urls: false,
      resize: 'both',
      min_height: 500,
      min_width: 300,
      width: 300,
      menubar: false,
      inline: true,
      plugins: [
        'advlist', 'autoresize', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview', 'pagebreak',
        'anchor', 'searchreplace', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'quickbars ', 'codesample',
        'insertdatetime', 'media', 'nonbreaking', 'save', 'table', 'help', 'wordcount', 'directionality',
        'emoticons', 'template'
      ],
      codesample_languages: [{
          text: 'HTML/XML',
          value: 'markup'
        },
        {
          text: 'JavaScript',
          value: 'javascript'
        },
        {
          text: 'CSS',
          value: 'css'
        },
        {
          text: 'PHP',
          value: 'php'
        },
        {
          text: 'Ruby',
          value: 'ruby'
        },
        {
          text: 'Python',
          value: 'python'
        },
        {
          text: 'Java',
          value: 'java'
        },
        {
          text: 'C',
          value: 'c'
        },
        {
          text: 'C#',
          value: 'csharp'
        },
        {
          text: 'C++',
          value: 'cpp'
        }
      ],
      toolbar: 'undo redo codesample code preview | styleselect blocks removeformat emoticons | ' +
        'bold italic backcolor table | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'link image media help',
      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
      contextmenu: 'undo redo | inserttable | cell row column deletetable | help',
      quickbars_insert_toolbar: false,
      quickbars_selection_toolbar: 'bold italic underline | styleselect blocks | bullist numlist | backcolor quicklink',
      quickbars_image_toolbar: 'alignleft aligncenter alignright | rotateleft rotateright | imageoptions',
      powerpaste_word_import: 'clean',
      powerpaste_html_import: 'clean',

      setup: function(editor) {
        var part = true;

        window.addEventListener('edit-content', event => {
          editor.setContent(event.detail.content)
          part = event.detail.part
        })

        editor.on('init change', function() {
          editor.save();
        });

        editor.on('blur', function(e) {
          Livewire.emit('contentUpdate', [part, editor.getContent()]);
        });
      },

      file_picker_callback: function(callback, value, meta) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
        if (meta.filetype == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.openUrl({
          url: cmsURL,
          title: 'Filemanager',
          width: x * 0.8,
          height: y * 0.8,
          resizable: "yes",
          close_previous: "no",
          onMessage: (api, message) => {
            callback(message.content);
          }
        });
      }
    };

    tinymce.init(editor_config);
  </script>
  @endpush
</div>