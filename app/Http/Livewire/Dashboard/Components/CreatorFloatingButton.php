<?php

namespace App\Http\Livewire\Dashboard\Components;

use Livewire\Component;

class CreatorFloatingButton extends Component
{
    public function render()
    {
        return view('livewire.dashboard.components.creator-floating-button');
    }
}
