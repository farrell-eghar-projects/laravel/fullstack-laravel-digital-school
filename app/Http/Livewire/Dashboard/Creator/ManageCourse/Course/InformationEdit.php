<?php

namespace App\Http\Livewire\Dashboard\Creator\ManageCourse\Course;

use App\Models\Course;
use App\Models\CoursePrerequisite;
use App\Models\CourseSuitableTarget;
use Livewire\Component;

class InformationEdit extends Component
{
  public Course $course;
  public $displayStudentTargets;
  public $studentTargets = array();
  public $prerequisites = array();
  public $suitableTargets = array();
  public $started_at;
  public $finished_at;

  protected $rules = [
    'course.code' => 'required|string',
    'course.password' => 'required|string',
    'course.description' => 'required|string',
    'course.student_targets' => 'required|string',
    'prerequisites.*' => 'required|string',
    'suitableTargets.*' => 'required|string'
  ];

  protected $listeners = [
    'setContent' => 'setContent',
    'contentUpdate' => 'contentUpdate',
    'refreshComponent' => '$refresh'
  ];

  public function mount()
  {
    foreach ($this->course->coursePrerequisites as $index => $coursePrerequisite) {
      array_push($this->prerequisites, $coursePrerequisite->prerequisite);
    }

    foreach ($this->course->courseSuitableTargets as $index => $courseSuitableTarget) {
      array_push($this->suitableTargets, $courseSuitableTarget->target);
    }

    $this->studentTargets = explode(',', $this->course->student_targets);
    $this->displayStudentTargets = $this->studentTargets;
    $this->started_at = date("F j, Y", mktime(0, 0, 0, explode('-', $this->course->started_at)[1], explode('-', $this->course->started_at)[2], explode('-', $this->course->started_at)[0]));
    $this->finished_at = date("F j, Y", mktime(0, 0, 0, explode('-', $this->course->finished_at)[1], explode('-', $this->course->finished_at)[2], explode('-', $this->course->finished_at)[0]));
  }

  public function refreshCourse()
  {
    $this->course->refresh();
  }

  public function setContent()
  {
    $this->dispatchBrowserEvent('fetch-content', ['content' => $this->course->description]);
  }

  public function contentUpdate($description)
  {
    $this->course->description = $description;
  }

  public function validateSuitableTargets()
  {
    if (count($this->course->courseSuitableTargets) > count($this->suitableTargets)) {
      $count = count($this->course->courseSuitableTargets) - count($this->suitableTargets);
      $length = count($this->course->courseSuitableTargets);

      while ($count != 0) {
        $this->course->courseSuitableTargets[$length - 1]->delete();
        $length--;
        $count--;
      }

      $this->course->refresh();
    } else if (count($this->suitableTargets) > count($this->course->courseSuitableTargets)) {
      $count = count($this->suitableTargets) - count($this->course->courseSuitableTargets);
      $length = count($this->suitableTargets);

      while ($count != 0) {
        CourseSuitableTarget::create([
          'course_id' => $this->course->id,
          'target' => $this->suitableTargets[$length - 1]
        ]);

        $length--;
        $count--;
      }

      $this->course->refresh();
    }

    $checker = false;
    foreach ($this->course->courseSuitableTargets as $index => $courseSuitableTarget) {
      if ($courseSuitableTarget->target != $this->suitableTargets[$index]) {
        $checker = true;
        $courseSuitableTarget->target = $this->suitableTargets[$index];
      }
    }

    if ($checker) {
      $this->course->courseSuitableTargets->each->save();
    }
  }

  public function validatePrerequisites()
  {
    if (count($this->course->coursePrerequisites) > count($this->prerequisites)) {
      $count = count($this->course->coursePrerequisites) - count($this->prerequisites);
      $length = count($this->course->coursePrerequisites);

      while ($count != 0) {
        $this->course->coursePrerequisites[$length - 1]->delete();
        $length--;
        $count--;
      }

      $this->course->refresh();
    } else if (count($this->prerequisites) > count($this->course->coursePrerequisites)) {
      $count = count($this->prerequisites) - count($this->course->coursePrerequisites);
      $length = count($this->prerequisites);

      while ($count != 0) {
        CoursePrerequisite::create([
          'course_id' => $this->course->id,
          'prerequisite' => $this->prerequisites[$length - 1]
        ]);

        $length--;
        $count--;
      }

      $this->course->refresh();
    }

    $checker = false;
    foreach ($this->course->coursePrerequisites as $index => $coursePrerequisite) {
      if ($coursePrerequisite->prerequisite != $this->prerequisites[$index]) {
        $checker = true;
        $coursePrerequisite->prerequisite = $this->prerequisites[$index];
      }
    }

    if ($checker) {
      $this->course->coursePrerequisites->each->save();
    }
  }


  public function addSuitableTarget()
  {
    array_push($this->suitableTargets, '');
  }

  public function deleteSuitableTarget($index)
  {
    array_splice($this->suitableTargets, $index, 1);
  }

  public function addPrerequisite()
  {
    array_push($this->prerequisites, '');
  }

  public function deletePrerequisite($index)
  {
    array_splice($this->prerequisites, $index, 1);
  }

  public function save()
  {
    $this->validateSuitableTargets();
    $this->validatePrerequisites();
    $this->dispatchBrowserEvent('update-content');
    $this->course->student_targets = implode(',', $this->studentTargets);
    $this->displayStudentTargets = $this->studentTargets;
    $this->course->save();
    $this->course->refresh();
  }

  public function render()
  {
    return view('livewire.dashboard.creator.manage-course.course.information-edit');
  }
}
