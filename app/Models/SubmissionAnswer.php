<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubmissionAnswer extends Model
{
    use HasFactory;
	
	protected $guarded = [];
	
	public function assignmentQuestion()
	{
		return $this->belongsTo(AssignmentQuestion::class);
	}
	
	public function assignmentSubmission()
	{
		return $this->belongsTo(AssignmentSubmission::class);
	}
}
