@extends('layouts.master-dashboard')

@section('title', 'Course Details')

@section('content')
	{{-- Header Page Start --}}
	<div class="pt-24 lg:pt-28 lg:px-16">
		<div class="container">
			<div class="flex items-center justify-center flex-col my-4 space-y-8">
				<div class="bg-cyan-100 px-4 py-1 rounded-full">
					<h5 class="font-medium text-md text-cyan-600">{{ $course->type->name }}</h5>
				</div>
				
				<div class="flex items-center justify-center flex-col space-y-4">
					<h2 class="font-semibold text-3xl text-dark text-center max-w-lg">{{ $course->title }}</h2>
					<!-- <h5 class="text-md text-secondary text-center max-w-sm">Mastering English conversation as well as native speaker</h5> -->
				</div>
			</div>
		</div>
	</div>
	{{-- Header Page End --}}
	
	{{-- Main Page Start --}}
	<div class="my-12 lg:px-16">
		<div class="container max-w-7xl">
			<div class="grid grid-cols-1 rounded-md lg:grid-cols-3 lg:bg-white">
				<div class="bg-slate-100 w-full h-min lg:rounded-br-xl lg:col-span-2">
					<div class="w-full rounded-md overflow-hidden shrink-0 pb-4 lg:pr-4">
						<img src="{{ $course->thumbnail }}" alt="Web Design" class="w-full rounded-md object-cover">
					</div>
				</div>
				
				<div class="bg-white rounded-md p-8 space-y-8 lg:row-span-4">
					<div class="flex flex-nowrap flex-col items-center justify-center space-y-2">
						<h4 class="font-normal text-sm text-primary">Course Sessions</h4>
						<h5 class="flex flex-nowrap items-center space-x-2">
							<span class="material-symbols-outlined font-semibold text-lime-500">
								calendar_month
							</span>
							
							<span class="font-normal text-dark">
								{{ $days }} Days {{ $sessionCount }} Meetings
							</span>
						</h5>
					</div>
					
					<div class="flex flex-nowrap flex-col items-center justify-center space-y-2">
						<h4 class="font-normal text-sm text-primary">Material Accessibility</h4>
						<h5 class="flex flex-nowrap items-center space-x-2">
							<span class="material-symbols-outlined font-semibold text-fuchsia-500">
								all_inclusive
							</span>
							
							<span class="font-normal text-dark">
								Lifetime Access
							</span>
						</h5>
					</div>
					
					<div class="flex flex-nowrap flex-col items-center justify-center space-y-2">
						<h4 class="font-normal text-sm text-primary">Certificate</h4>
						<h5 class="flex flex-nowrap items-center space-x-2">
							<span class="material-symbols-outlined font-semibold text-indigo-500">
								verified
							</span>
							
							<span class="font-normal text-dark">
								Permanent Certification
							</span>
						</h5>
					</div>
					
					<div class="flex flex-nowrap flex-col items-center justify-center space-y-2">
						<h4 class="font-normal text-sm text-primary">Total Students</h4>
						<h5 class="flex flex-nowrap items-center space-x-2">
							<span class="material-symbols-outlined font-semibold text-blue-500">
								person
							</span>
							
							<span class="font-normal text-dark">
								{{ count($course->myCourses) }} Joined
							</span>
						</h5>
					</div>
					
					<div class="flex flex-nowrap flex-col items-center justify-center space-y-2">
						<h4 class="font-normal text-sm text-primary">Ratings</h4>
						<h5 class="flex flex-nowrap items-center space-x-2">
							<span class="material-symbols-outlined font-semibold text-yellow-400">
								star
							</span>
							
							<span class="font-normal text-dark">
								4.8/5 of 11.111 Reviews
							</span>
						</h5>
					</div>
					
					<div class="flex flex-nowrap flex-col items-center justify-center space-y-2">
						<h4 class="font-normal text-sm text-primary">Consultation Group</h4>
						<h5 class="flex flex-nowrap items-center space-x-2">
							<span class="material-symbols-outlined font-semibold text-sky-500">
								done_all
							</span>
							
							<span class="font-normal text-dark">
								Free Access
							</span>
						</h5>
					</div>
					
					<div class="flex flex-nowrap flex-col items-center justify-center space-y-2">
						<h4 class="font-normal text-sm text-primary">Material Maintenance</h4>
						<h5 class="flex flex-nowrap items-center space-x-2">
							<span class="material-symbols-outlined font-semibold text-sky-500">
								done_all
							</span>
							
							<span class="font-normal text-dark">
								Always Up-to-date
							</span>
						</h5>
					</div>
				</div>
				
				<div class="bg-white rounded-md p-4 lg:col-span-2">
					<h4 class="font-semibold text-lg text-dark py-2">Best suits for:</h4>
					
					<ul class="list-disc grid grid-cols-1 gap-x-8 gap-y-4 pl-4 lg:grid-cols-2">
						@foreach ($course->courseSuitableTargets as $index => $courseSuitableTarget)
              <li>{{ $courseSuitableTarget->target }}</li>
            @endforeach
					</ul>
				</div>
				
				<div class="bg-white rounded-md p-4">
					<h4 class="font-semibold text-lg text-dark py-2">Prerequisites:</h4>
					
					<ul class="list-disc grid grid-cols-1 gap-y-4 pl-4">
            @foreach ($course->coursePrerequisites as $index => $coursePrerequisite)
              <li>{{ $coursePrerequisite->prerequisite }}</li>
            @endforeach
					</ul>
				</div>
				
				@livewire('dashboard.student.all-course.join-course', ['course' => $course])
			</div>
			
			<div class="my-4 grid grid-cols-1 gap-8 lg:grid-cols-4 xl:grid-cols-3">
				<div class="p-4 space-y-4 lg:col-span-3 xl:col-span-2">
					<h4 class="font-semibold text-2xl text-dark">TL;DR</h4>
					
					<div class="space-y-4">
						{!! $course->description !!}
					</div>
				</div>
				
				<div class="p-4 space-y-4 lg:row-span-2">
					<h4 class="font-semibold text-2xl text-dark">Sneak Peek</h4>
					
					<div class="grid grid-cols-2 gap-2 lg:grid-cols-1">
						@foreach (explode(',', $course->images) as $index => $image)
              <div class="bg-white p-2 w-full rounded-md overflow-hidden shrink-0">
                <img src="{{ $image }}" alt="Web Design" class="w-full rounded-md object-cover">
              </div>
            @endforeach
					</div>
				</div>
				
				<div class="p-4 space-y-4 lg:col-span-3 xl:col-span-2">
					<h4 class="font-semibold text-2xl text-dark">Mentors</h4>
					
					<div class="grid grid-cols-1 gap-4 lg:grid-cols-2 xl:gap-8">
						<div class="bg-white p-4 shrink rounded-xl shadow-lg space-y-4">
							<div class="flex flex-nowrap items-center px-2 space-x-2">
								<div class="p-1 rounded-full overflow-hidden shrink-0 border-2 border-primary">
									<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Mentor Profile" class="w-20 h-20 rounded-full overflow-hidden object-cover">
								</div>
								
								<div class="block">
									<h4 class="font-medium text-dark text-md">{{ $course->user->name }}</h4>
									<h5 class="text-secondary text-sm">English Enthusiast</h5>
								</div>
							</div>
							
							<div class="flex items-center justify-around">
								<div class="block">
									<span class="block mx-auto text-secondary text-sm text-center">
										Courses
									</span>
									
									<span class="block mx-auto text-dark text-lg text-center">
										{{ count($course->user->courses) }}
									</span>
								</div>
								
								<div class="block">
									<span class="block mx-auto text-secondary text-sm text-center">
										Students
									</span>
									
									<span class="block mx-auto text-dark text-lg text-center">
										{{ $studentCount }}
									</span>
								</div>
							</div>
							
							<div class="flex items-center justify-center w-full">
								<a href="#" class="bg-primary w-full text-white text-medium text-center rounded-xl mx-4 py-2 transition duration-300 hover:ring hover:ring-primary/70">Mentor Profile</a>
							</div>
						</div>
						
            @foreach ($course->courseAssistants as $index => $courseAssistant)
              <div class="bg-white p-4 shrink rounded-xl shadow-lg space-y-4">
                <div class="flex flex-nowrap items-center px-2 space-x-2">
                  <div class="p-1 rounded-full overflow-hidden shrink-0 border-2 border-primary">
                    <img src="{{ url('https://randomuser.me/api/portraits/men/23.jpg') }}" alt="Mentor Profile" class="w-20 h-20 rounded-full overflow-hidden object-cover">
                  </div>
                  
                  <div class="block">
                    <h4 class="font-medium text-dark text-md">{{ $courseAssistant->user->name }}</h4>
                    <h5 class="text-secondary text-sm">English Enthusiast</h5>
                  </div>
                </div>
                
                <div class="flex items-center justify-around">
                  <div class="block">
                    <span class="block mx-auto text-secondary text-sm text-center">
                      Courses
                    </span>
                    
                    <span class="block mx-auto text-dark text-lg text-center">
                      {{ count($courseAssistant->user->courses) }}
                    </span>
                  </div>
                  
                  <div class="block">
                    <span class="block mx-auto text-secondary text-sm text-center">
                      Students
                    </span>
                    
                    <span class="block mx-auto text-dark text-lg text-center">
                      {{ $assistantStudent[$index] }}
                    </span>
                  </div>
                </div>
                
                <div class="flex items-center justify-center w-full">
                  <a href="#" class="bg-primary w-full text-white text-medium text-center rounded-xl mx-4 py-2 transition duration-300 hover:ring hover:ring-primary/70">Mentor Profile</a>
                </div>
              </div>
            @endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- Main Page End --}}
	
	{{-- FAQ Section Start --}}
	<div class="my-12 lg:px-16">
		<div class="container lg:max-w-3xl xl:max-w-4xl">
			<div class="mx-auto grid grid-cols-1 gap-16 lg:grid-cols-5">
				<div class="block space-y-8 lg:col-span-2">
					<div class="block space-y-2">
						<h4 class="font-semibold text-2xl text-dark">Frequently Asked Questions</h4>
						<h5 class="text-secondary">Great action of knowledge and skills invesment with the expert of its field</h5>
					</div>
					
					<div>
						<a href="#" class="bg-slate-200 text-dark px-8 py-2 rounded-full shadow-lg hover:ring hover:ring-secondary/50">Contact Us</a>
					</div>
				</div>
				
				<div class="block space-y-4 lg:col-span-3">
					<div x-data="{open: false}" class="bg-white rounded-xl shadow-md">
						<button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full p-4">
							<h4 class="font-medium text-dark text-md">What is Lifetime Course?</h4>
							
							<span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
								expand_more
							</span>
						</button>
						
						<h5 x-show="open" x-transition class="p-4 text-dark">Lifetime course is type of course with lifetime accessibility and maintained. Consultation are held in a lifetime consultation group without any limitation following the provision of mentor</h5>
					</div>
					
					<div x-data="{open: false}" class="bg-white rounded-xl shadow-md">
						<button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full p-4">
							<h4 class="font-medium text-dark text-md">What is Intensive Course?</h4>
							
							<span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
								expand_more
							</span>
						</button>
						
						<h5 x-show="open" x-transition class="p-4 text-dark">Intensive course is an intensive learning on certain schedule. Mentors are obligated to help students during the period of intensive learning. Depends on the mentors, students are still able to ask eventhough the period has over. However, the materials are still accessible and maintained</h5>
					</div>
					
					<div x-data="{open: false}" class="bg-white rounded-xl shadow-md">
						<button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full p-4">
							<h4 class="font-medium text-dark text-md">What is Subscription Course?</h4>
							
							<span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
								expand_more
							</span>
						</button>
						
						<h5 x-show="open" x-transition class="p-4 text-dark">Subscription course is a collection of many maintained courses that can be accessed by subscriber. Consultation are held in a limited consultation group following the provision of mentor</h5>
					</div>
					
					<div x-data="{open: false}" class="bg-white rounded-xl shadow-md">
						<button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full p-4">
							<h4 class="font-medium text-dark text-md">What is the certification requirements?</h4>
							
							<span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
								expand_more
							</span>
						</button>
						
						<h5 x-show="open" x-transition class="p-4 text-dark">You can be permanently certificated in all courses as long as you finished the final assignment</h5>
					</div>
					
					<div x-data="{open: false}" class="bg-white rounded-xl shadow-md">
						<button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full p-4">
							<h4 class="font-medium text-dark text-md">How to become an assistant mentor?</h4>
							
							<span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
								expand_more
							</span>
						</button>
						
						<h5 x-show="open" x-transition class="p-4 text-dark">Be active and struggle for the greatest student! Mentor will invite you through e-mail if you are selected</h5>
					</div>
					
					<div x-data="{open: false}" class="bg-white rounded-xl shadow-md">
						<button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full p-4">
							<h4 class="font-medium text-dark text-md">will I get paid if I become an assistant mentor?</h4>
							
							<span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
								expand_more
							</span>
						</button>
						
						<h5 x-show="open" x-transition class="p-4 text-dark">Of course! Negotiation are held between mentor and assistant candidate</h5>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- FAQ Section End --}}
	
	{{-- Footer Section Start --}}
	<div class="bg-white">
		<div class="flex justify-center w-full p-2">
			<span class="material-symbols-outlined font-medium text-sm align-text-top text-secondary">
				copyright
			</span>
			
			<span class="text-secondary text-medium">
				Powered by Serpihan
			</span>
		</div>
	</div>
	{{-- FOoter Section End --}}
@endsection