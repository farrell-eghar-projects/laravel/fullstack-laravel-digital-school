@extends('layouts.master-dashboard')

@section('title', 'All Courses')

@section('content')
	{{-- Header Page Start --}}
	<div class="pt-24 lg:pt-28 lg:px-16">
		<div class="container">
			<div class="bg-white px-4 pt-4 my-4 rounded-md shadow-md space-y-8">
				<div class="block">
					<h3 class="font-bold text-lg text-dark md:text-2xl">Courses Catalog</h3>
					<h5 class="text-sm text-secondary">Learn the latest knowledge from mentors who experienced in the field</h5>
				</div>
				
				<div class="block">
					<div class="block relative max-w-lg">
						<input type="text" placeholder="Course subject" class="default-input w-full max-w-lg">
						<button type="button" class="absolute top-1/2 right-0 -translate-y-1/2 rounded-r-lg flex items-center bg-primary p-2 ring-primary ring-1 border-primary space-x-1">
							<span class="material-symbols-outlined font-medium text-white">
								search
							</span>
							
							<span class="text-white text-md">
								Search
							</span>
						</button>
					</div>
					
					<div class="flex py-4 space-x-4 overflow-x-auto">
						<button class="bg-slate-200 py-1 px-8 rounded-3xl font-medium text-center text-dark hover:bg-dark hover:text-slate-100">
							Laravel
						</button>
						
						<button class="bg-slate-200 py-1 px-8 rounded-3xl font-medium text-center text-dark hover:bg-dark hover:text-slate-100">
							Laravel
						</button>
						
						<button class="bg-slate-200 py-1 px-8 rounded-3xl font-medium text-center text-dark hover:bg-dark hover:text-slate-100">
							Laravel
						</button>
						
						<button class="bg-slate-200 py-1 px-8 rounded-3xl font-medium text-center text-dark hover:bg-dark hover:text-slate-200">
							Laravel
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- Header Page End --}}
	
	{{-- Main Content Start --}}
	<div x-data="{open: false}" class="lg:px-16">
		<div class="container">
			<div class="grid grid-cols-1 gap-8 my-8 lg:grid-cols-3 xl:grid-cols-11">
				{{-- Sort Filter Start --}}
				<div class="hidden sticky top-16 bg-white p-4 rounded-md shadow-md h-min space-y-8 lg:block lg:col-span-1 xl:col-span-2">
					<div class="block space-y-4">
						<h3 class="font-semibold text-dark text-xl">Sort & Filter</h3>
						
						<div class="block space-y-2">
							<div class="flex items-center px-2 space-x-2">
								<input type="radio" name="sort" id="latest_release" value="Latest Release" class="checked:text-primary focus:outline-none focus:ring focus:ring-primary/50 focus:ring-offset-0">
								<label for="latest_release" class="text-secondary">Latest Release</label>
							</div>
							
							<div class="flex items-center px-2 space-x-2">
								<input type="radio" name="sort" id="most_popular" value="Most Popular" class="checked:text-primary focus:outline-none focus:ring focus:ring-primary/50 focus:ring-offset-0">
								<label for="most_popular" class="text-secondary">Most Popular</label>
							</div>
							
							<div class="flex items-center px-2 space-x-2">
								<input type="radio" name="sort" id="flash_sale" value="Flash Sale" class="checked:text-primary focus:outline-none focus:ring focus:ring-primary/50 focus:ring-offset-0">
								<label for="flash_sale" class="text-secondary">Flash Sale</label>
							</div>
						</div>
					</div>
					
					<div class="block space-y-4">
						<h3 class="font-semibold text-dark text-xl">Grade Difficulty</h3>
						
						<div class="block space-y-2">
							<div class="flex items-center px-2 space-x-2">
								<input type="checkbox" name="highschool" id="highschool" value="Highschool" class="default-input-checkbox">
								<label for="highschool" class="text-secondary">Highschool</label>
							</div>
							
							<div class="flex items-center px-2 space-x-2">
								<input type="checkbox" name="college" id="college" value="College" class="default-input-checkbox">
								<label for="college" class="text-secondary">College</label>
							</div>
							
							<div class="flex items-center px-2 space-x-2">
								<input type="checkbox" name="specialty" id="specialty" value="Specialty" class="default-input-checkbox">
								<label for="specialty" class="text-secondary">Specialty</label>
							</div>
						</div>
					</div>
					
					<div class="block space-y-4">
						<h3 class="font-semibold text-dark text-xl">Type</h3>
						
						<div class="block space-y-2">
							<div class="flex items-center px-2 space-x-2">
								<input type="checkbox" name="lifetime_course" id="lifetime_course" value="Lifetime Course" class="default-input-checkbox">
								<label for="lifetime_course" class="text-secondary whitespace-nowrap">Lifetime Course</label>
							</div>
							
							<div class="flex items-center px-2 space-x-2">
								<input type="checkbox" name="intensive_course" id="intensive_course" value="Intensive Course" class="default-input-checkbox">
								<label for="intensive_course" class="text-secondary whitespace-nowrap">Intensive Course</label>
							</div>
							
							<div class="flex items-center px-2 space-x-2">
								<input type="checkbox" name="subscribe_course" id="subscribe_course" value="Subscribe Course" class="default-input-checkbox">
								<label for="subscribe_course" class="text-secondary whitespace-nowrap">Subscribe Course</label>
							</div>
						</div>
					</div>
				</div>
				{{-- Sort Filter End --}}
				
				{{-- Course List Start --}}
				<div class="bg-white grid grid-cols-1 gap-8 p-4 rounded-md shadow-md sm:grid-cols-2 lg:col-span-2 xl:grid-cols-3 xl:col-span-9">
          @foreach ($courses as $index => $course)
            @if (!in_array($course->id, $myCoursesID))
              <a href="{{ route('student.all-courses.show', ['course' => $course->id]) }}" class="group bg-white w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
                <div class="w-full rounded-md overflow-hidden shrink-0">
                  <img src="{{ $course->thumbnail }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
                </div>
                
                <div class="p-4 space-y-4">
                  <div class="block">
                    <h3 class="font-bold text-lg text-dark">{{ $course->title }}</h3>
                    <h5 class="text-md text-dark">Rp. 999.000,-</h5>
                  </div>
                  
                  <div class="block">
                    <div class="flex flex-nowrap items-center space-x-2">
                      <div class="flex flex-nowrap">
                        <span class="material-symbols-outlined text-3xl text-yellow-500">
                          star
                        </span>
                        
                        <span class="material-symbols-outlined text-3xl text-yellow-500">
                          star
                        </span>
                        
                        <span class="material-symbols-outlined text-3xl text-yellow-500">
                          star
                        </span>
                        
                        <span class="material-symbols-outlined text-3xl text-yellow-500">
                          star
                        </span>
                        
                        <span class="material-symbols-outlined text-3xl text-yellow-500">
                          star_half
                        </span>
                      </div>
                      
                      <span class="w-1/2 text-dark text-sm whitespace-nowrap">(4.89 / 19.999)</span>
                    </div>
                    
                    <div class="flex flex-nowrap items-center space-x-2">
                      <span class="material-symbols-outlined text-2xl text-blue-500">
                        person
                      </span>
                      
                      <span class="w-1/2 text-secondary text-sm whitespace-nowrap">{{ count($course->myCourses) }} students</span>
                    </div>
                  </div>
                </div>
              </a>
            @endif
          @endforeach
				</div>
				{{-- Course List End --}}
				
				{{-- Sort Filter Mob Ver Start --}}
				<button x-on:click="open = true" class="flex items-center fixed left-1/2 bottom-10 -translate-x-1/2 bg-slate-200 py-2 px-4 divide-x divide-dark/50 rounded-2xl shadow-md lg:hidden">
					<span class="material-symbols-outlined text-dark px-1">
						tune
					</span>
					
					<span class="font-semibold text-dark text-md px-2">
						Sort & Filter
					</span>
				</button>
				
				<div x-show="open" x-transition class="block fixed top-0 right-0 bottom-0 left-0 p-4 bg-white z-20 space-y-8 overflow-y-auto overscroll-none lg:hidden">
					<div class="bg-transparent">
						<span x-on:click="open = false" class="text-dark text-xl float-right p-2 cursor-pointer">X</span>
					</div>
					
					<div class="p-4 space-y-8">
						<div class="block space-y-4">
							<h3 class="font-bold text-dark text-xl">Sort & Filter</h3>
							
							<div class="block space-y-2">
								<div class="flex items-center px-2 space-x-2">
									<input type="radio" name="sort" id="latest_release" value="Latest Release" class="checked:text-primary focus:outline-none focus:ring focus:ring-primary/50 focus:ring-offset-0">
									<label for="latest_release" class="font-semibold text-secondary">Latest Release</label>
								</div>
								
								<div class="flex items-center px-2 space-x-2">
									<input type="radio" name="sort" id="most_popular" value="Most Popular" class="checked:text-primary focus:outline-none focus:ring focus:ring-primary/50 focus:ring-offset-0">
									<label for="most_popular" class="font-semibold text-secondary">Most Popular</label>
								</div>
								
								<div class="flex items-center px-2 space-x-2">
									<input type="radio" name="sort" id="flash_sale" value="Flash Sale" class="checked:text-primary focus:outline-none focus:ring focus:ring-primary/50 focus:ring-offset-0">
									<label for="flash_sale" class="font-semibold text-secondary">Flash Sale</label>
								</div>
							</div>
						</div>
						
						<div class="block space-y-4">
							<h3 class="font-bold text-dark text-xl">Grade Difficulty</h3>
							
							<div class="block space-y-2">
								<div class="flex items-center px-2 space-x-2">
									<input type="checkbox" name="highschool" id="highschool_mob" value="Highschool" class="default-input-checkbox">
									<label for="highschool_mob" class="font-semibold text-secondary">Highschool</label>
								</div>
								
								<div class="flex items-center px-2 space-x-2">
									<input type="checkbox" name="college" id="college_mob" value="College" class="default-input-checkbox">
									<label for="college_mob" class="font-semibold text-secondary">College</label>
								</div>
								
								<div class="flex items-center px-2 space-x-2">
									<input type="checkbox" name="specialty" id="specialty_mob" value="Specialty" class="default-input-checkbox">
									<label for="specialty_mob" class="font-semibold text-secondary">Specialty</label>
								</div>
							</div>
						</div>
						
						<div class="block space-y-4">
							<h3 class="font-bold text-dark text-xl">Type</h3>
							
							<div class="block space-y-2">
								<div class="flex items-center px-2 space-x-2">
									<input type="checkbox" name="lifetime_course" id="lifetime_course" value="Lifetime Course" class="default-input-checkbox">
									<label for="lifetime_course" class="font-semibold text-secondary">Lifetime Course</label>
								</div>
								
								<div class="flex items-center px-2 space-x-2">
									<input type="checkbox" name="intensive_course" id="intensive_course" value="Intensive Course" class="default-input-checkbox">
									<label for="intensive_course" class="font-semibold text-secondary whitespace-nowrap">Intensive Course</label>
								</div>
								
								<div class="flex items-center px-2 space-x-2">
									<input type="checkbox" name="subscribe_course" id="subscribe_course" value="Subscribe Course" class="default-input-checkbox">
									<label for="subscribe_course" class="font-semibold text-secondary whitespace-nowrap">Subscribe Course</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="block mx-auto space-y-2">
						<button x-on:click="open = false" type="button" class="block mx-auto w-3/4 bg-primary font-semibold py-2 text-white rounded-lg">
							Filter Courses
						</button>
						
						<button x-on:click="open = false" type="button" class="block mx-auto w-3/4 bg-slate-200 font-semibold py-2 text-dark rounded-lg">
							Close
						</button>
					</div>
				</div>
				{{-- Sort Filter Mob Ver End --}}
			</div>
		</div>
	</div>
	{{-- Main Content End --}}
@endsection