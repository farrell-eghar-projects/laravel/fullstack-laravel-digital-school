<?php

namespace App\Http\Controllers\Dashboard\Creator\ManageCourses;

use App\Http\Controllers\Controller;
use App\Models\Material;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
  public function show(Material $material)
  {
    return view('pages.dashboard.creator.manage-courses.material.show', compact('material'));
  }
}
