@extends('layouts.master-dashboard')

@section('title', 'Assignments')

@section('content')
	{{-- Breadcrumb Start --}}
	@livewire('dashboard.components.breadcrumb', ['routes' => [
			[
				'link' => route('creator.manage-courses.index'),
				'name' => 'Manage Courses'
			],
			[
				'link' => route('creator.manage-courses.course.show', ['course' => $course->id]),
				'name' => 'Course Details'
			],
			[
				'link' => route('creator.manage-courses.course.assignment.show', ['course' => $course->id, 'assignment' => $assignment->id]),
				'name' => 'Assignment Submissions'
			]
		]
	])
	{{-- Breadcrumb End --}}
	
	<div class="lg:px-32">
		<div class="container">
			<div class="block bg-white my-2 px-8 py-4 rounded-md shadow-lg">
				{{-- Header Page Start --}}
				<h3 class="font-semibold text-xl text-dark text-center p-4 max-w-xl mx-auto lg:text-2xl">{{ $assignment->name }}</h3>
				
				<div class="block mt-4 space-y-2 sm:mt-8 sm:space-y-0 sm:flex sm:items-center sm:justify-between">
					<div class="space-y-2">
						<div class="w-56 grid grid-cols-3">
							<h4 class="col-span-2 text-xs text-dark whitespace-nowrap">Scored Assignments</h4>
							<span class="flex flex-nowrap space-x-2">
								<h5 class="text-xs">:</h5>
								<h5 class="text-xs text-secondary">{{ $scoredSubmission }}</h5>
							</span>
						</div>
						
						<div class="w-56 grid grid-cols-3">
							<h4 class="col-span-2 text-xs text-dark whitespace-nowrap">Unscored Assignments</h4>
							<span class="flex flex-nowrap space-x-2">
								<h5 class="text-xs">:</h5>
								<h5 class="text-xs text-secondary">{{ count($assignmentSubmissions) - $scoredSubmission }}</h5>
							</span>
						</div>
					</div>
					
					<select name="assignments_filter" id="assignments_filter" class="bg-transparent text-xs text-secondary border-0 border-b border-slate-200 focus:outline-none focus:ring-0 focus:border-slate-100 sm:text-sm">
						<option value="all">All</option>
						<option value="scored">Scored</option>
						<option value="unscored">Unscored</option>
					</select>
				</div>
				{{-- Header Page End --}}
				
				{{-- Main Page Start --}}
				<div class="block my-4 rounded-lg shadow-md overflow-x-auto scrollbar">
					<table class="w-full sm:table-fixed">
						<thead class="bg-slate-100 text-xs text-dark text-center uppercase">
							<tr>
								<th class="px-4 py-3 sm:w-64">Students</th>	
								<th class="hidden px-2 py-3 sm:w-full sm:table-cell">Started At</th>
								<th class="hidden px-2 py-3 sm:w-full sm:table-cell">Duration</th>
								<th class="hidden px-2 py-3 md:w-full md:table-cell">Status</th>
								<th class="hidden px-2 py-3 lg:w-full lg:table-cell">Score</th>
								<th class="hidden px-2 py-3 xl:w-full xl:table-cell">Action</th>
							</tr>
						</thead>
						
            @foreach ($assignmentSubmissions as $index => $assignmentSubmission)
              <tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
                <tr>
                  <td class="px-4 py-4">
                    <div class="flex flex-nowrap items-center justify-between space-x-16">
                      <div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
                        <div class="rounded-full overflow-hidden shrink-0">
                          <img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
                        </div>
                        
                        <div class="block">
                          <h4 class="font-medium text-dark">{{ $assignmentSubmission->user->name }}</h4>
                          <h5 class="text-secondary text-sm">{{ $assignmentSubmission->user->email }}</h5>
                        </div>
                      </div>
                      
                      <div class="block">
                        <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary sm:hidden">
                          expand_more
                        </span>
                      </div>
                    </div>
                  </td>
                  <td class="hidden px-2 py-4 text-sm text-dark text-center sm:table-cell">{{ $assignmentSubmission->created_at }}</td>
                  <td class="hidden relative px-2 py-4 text-sm text-dark text-center sm:table-cell">
                    {{ $submissionDurations[$index] }}
                    
                    <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined absolute top-1/2 right-2 -translate-y-1/2 transition duration-300 hover:text-primary md:hidden">
                      expand_more
                    </span>
                  </td>
                  <td class="hidden relative px-2 py-4 text-sm text-dark text-center md:table-cell">
                    {{ $submissionStatus[$index] }}
                    
                    <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined absolute top-1/2 right-2 -translate-y-1/2 transition duration-300 hover:text-primary lg:hidden">
                      expand_more
                    </span>
                  </td>
                  <td class="hidden relative px-2 py-4 text-sm text-dark text-center lg:table-cell">
                    {{ is_null($assignmentSubmission->score) ? 'Unscored' : $assignmentSubmission->score }}
                    
                    <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined absolute top-1/2 right-2 -translate-y-1/2 transition duration-300 hover:text-primary xl:hidden">
                      expand_more
                    </span>
                  </td>
                  <td class="hidden px-2 py-4 text-dark text-center xl:table-cell">
                    <div class="flex flex-nowrap items-center justify-center">
                      <a href="{{ route('creator.manage-courses.course.assignment.submission.show', ['course' => $course->id, 'assignment' => $assignment->id, 'assignmentSubmission' => $assignmentSubmission->id]) }}" x-on:click.stop class="flex flex-nowrap items-center p-1 bg-primary rounded-md transition duration-300 hover:ring hover:ring-primary/70 hover:shadow-md">
                        <span class="material-symbols-outlined text-white">
                          biotech
                        </span>
                        
                        <span class="text-white px-px">
                          Inspect
                        </span>
                      </a>
                    </div>
                  </td>
                </tr>
                
                <tr x-show="expand" x-transition class="xl:hidden">
                  <td colspan="6">
                    <div class="grid grid-cols-12 px-4 py-2 sm:hidden">
                      <h4 class="col-span-5 font-medium text-dark whitespace-nowrap">Started At</h4>
                      <span class="col-span-7 flex flex-nowrap space-x-2">
                        <h5>:</h5>
                        <h5 class="text-secondary">{{ $assignmentSubmission->created_at }}</h5>
                      </span>
                    </div>
                    
                    <div class="grid grid-cols-12 px-4 py-2 sm:hidden">
                      <h4 class="col-span-5 font-medium text-dark whitespace-nowrap">Duration</h4>
                      <span class="col-span-7 flex flex-nowrap space-x-2">
                        <h5>:</h5>
                        <h5 class="text-secondary">{{ $submissionDurations[$index] }}</h5>
                      </span>
                    </div>
                    
                    <div class="grid grid-cols-12 px-4 py-2 sm:flex sm:flex-nowrap md:hidden">
                      <h4 class="col-span-5 basis-16 font-medium text-dark whitespace-nowrap">Status</h4>
                      <span class="col-span-7 flex flex-nowrap space-x-2">
                        <h5>:</h5>
                        <h5 class="text-secondary">{{ $submissionStatus[$index] }}</h5>
                      </span>
                    </div>
                    
                    <div class="grid grid-cols-12 px-4 py-2 sm:flex sm:flex-nowrap lg:hidden">
                      <h4 class="col-span-5 basis-16 font-medium text-dark whitespace-nowrap">Score</h4>
                      <span class="col-span-7 flex flex-nowrap space-x-2">
                        <h5>:</h5>
                        <h5 class="text-secondary">{{ is_null($assignmentSubmission->score) ? 'Unscored' : $assignmentSubmission->score }}</h5>
                      </span>
                    </div>
                    
                    <a href="{{ route('creator.manage-courses.course.assignment.submission.show', ['course' => $course->id, 'assignment' => $assignment->id, 'assignmentSubmission' => $assignmentSubmission->id]) }}" x-on:click.stop class="flex flex-nowrap items-center justify-center w-24 mx-4 my-2 p-1 bg-primary rounded-md transition duration-300 hover:ring hover:ring-primary/70 hover:shadow-md">
                      <span class="material-symbols-outlined text-white">
                        biotech
                      </span>
                      
                      <span class="text-white px-px">
                        Inspect
                      </span>
                    </a>
                  </td>
                </tr>
              </tbody>
            @endforeach
					</table>
				</div>
				{{-- Main Page End --}}
			</div>
		</div>
	</div>
@endsection