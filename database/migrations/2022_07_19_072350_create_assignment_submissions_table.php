<?php

use App\Models\CourseAssignment;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('assignment_submissions', function (Blueprint $table) {
      $table->id();
      $table->foreignIdFor(User::class)->nullable();
      $table->foreignIdFor(CourseAssignment::class)->nullable();
      $table->integer('score')->nullable();
      $table->boolean('status')->nullable();
      $table->timestamp('duration')->nullable();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
      $table->foreign('course_assignment_id')->references('id')->on('course_assignments')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('assignment_submissions');
  }
};
