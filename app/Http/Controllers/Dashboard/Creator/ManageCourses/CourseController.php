<?php

namespace App\Http\Controllers\Dashboard\Creator\ManageCourses;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\CoursePrerequisite;
use App\Models\CourseSuitableTarget;
use Illuminate\Http\Request;

class CourseController extends Controller
{
	public function create(Course $course)
	{
		return view('pages.dashboard.creator.manage-courses.course.create', compact('course'));
	}
	
	public function store()
	{
		$course = Course::create([
			'user_id' => auth()->id(),
			'description' => ''
		]);
		
		CoursePrerequisite::create([
			'course_id' => $course->id
		]);
		
		CourseSuitableTarget::create([
			'course_id' => $course->id
		]);
		
		return redirect()->route('creator.manage-courses.course.create', ['course' => $course]);
	}
	
    public function show(Course $course)
	{
		return view('pages.dashboard.creator.manage-courses.course.show', compact('course'));
	}
	
	public function destroy(Course $course)
	{
		$course->delete();
		return redirect()->route('creator.manage-courses.index');
	}
}
