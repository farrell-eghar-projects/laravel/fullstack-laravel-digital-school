@extends('layouts.master-dashboard')

@section('title', 'Manage Courses')
	
@section('content')
	<div x-data="{floatMaterial: false}">
		{{-- Header Page Start --}}
		<div class="pt-24 lg:pt-28 lg:px-16">
			<div class="container">
				<div class="block mx-auto max-w-5xl shadow-md">
					<div class="block w-full bg-primary px-4 py-2 rounded-t-md">
						<h3 class="font-semibold text-lg text-white ">Rating & Feedback</h3>
					</div>
					
					<div class="block w-full divide-y px-8 bg-white rounded-b-md">
						<div x-data="{open: false}" class="block py-4 space-y-8 lg:grid lg:grid-cols-3 lg:gap-8 lg:space-y-0">
							<div x-on:click="open = !open" class="flex items-center justify-between cursor-default lg:space-x-4 lg:justify-start lg:col-span-1">
								<div class="hidden w-32 h-20 rounded-md overflow-hidden lg:block">
									<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full h-full rounded-md object-cover">
								</div>
								
								<div class="block space-y-1">
									<h5 class="font-medium text-dark line-clamp-2">Easy English Interview with Native Speaker</h5>
									
									<div class="flex flex-nowrap">
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-slate-300 text-xl">
											star
										</span>
									</div>
									
									<p class="text-secondary text-xs">2022-06-22 13:25 WIB</p>
								</div>
								
								<button x-bind:class="{'text-primary rotate-180': open}" type="button" class="material-symbols-outlined float-right transition duration-300 lg:hidden">
									expand_more
								</button>
							</div>
							
							<div x-show="open" x-transition class="block space-y-1 lg:col-span-2 lg:hidden">
								<h5 class="font-medium text-dark">Farrell Eghar</h5>
								<p class="text-base">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem itaque cumque repellat consequuntur ex, accusantium iure rerum id!</p>
							</div>
							
							<div class="hidden space-y-1 lg:block lg:col-span-2">
								<h5 class="font-medium text-dark">Farrell Eghar</h5>
								<p class="text-sm">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem itaque cumque repellat consequuntur ex, accusantium iure rerum id!</p>
							</div>
						</div>
						
						<div x-data="{open: false}" class="block py-4 space-y-8 lg:grid lg:grid-cols-3 lg:gap-8 lg:space-y-0">
							<div x-on:click="open = !open" class="flex items-center justify-between cursor-default lg:space-x-4 lg:justify-start lg:col-span-1">
								<div class="hidden w-32 h-20 rounded-md overflow-hidden lg:block">
									<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full h-full rounded-md object-cover">
								</div>
								
								<div class="block space-y-1">
									<h5 class="font-medium text-dark line-clamp-2">Easy English Interview with Native Speaker</h5>
									
									<div class="flex flex-nowrap">
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-slate-300 text-xl">
											star
										</span>
									</div>
									
									<p class="text-secondary text-xs">2022-06-22 13:25 WIB</p>
								</div>
								
								<button x-bind:class="{'text-primary rotate-180': open}" type="button" class="material-symbols-outlined float-right transition duration-300 lg:hidden">
									expand_more
								</button>
							</div>
							
							<div x-show="open" x-transition class="block space-y-1 lg:col-span-2 lg:hidden">
								<h5 class="font-medium text-dark">Farrell Eghar</h5>
								<p class="text-base">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem itaque cumque repellat consequuntur ex, accusantium iure rerum id!</p>
							</div>
							
							<div class="hidden space-y-1 lg:block lg:col-span-2">
								<h5 class="font-medium text-dark">Farrell Eghar</h5>
								<p class="text-sm">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem itaque cumque repellat consequuntur ex, accusantium iure rerum id!</p>
							</div>
						</div>
						
						<div x-data="{open: false}" class="block py-4 space-y-8 lg:grid lg:grid-cols-3 lg:gap-8 lg:space-y-0">
							<div x-on:click="open = !open" class="flex items-center justify-between cursor-default lg:space-x-4 lg:justify-start lg:col-span-1">
								<div class="hidden w-32 h-20 rounded-md overflow-hidden lg:block">
									<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full h-full rounded-md object-cover">
								</div>
								
								<div class="block space-y-1">
									<h5 class="font-medium text-dark line-clamp-2">Easy English Interview with Native Speaker</h5>
									
									<div class="flex flex-nowrap">
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-slate-300 text-xl">
											star
										</span>
									</div>
									
									<p class="text-secondary text-xs">2022-06-22 13:25 WIB</p>
								</div>
								
								<button x-bind:class="{'text-primary rotate-180': open}" type="button" class="material-symbols-outlined float-right transition duration-300 lg:hidden">
									expand_more
								</button>
							</div>
							
							<div x-show="open" x-transition class="block space-y-1 lg:col-span-2 lg:hidden">
								<h5 class="font-medium text-dark">Farrell Eghar</h5>
								<p class="text-base">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem itaque cumque repellat consequuntur ex, accusantium iure rerum id!</p>
							</div>
							
							<div class="hidden space-y-1 lg:block lg:col-span-2">
								<h5 class="font-medium text-dark">Farrell Eghar</h5>
								<p class="text-sm">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem itaque cumque repellat consequuntur ex, accusantium iure rerum id!</p>
							</div>
						</div>
						
						<div x-data="{open: false}" class="block py-4 space-y-8 lg:grid lg:grid-cols-3 lg:gap-8 lg:space-y-0">
							<div x-on:click="open = !open" class="flex items-center justify-between cursor-default lg:space-x-4 lg:justify-start lg:col-span-1">
								<div class="hidden w-32 h-20 rounded-md overflow-hidden lg:block">
									<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full h-full rounded-md object-cover">
								</div>
								
								<div class="block space-y-1">
									<h5 class="font-medium text-dark line-clamp-2">Easy English Interview with Native Speaker</h5>
									
									<div class="flex flex-nowrap">
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-yellow-400 text-xl">
											star
										</span>
										
										<span class="material-symbols-outlined text-slate-300 text-xl">
											star
										</span>
									</div>
									
									<p class="text-secondary text-xs">2022-06-22 13:25 WIB</p>
								</div>
								
								<button x-bind:class="{'text-primary rotate-180': open}" type="button" class="material-symbols-outlined float-right transition duration-300 lg:hidden">
									expand_more
								</button>
							</div>
							
							<div x-show="open" x-transition class="block space-y-1 lg:col-span-2 lg:hidden">
								<h5 class="font-medium text-dark">Farrell Eghar</h5>
								<p class="text-base">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem itaque cumque repellat consequuntur ex, accusantium iure rerum id!</p>
							</div>
							
							<div class="hidden space-y-1 lg:block lg:col-span-2">
								<h5 class="font-medium text-dark">Farrell Eghar</h5>
								<p class="text-sm">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem itaque cumque repellat consequuntur ex, accusantium iure rerum id!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{-- Header Page End --}}
		
		{{-- Main Page Start --}}
		<div class="lg:px-32">
			<div class="container">
				<div x-data="{open: 'courses'}" class="bg-white shadow-md rounded-md px-12 my-8">
					<div class="flex items-center justify-between border-b border-slate-200">
						<div class="flex flex-wrap items-center">
							<button type="button" x-bind:class="open == 'courses' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'courses'" class="px-4 py-8 mr-4 border-b-2">
								Courses
							</button>
							
							<button type="button" x-bind:class="open == 'materials' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'materials'" class="px-4 py-8 mr-4 border-b-2">
								Materials
							</button>
							
							<button type="button" x-bind:class="open == 'assistants' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'assistants'" class="px-4 py-8 mr-4 border-b-2">
								Assistants
							</button>
						</div>
						
						<div class="hidden items-center py-4 lg:flex">
							<select x-bind:class="open == 'courses' ? '' : 'hidden'" name="course_sort" id="course_sort" class="bg-transparent text-secondary border-0 focus:outline-none focus:ring-0">
								<option value="all">All Courses</option>
								<option value="drafts">Drafts</option>
								<option value="published">Published</option>
							</select>
							
							<select x-bind:class="open == 'materials' ? '' : 'hidden'" name="material_sort" id="material_sort" class="bg-transparent text-secondary border-0 focus:outline-none focus:ring-0">
								<option value="all">All Materials</option>
								<option value="unused">Unused Materials</option>
								<option value="lifetime_course">Lifetime Materials</option>
								<option value="intensive_course">Intensive Materials</option>
								<option value="subscription_course">Subscription Materials</option>
							</select>
							
							<select x-bind:class="open == 'assistants' ? '' : 'hidden'" name="assistant_sort" id="assistant_sort" class="bg-transparent text-secondary border-0 focus:outline-none focus:ring-0">
								<option value="all">All</option>
								<option value="active">Active</option>
								<option value="inactive">Inactive</option>
							</select>
						</div> 
					</div>
					
					<div class="py-4 lg:py-8">
						@auth
						{{-- Manage Courses Start --}}
						<div x-show="open == 'courses'" x-transition class="block">
							<div class="flex items-center justify-end py-4 lg:hidden">
								<select name="course_sort" id="course_sort" class="bg-transparent text-secondary border-0 border-b border-slate-200 focus:outline-none focus:ring-0 focus:border-slate-100">
									<option value="all">All Courses</option>
									<option value="drafts">Drafts</option>
									<option value="published">Published</option>
								</select>
							</div>
							
							<div class="grid grid-cols-1 gap-8 py-4 sm:grid-cols-2 xl:grid-cols-3">
								@foreach ($courses as $index => $course)
									<a href="{{ $course->published ? route('creator.manage-courses.course.show', ['course' => $course->id]) : route('creator.manage-courses.course.create', ['course' => $course->id]) }}" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
										<div class="w-full rounded-md overflow-hidden shrink-0">
											<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
										</div>
										
										<div class="p-4 space-y-2">
											<h3 class="font-semibold text-lg text-dark">{{ empty($course->title) ? 'No Title' : $course->title }}</h3>
											
											<div class="block space-y-1">
												<div class="flex flex-nowrap items-center space-x-2">
													<span class="material-symbols-outlined text-red-500">
														assignment
													</span>
													
													<span class="w-1/2 font-semibold text-red-500 text-xs whitespace-nowrap">12 Unscored Assignments</span>
												</div>
												
												<div class="flex flex-nowrap items-center space-x-2">
													<span class="material-symbols-outlined text-blue-500">
														person
													</span>
													
													<span class="w-1/2 font-semibold text-blue-500 text-xs whitespace-nowrap">99 Students</span>
												</div>
											</div>
											
											<div class="flex flex-nowrap items-end w-full space-x-4">
												<span class="w-1/2 {{ $course->published ? 'bg-green-500' : 'bg-orange-400'}} py-1 rounded-full text-xs text-white text-center">
													{{ $course->published ? 'Published' : 'Drafted' }}
												</span>
												
												<span class="w-1/2 bg-blue-500 py-1 rounded-full text-xs text-white text-center">
													Intensive Course
												</span>
											</div>
										</div>
									</a>
								@endforeach
							</div>
						</div>
						{{-- Manage Courses End --}}
						
						{{-- Manage Materials Start --}}
						<div x-show="open == 'materials'" x-transition class="block">
							<div class="flex items-center justify-end py-4 lg:hidden">
								<select name="material_sort" id="material_sort" class="bg-transparent text-secondary border-0 border-b border-slate-200 focus:outline-none focus:ring-0 focus:border-slate-100">
									<option value="all">All Materials</option>
									<option value="unused">Unused Materials</option>
									<option value="lifetime_course">Lifetime Materials</option>
									<option value="intensive_course">Intensive Materials</option>
									<option value="subscription_course">Subscription Materials</option>
								</select>
							</div> 
							
							<div class="grid grid-cols-1 place-items-center gap-8 py-4 md:grid-cols-2 2xl:grid-cols-3">
								@foreach ($materials as $index => $material)
									<div class="bg-white w-full rounded-md shadow-lg overflow-hidden max-w-sm">
										<div class="bg-primary px-4 py-2">
											<h3 class="font-semibold text-white text-center">{{ $material->name }}</h3>
										</div>
										
										<div class="block p-4 space-y-4">
											<div class="block space-y-2">
												<div class="grid grid-cols-3 gap-2">
													<div class="block">
														<h5 class="text-sm float-right">: </h5>
														<h4 class="text-sm">Session</h4>
													</div>
													
													<h5 class="text-sm col-span-2"> {{ $sessions[$index] }} Meetings</h5>
												</div>
												
												<div class="grid grid-cols-3 gap-2">
													<div class="block">
														<h5 class="text-sm float-right">: </h5>
														<h4 class="text-sm">Assignment</h4>
													</div>
													
													<h5 class="text-sm col-span-2"> {{ count($material->assignments) }} Submissions</h5>
												</div>
												
												<div class="grid grid-cols-3 gap-2">
													<div class="block">
														<h5 class="text-sm float-right">: </h5>
														<h4 class="text-sm">Type</h4>
													</div>
													
													<h5 class="text-sm col-span-2"> Lifetime, Intensive, Subscription</h5>
												</div>
												
												<div class="grid grid-cols-3 gap-2">
													<div class="block">
														<h5 class="text-sm float-right">: </h5>
														<h4 class="text-sm">Courses</h4>
													</div>
													
													<h5 class="text-sm col-span-2"> {{ count($material->courses ) }} Applied</h5>
												</div>
												
												<div class="grid grid-cols-3 gap-2">
													<div class="block">
														<h5 class="text-sm float-right">: </h5>
														<h4 class="text-sm">Updated</h4>
													</div>
													
													<h5 class="text-sm col-span-2"> {{  date("M d, Y", strtotime($material->updated_at)) }} </h5>
												</div>
											</div>
											
											<div class="flex flex-nowrap items-end justify-center">
												<a href="{{ route('creator.manage-courses.material.show', ['material' => $material->id]) }}" class="flex items-center justify-center bg-gradient-to-br from-primary to-teal-400 py-2 px-4 rounded-md transition duration-300 hover:ring hover:ring-primary/70 hover:shadow-md hover:bg-gradient-to-tl">
													<span class="material-symbols-outlined font-medium text-center text-white">
														biotech
													</span>
													
													<span class="font-medium text-center text-white">
														Inspect
													</span>
												</a>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
						{{-- Manage Materials End --}}
						
						{{-- Manage Assistants Start --}}
						<div x-show="open == 'assistants'" x-transition class="block">
							<div class="flex items-center justify-end py-4 lg:hidden">
								<select name="assistant_sort" id="assistant_sort" class="bg-transparent text-secondary border-0 border-b border-slate-200 focus:outline-none focus:ring-0">
									<option value="all">All</option>
									<option value="active">Active</option>
									<option value="inactive">Inactive</option>
								</select>
							</div> 
							
							<div class="block my-4 rounded-lg shadow-md overflow-x-auto scrollbar">
								<table class="w-full ">
									<thead class="bg-slate-100 text-xs text-dark text-center uppercase">
										<tr>
											<th class="px-6 py-3">Assistant Name</th>
											<th class="px-6 py-3">Course</th>
											<th class="px-6 py-3">Since</th>
											<th class="px-6 py-3">Status</th>
											<th class="px-6 py-3 sr-only">Action</th>
										</tr>
									</thead>
									
									<tbody class="text-md text-secondary text-left">
										<tr class="border-b">
											<td class="px-6 py-4 text-dark font-medium">Mohammed Farrell Eghar Syahputra</td>
											<td class="px-6 py-4">Easy English Interview with Native Speaker</td>
											<td class="px-6 py-4">January, 1<sup>st</sup> 2021</td>
											<td x-data="{active: false}" class="px-6 py-4 text-center">
												<button type="button" x-on:click="active = !active" x-bind:class="active ? 'border-primary/80' : 'border-secondary/80'" class="h-4 w-8 bg-slate-100 border rounded-full py-px px-px">
													<div x-bind:class="active ? 'translate-x-4 bg-primary/50' : 'bg-secondary/50'" class="my-auto h-3 w-3 shadow-sm rounded-full transition duration-300"></div>
												</button>
												
												<h5 x-bind:class="{ 'text-primary': active }" x-text="active ? 'Active' : 'Inactive'">Inactive</h5>
											</td>
											<td class="px-6 py-4">
												<div class="flex flex-nowrap items-center justify-center space-x-2">
													<a href="#" class="flex items-center p-1 bg-orange-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															edit
														</span>
													</a>
													
													<a href="#" class="flex items-center p-1 bg-red-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															delete
														</span>
													</a>
												</div>
											</td>
										</tr>
										
										<tr class="border-b">
											<td class="px-6 py-4 text-dark font-medium">Mohammed Farrell Eghar Syahputra</td>
											<td class="px-6 py-4">Easy English Interview with Native Speaker</td>
											<td class="px-6 py-4">January, 1<sup>st</sup> 2021</td>
											<td x-data="{active: false}" class="px-6 py-4 text-center">
												<button type="button" x-on:click="active = !active" x-bind:class="active ? 'border-primary/80' : 'border-secondary/80'" class="h-4 w-8 bg-slate-100 border rounded-full py-px px-px">
													<div x-bind:class="active ? 'translate-x-4 bg-primary/50' : 'bg-secondary/50'" class="my-auto h-3 w-3 shadow-sm rounded-full transition duration-300"></div>
												</button>
												
												<h5 x-bind:class="{ 'text-primary': active }" x-text="active ? 'Active' : 'Inactive'">Inactive</h5>
											</td>
											<td class="px-6 py-4">
												<div class="flex flex-nowrap items-center justify-center space-x-2">
													<a href="#" class="flex items-center p-1 bg-orange-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															edit
														</span>
													</a>
													
													<a href="#" class="flex items-center p-1 bg-red-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															delete
														</span>
													</a>
												</div>
											</td>
										</tr>
										
										<tr class="border-b">
											<td class="px-6 py-4 text-dark font-medium">Mohammed Farrell Eghar Syahputra</td>
											<td class="px-6 py-4">Easy English Interview with Native Speaker</td>
											<td class="px-6 py-4">January, 1<sup>st</sup> 2021</td>
											<td x-data="{active: false}" class="px-6 py-4 text-center">
												<button type="button" x-on:click="active = !active" x-bind:class="active ? 'border-primary/80' : 'border-secondary/80'" class="h-4 w-8 bg-slate-100 border rounded-full py-px px-px">
													<div x-bind:class="active ? 'translate-x-4 bg-primary/50' : 'bg-secondary/50'" class="my-auto h-3 w-3 shadow-sm rounded-full transition duration-300"></div>
												</button>
												
												<h5 x-bind:class="{ 'text-primary': active }" x-text="active ? 'Active' : 'Inactive'">Inactive</h5>
											</td>
											<td class="px-6 py-4">
												<div class="flex flex-nowrap items-center justify-center space-x-2">
													<a href="#" class="flex items-center p-1 bg-orange-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															edit
														</span>
													</a>
													
													<a href="#" class="flex items-center p-1 bg-red-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															delete
														</span>
													</a>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						{{-- Manage Assistants End --}}
						@endauth
						
						@guest
						{{-- Manage Courses Start --}}
						<div x-show="open == 'courses'" x-transition class="block">
							<div class="flex items-center justify-end py-4 lg:hidden">
								<select name="course_sort" id="course_sort" class="bg-transparent text-secondary border-0 border-b border-slate-200 focus:outline-none focus:ring-0 focus:border-slate-100">
									<option value="all">All Courses</option>
									<option value="drafts">Drafts</option>
									<option value="published">Published</option>
								</select>
							</div>
							
							<div class="grid grid-cols-1 gap-8 py-4 sm:grid-cols-2 xl:grid-cols-3">
								<a href="{{ route('creator.manage-courses.course.show', ['course' => 'tapi-boong-wkwkwk']) }}" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
									<div class="w-full rounded-md overflow-hidden shrink-0">
										<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
									</div>
									
									<div class="p-4 space-y-2">
										<h3 class="font-semibold text-lg text-dark">Easy English Interview with Native Speaker</h3>
										
										<div class="block space-y-1">
											<div class="flex flex-nowrap items-center space-x-2">
												<span class="material-symbols-outlined text-red-500">
													assignment
												</span>
												
												<span class="w-1/2 font-semibold text-red-500 text-xs whitespace-nowrap">12 Unscored Assignments</span>
											</div>
											
											<div class="flex flex-nowrap items-center space-x-2">
												<span class="material-symbols-outlined text-blue-500">
													person
												</span>
												
												<span class="w-1/2 font-semibold text-blue-500 text-xs whitespace-nowrap">99 Students</span>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end w-full space-x-4">
											<span class="w-1/2 bg-green-500 py-1 rounded-full text-xs text-white text-center">
												Published
											</span>
											
											<span class="w-1/2 bg-blue-500 py-1 rounded-full text-xs text-white text-center">
												Intensive Course
											</span>
										</div>
									</div>
								</a>
								
								<a href="{{ route('creator.manage-courses.course.show', ['course' => 'tapi-boong-wkwkwk']) }}" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
									<div class="w-full rounded-md overflow-hidden shrink-0">
										<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
									</div>
									
									<div class="p-4 space-y-2">
										<h3 class="font-semibold text-lg text-dark">Easy English Interview with Native Speaker</h3>
										
										<div class="block space-y-1">
											<div class="flex flex-nowrap items-center space-x-2">
												<span class="material-symbols-outlined text-red-500">
													assignment
												</span>
												
												<span class="w-1/2 font-semibold text-red-500 text-xs whitespace-nowrap">12 Unscored Assignments</span>
											</div>
											
											<div class="flex flex-nowrap items-center space-x-2">
												<span class="material-symbols-outlined text-blue-500">
													person
												</span>
												
												<span class="w-1/2 font-semibold text-blue-500 text-xs whitespace-nowrap">99 Students</span>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end w-full space-x-4">
											<span class="w-1/2 bg-green-500 py-1 rounded-full text-xs text-white text-center">
												Published
											</span>
											
											<span class="w-1/2 bg-blue-500 py-1 rounded-full text-xs text-white text-center">
												Intensive Course
											</span>
										</div>
									</div>
								</a>
								
								<a href="{{ route('creator.manage-courses.course.show', ['course' => 'tapi-boong-wkwkwk']) }}" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
									<div class="w-full rounded-md overflow-hidden shrink-0">
										<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
									</div>
									
									<div class="p-4 space-y-2">
										<h3 class="font-semibold text-lg text-dark">Easy English Interview with Native Speaker</h3>
										
										<div class="block space-y-1">
											<div class="flex flex-nowrap items-center space-x-2">
												<span class="material-symbols-outlined text-red-500">
													assignment
												</span>
												
												<span class="w-1/2 font-semibold text-red-500 text-xs whitespace-nowrap">12 Unscored Assignments</span>
											</div>
											
											<div class="flex flex-nowrap items-center space-x-2">
												<span class="material-symbols-outlined text-blue-500">
													person
												</span>
												
												<span class="w-1/2 font-semibold text-blue-500 text-xs whitespace-nowrap">99 Students</span>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end w-full space-x-4">
											<span class="w-1/2 bg-green-500 py-1 rounded-full text-xs text-white text-center">
												Published
											</span>
											
											<span class="w-1/2 bg-blue-500 py-1 rounded-full text-xs text-white text-center">
												Intensive Course
											</span>
										</div>
									</div>
								</a>
								
								<a href="{{ route('creator.manage-courses.course.show', ['course' => 'tapi-boong-wkwkwk']) }}" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
									<div class="w-full rounded-md overflow-hidden shrink-0">
										<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
									</div>
									
									<div class="p-4 space-y-2">
										<h3 class="font-semibold text-lg text-dark">Easy English Interview with Native Speaker</h3>
										
										<div class="block space-y-1">
											<div class="flex flex-nowrap items-center space-x-2">
												<span class="material-symbols-outlined text-red-500">
													assignment
												</span>
												
												<span class="w-1/2 font-semibold text-red-500 text-xs whitespace-nowrap">12 Unscored Assignments</span>
											</div>
											
											<div class="flex flex-nowrap items-center space-x-2">
												<span class="material-symbols-outlined text-blue-500">
													person
												</span>
												
												<span class="w-1/2 font-semibold text-blue-500 text-xs whitespace-nowrap">99 Students</span>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end w-full space-x-4">
											<span class="w-1/2 bg-green-500 py-1 rounded-full text-xs text-white text-center">
												Published
											</span>
											
											<span class="w-1/2 bg-blue-500 py-1 rounded-full text-xs text-white text-center">
												Intensive Course
											</span>
										</div>
									</div>
								</a>
							</div>
						</div>
						{{-- Manage Courses End --}}
						
						{{-- Manage Materials Start --}}
						<div x-show="open == 'materials'" x-transition class="block">
							<div class="flex items-center justify-end py-4 lg:hidden">
								<select name="material_sort" id="material_sort" class="bg-transparent text-secondary border-0 border-b border-slate-200 focus:outline-none focus:ring-0 focus:border-slate-100">
									<option value="all">All Materials</option>
									<option value="unused">Unused Materials</option>
									<option value="lifetime_course">Lifetime Materials</option>
									<option value="intensive_course">Intensive Materials</option>
									<option value="subscription_course">Subscription Materials</option>
								</select>
							</div> 
							
							<div class="grid grid-cols-1 place-items-center gap-8 py-4 md:grid-cols-2 2xl:grid-cols-3">
								<div class="bg-white w-full rounded-md shadow-lg overflow-hidden max-w-sm">
									<div class="bg-primary px-4 py-2">
										<h3 class="font-semibold text-white text-center">Easy English Interview with Native Speaker</h3>
									</div>
									
									<div class="block p-4 space-y-4">
										<div class="block space-y-2">
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Session</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 32 Meetings</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Assignment</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 8 Submissions</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Type</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> Lifetime, Intensive, Subscription</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Courses</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 30 Applied</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Updated</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> July, 1<sup>st</sup> 2022</h5>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end justify-center">
											<a href="{{ route('creator.manage-courses.material.show', ['material' => 'tapi-boong-wkwkwk']) }}" class="flex items-center justify-center bg-gradient-to-br from-primary to-teal-400 py-2 px-4 rounded-md transition duration-300 hover:ring hover:ring-primary/70 hover:shadow-md hover:bg-gradient-to-tl">
												<span class="material-symbols-outlined font-medium text-center text-white">
													biotech
												</span>
												
												<span class="font-medium text-center text-white">
													Inspect
												</span>
											</a>
										</div>
									</div>
								</div>
								
								<div class="bg-white w-full rounded-md shadow-lg overflow-hidden max-w-sm">
									<div class="bg-primary px-4 py-2">
										<h3 class="font-semibold text-white text-center">Easy English Interview with Native Speaker</h3>
									</div>
									
									<div class="block p-4 space-y-4">
										<div class="block space-y-2">
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Session</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 32 Meetings</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Assignment</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 8 Submissions</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Type</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> Lifetime, Intensive, Subscription</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Courses</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 30 Applied</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Updated</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> July, 1<sup>st</sup> 2022</h5>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end justify-center">
											<a href="{{ route('creator.manage-courses.material.show', ['material' => 'tapi-boong-wkwkwk']) }}" class="flex items-center justify-center bg-gradient-to-br from-primary to-teal-400 py-2 px-4 rounded-md transition duration-300 hover:ring hover:ring-primary/70 hover:shadow-md hover:bg-gradient-to-tl">
												<span class="material-symbols-outlined font-medium text-center text-white">
													biotech
												</span>
												
												<span class="font-medium text-center text-white">
													Inspect
												</span>
											</a>
										</div>
									</div>
								</div>
								
								<div class="bg-white w-full rounded-md shadow-lg overflow-hidden max-w-sm">
									<div class="bg-primary px-4 py-2">
										<h3 class="font-semibold text-white text-center">Easy English Interview with Native Speaker</h3>
									</div>
									
									<div class="block p-4 space-y-4">
										<div class="block space-y-2">
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Session</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 32 Meetings</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Assignment</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 8 Submissions</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Type</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> Lifetime, Intensive, Subscription</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Courses</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 30 Applied</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Updated</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> July, 1<sup>st</sup> 2022</h5>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end justify-center">
											<a href="{{ route('creator.manage-courses.material.show', ['material' => 'tapi-boong-wkwkwk']) }}" class="flex items-center justify-center bg-gradient-to-br from-primary to-teal-400 py-2 px-4 rounded-md transition duration-300 hover:ring hover:ring-primary/70 hover:shadow-md hover:bg-gradient-to-tl">
												<span class="material-symbols-outlined font-medium text-center text-white">
													biotech
												</span>
												
												<span class="font-medium text-center text-white">
													Inspect
												</span>
											</a>
										</div>
									</div>
								</div>
								
								<div class="bg-white w-full rounded-md shadow-lg overflow-hidden max-w-sm">
									<div class="bg-primary px-4 py-2">
										<h3 class="font-semibold text-white text-center">Easy English Interview with Native Speaker</h3>
									</div>
									
									<div class="block p-4 space-y-4">
										<div class="block space-y-2">
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Session</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 32 Meetings</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Assignment</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 8 Submissions</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Type</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> Lifetime, Intensive, Subscription</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Courses</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 30 Applied</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Updated</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> July, 1<sup>st</sup> 2022</h5>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end justify-center">
											<a href="{{ route('creator.manage-courses.material.show', ['material' => 'tapi-boong-wkwkwk']) }}" class="flex items-center justify-center bg-gradient-to-br from-primary to-teal-400 py-2 px-4 rounded-md transition duration-300 hover:ring hover:ring-primary/70 hover:shadow-md hover:bg-gradient-to-tl">
												<span class="material-symbols-outlined font-medium text-center text-white">
													biotech
												</span>
												
												<span class="font-medium text-center text-white">
													Inspect
												</span>
											</a>
										</div>
									</div>
								</div>
								
								<div class="bg-white w-full rounded-md shadow-lg overflow-hidden max-w-sm">
									<div class="bg-primary px-4 py-2">
										<h3 class="font-semibold text-white text-center">Easy English Interview with Native Speaker</h3>
									</div>
									
									<div class="block p-4 space-y-4">
										<div class="block space-y-2">
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Session</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 32 Meetings</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Assignment</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 8 Submissions</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Type</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> Lifetime, Intensive, Subscription</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Courses</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> 30 Applied</h5>
											</div>
											
											<div class="grid grid-cols-3 gap-2">
												<div class="block">
													<h5 class="text-sm float-right">: </h5>
													<h4 class="text-sm">Updated</h4>
												</div>
												
												<h5 class="text-sm col-span-2"> July, 1<sup>st</sup> 2022</h5>
											</div>
										</div>
										
										<div class="flex flex-nowrap items-end justify-center">
											<a href="{{ route('creator.manage-courses.material.show', ['material' => 'tapi-boong-wkwkwk']) }}" class="flex items-center justify-center bg-gradient-to-br from-primary to-teal-400 py-2 px-4 rounded-md transition duration-300 hover:ring hover:ring-primary/70 hover:shadow-md hover:bg-gradient-to-tl">
												<span class="material-symbols-outlined font-medium text-center text-white">
													biotech
												</span>
												
												<span class="font-medium text-center text-white">
													Inspect
												</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						{{-- Manage Materials End --}}
						
						{{-- Manage Assistants Start --}}
						<div x-show="open == 'assistants'" x-transition class="block">
							<div class="flex items-center justify-end py-4 lg:hidden">
								<select name="assistant_sort" id="assistant_sort" class="bg-transparent text-secondary border-0 border-b border-slate-200 focus:outline-none focus:ring-0">
									<option value="all">All</option>
									<option value="active">Active</option>
									<option value="inactive">Inactive</option>
								</select>
							</div> 
							
							<div class="block my-4 rounded-lg shadow-md overflow-x-auto scrollbar">
								<table class="w-full ">
									<thead class="bg-slate-100 text-xs text-dark text-center uppercase">
										<tr>
											<th class="px-6 py-3">Assistant Name</th>
											<th class="px-6 py-3">Course</th>
											<th class="px-6 py-3">Since</th>
											<th class="px-6 py-3">Status</th>
											<th class="px-6 py-3 sr-only">Action</th>
										</tr>
									</thead>
									
									<tbody class="text-md text-secondary text-left">
										<tr class="border-b">
											<td class="px-6 py-4 text-dark font-medium">Mohammed Farrell Eghar Syahputra</td>
											<td class="px-6 py-4">Easy English Interview with Native Speaker</td>
											<td class="px-6 py-4">January, 1<sup>st</sup> 2021</td>
											<td x-data="{active: false}" class="px-6 py-4 text-center">
												<button type="button" x-on:click="active = !active" x-bind:class="active ? 'border-primary/80' : 'border-secondary/80'" class="h-4 w-8 bg-slate-100 border rounded-full py-px px-px">
													<div x-bind:class="active ? 'translate-x-4 bg-primary/50' : 'bg-secondary/50'" class="my-auto h-3 w-3 shadow-sm rounded-full transition duration-300"></div>
												</button>
												
												<h5 x-bind:class="{ 'text-primary': active }" x-text="active ? 'Active' : 'Inactive'">Inactive</h5>
											</td>
											<td class="px-6 py-4">
												<div class="flex flex-nowrap items-center justify-center space-x-2">
													<a href="#" class="flex items-center p-1 bg-orange-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															edit
														</span>
													</a>
													
													<a href="#" class="flex items-center p-1 bg-red-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															delete
														</span>
													</a>
												</div>
											</td>
										</tr>
										
										<tr class="border-b">
											<td class="px-6 py-4 text-dark font-medium">Mohammed Farrell Eghar Syahputra</td>
											<td class="px-6 py-4">Easy English Interview with Native Speaker</td>
											<td class="px-6 py-4">January, 1<sup>st</sup> 2021</td>
											<td x-data="{active: false}" class="px-6 py-4 text-center">
												<button type="button" x-on:click="active = !active" x-bind:class="active ? 'border-primary/80' : 'border-secondary/80'" class="h-4 w-8 bg-slate-100 border rounded-full py-px px-px">
													<div x-bind:class="active ? 'translate-x-4 bg-primary/50' : 'bg-secondary/50'" class="my-auto h-3 w-3 shadow-sm rounded-full transition duration-300"></div>
												</button>
												
												<h5 x-bind:class="{ 'text-primary': active }" x-text="active ? 'Active' : 'Inactive'">Inactive</h5>
											</td>
											<td class="px-6 py-4">
												<div class="flex flex-nowrap items-center justify-center space-x-2">
													<a href="#" class="flex items-center p-1 bg-orange-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															edit
														</span>
													</a>
													
													<a href="#" class="flex items-center p-1 bg-red-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															delete
														</span>
													</a>
												</div>
											</td>
										</tr>
										
										<tr class="border-b">
											<td class="px-6 py-4 text-dark font-medium">Mohammed Farrell Eghar Syahputra</td>
											<td class="px-6 py-4">Easy English Interview with Native Speaker</td>
											<td class="px-6 py-4">January, 1<sup>st</sup> 2021</td>
											<td x-data="{active: false}" class="px-6 py-4 text-center">
												<button type="button" x-on:click="active = !active" x-bind:class="active ? 'border-primary/80' : 'border-secondary/80'" class="h-4 w-8 bg-slate-100 border rounded-full py-px px-px">
													<div x-bind:class="active ? 'translate-x-4 bg-primary/50' : 'bg-secondary/50'" class="my-auto h-3 w-3 shadow-sm rounded-full transition duration-300"></div>
												</button>
												
												<h5 x-bind:class="{ 'text-primary': active }" x-text="active ? 'Active' : 'Inactive'">Inactive</h5>
											</td>
											<td class="px-6 py-4">
												<div class="flex flex-nowrap items-center justify-center space-x-2">
													<a href="#" class="flex items-center p-1 bg-orange-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															edit
														</span>
													</a>
													
													<a href="#" class="flex items-center p-1 bg-red-500 rounded-md hover:opacity-80">
														<span class="material-symbols-outlined text-white text-center">
															delete
														</span>
													</a>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						{{-- Manage Assistants End --}}
						@endguest
					</div>
				</div>
			</div>
		</div>
		{{-- Main Page End --}}
		
		{{-- New Material Start --}}
		@livewire('dashboard.material.new-material')
		{{-- New Material End --}}
		
		{{-- Floating Button Start --}}
		@livewire('dashboard.components.creator-floating-button')
		{{-- Floating Button End --}}
	</div>
@endsection