<div x-data="{ open: false }" class="flex items-center justify-center fixed bottom-8 right-8 w-12 h-12">
	<button x-on:click="open = !open" class="relative w-full h-full bg-dark rounded-full shadow-md">
		<span x-show="!open" x-transition:enter.delay.100ms class="text-white text-2xl">+</span>
		<span x-show="open" x-transition:enter.delay.100ms class="text-white text-2xl">x</span>
	</button>
	
	<div x-show="open" class="block relative float-left">
		<div x-transition x-on:click.outside="open = false" class="absolute right-14 bottom-8 bg-dark rounded-lg shadow-md px-4 py-2 h-auto w-56">
			<form action="{{ route('creator.manage-courses.course.store') }}" method="POST" class="flex items-center w-full rounded-md my-1 py-1 hover:bg-slate-700">
				@csrf
				<button type="submit" class="flex flex-nowrap items-center w-full rounded-md space-x-4 hover:bg-slate-700">
					<span class="material-symbols-outlined text-4xl text-sky-500">
						history_edu
					</span>
					
					<span class="text-white">
						New Course
					</span>
				</button>
			</form>
			
			<div class="border-t border-secondary"></div>
			
			<button type="button" x-on:click="floatMaterial = true; open = false" class="flex flex-nowrap items-center w-full rounded-md space-x-4 my-1 py-1 hover:bg-slate-700">
				<span class="material-symbols-outlined text-4xl text-violet-500">
					matter
				</span>
				
				<span class="text-white">
					New Material
				</span>
			</button>
			
			<div class="border-t border-secondary"></div>
			
			<a href="#" class="flex flex-nowrap items-center w-full rounded-md space-x-3 my-1 py-1 px-1 hover:bg-slate-700">
				<span class="material-symbols-outlined text-4xl text-yellow-500">
					group_add
				</span>
				
				<span class="text-white">
					Hire Student
				</span>
			</a>
		</div>
	</div>
</div>
