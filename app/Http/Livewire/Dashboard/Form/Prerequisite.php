<?php

namespace App\Http\Livewire\Dashboard\Form;

use App\Models\Course;
use App\Models\CoursePrerequisite;
use Livewire\Component;

class Prerequisite extends Component
{
	public Course $course;
	public $prerequisites;
	
	protected $rules = [
        'prerequisites.*.prerequisite' => 'required|string',
    ];
	
	protected $listeners = [
		'refreshComponent' => 'getPrerequisites',
	];
	
	public function mount(){
        $this->prerequisites = CoursePrerequisite::where('course_id', $this->course->id)->get();
    }
	
	public function updatedPrerequisites()
	{
		$this->prerequisites->each->save();
	}
	
	public function getPrerequisites()
	{
		$this->prerequisites = CoursePrerequisite::where('course_id', $this->course->id)->get();
	}
	
	public function deletePrerequisite(CoursePrerequisite $prerequisite)
	{
		$prerequisite->delete();
		$this->emit('refreshComponent');
	}
	
	public function addPrerequisite()
	{
		$this->validate();
		
		CoursePrerequisite::create([
			'course_id' => $this->course->id
		]);
		
		$this->emit('refreshComponent');
	}
	
    public function render()
    {
        return view('livewire.dashboard.form.prerequisite');
    }
}
