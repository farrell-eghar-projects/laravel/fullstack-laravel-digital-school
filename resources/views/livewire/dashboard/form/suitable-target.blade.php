<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
	<label for="suitable_target" class="lg:col-span-2">Suitable Target</label>
	<div class="space-y-2 lg:col-span-5">
		@foreach ($suitableTargets as $index => $suitableTarget)
			<div wire:key="suitableTarget-field-{{ $suitableTarget->id }}" class="space-y-2">
				<div class="flex flex-nowrap items-center space-x-2">
					<input type="text" id="suitable_target" wire:model.lazy="suitableTargets.{{ $index }}.target" placeholder="Those who prepare to work on international environment" class="basis-11/12 border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md">
					
					@if ($loop->count > 1)
						<button type="button" wire:click.prevent="deleteTarget({{ $suitableTarget->id }})" class="flex items-center p-0 bg-red-500 rounded-full hover:opacity-70">
							<span class="material-symbols-outlined text-white text-center rounded-full">
								cancel
							</span>
						</button>
					@else
						<button type="button" wire:click.prevent="deleteTarget({{ $suitableTarget->id }})" disabled class="flex items-center p-0 bg-slate-200 rounded-full hover:opacity-70">
							<span class="material-symbols-outlined text-white text-center rounded-full">
								cancel
							</span>
						</button>	
					@endif
				</div>
				
				@if ($loop->last)
					<div class="flex flex-nowrap items-center space-x-4">
						<button type="button" wire:click="addTarget" class="my-auto flex items-center px-4 py-1 bg-green-400 rounded-full hover:opacity-70">
							<span class="material-symbols-outlined text-white text-center rounded-full">
								add
							</span>
						</button>
						<span wire:loading.delay wire:target="addTaget">Processing...</span> 
					</div>
				@endif
			</div>
		@endforeach
		
	</div>
</div>