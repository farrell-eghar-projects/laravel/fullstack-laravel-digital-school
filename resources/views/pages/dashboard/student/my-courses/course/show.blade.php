@extends('layouts.master-dashboard')

@section('title', 'Course Details')

@section('content')
{{-- Breadcrumb Start --}}
@livewire('dashboard.components.breadcrumb', ['routes' => [
[
'link' => route('student.my-courses.index'),
'name' => 'My Courses'
],
[
'link' => route('student.my-courses.course.show', ['course' => $course->id]),
'name' => 'Course Details'
]
]
])
{{-- Breadcrumb End --}}

<div class="lg:px-32">
  <div class="container">
    <div x-data="{open: 'information'}" class="block bg-white my-2 px-8 py-4 rounded-md shadow-lg">
      {{-- Header Page Start --}}
      <div class="block">
        <h3 class="font-semibold text-xl text-dark text-center p-4 max-w-xl mx-auto lg:text-2xl">Easy Interview with Native Speaker</h3>

        <div class="flex items-center justify-between border-b border-slate-200">
          <div class="flex flex-wrap items-center">
            <button type="button" x-bind:class="open == 'information' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'information'" class="px-4 py-8 mr-4 border-b-2">
              Information
            </button>

            <button type="button" x-bind:class="open == 'materials' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'materials'" class="px-4 py-8 mr-4 border-b-2">
              Materials
            </button>

            <button type="button" x-bind:class="open == 'assignments' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'assignments'" class="px-4 py-8 mr-4 border-b-2">
              Assignments
            </button>

            <button type="button" x-bind:class="open == 'assessment' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'assessment'" class="px-4 py-8 mr-4 border-b-2">
              Assessment
            </button>

            <button type="button" x-bind:class="open == 'ranking' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'ranking'" class="px-4 py-8 mr-4 border-b-2">
              Ranking
            </button>
          </div>
        </div>
      </div>
      {{-- Header Page End --}}

      {{-- Main Page Start --}}
      <div class="block p-4">
        {{-- Information Tab Start --}}
        <div x-show="open == 'information'" x-transition class="block space-y-8">
          <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
            <h4 class="font-medium text-dark">Course Type</h4>

            <h5 class="text-secondary lg:col-span-3">{{ $course->type->name }}</h5>
          </div>

          <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
            <h4 class="font-medium text-dark">Description</h4>

            <h5 class="text-secondary lg:col-span-3">{!! $course->description !!}</h5>
          </div>

          <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
            <h4 class="font-medium text-dark">Prerequisite</h4>

            <ul class="px-4 list-disc text-secondary lg:col-span-3">
              @foreach ($course->coursePrerequisites as $index => $coursePrerequisite)
              <li>{{ $coursePrerequisite->prerequisite }}</li>
              @endforeach
            </ul>
          </div>

          <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
            <h4 class="font-medium text-dark">Started At</h4>

            <h5 class="text-secondary lg:col-span-3">{{ date("F j, Y", mktime(0,0,0,explode('-', $course->started_at)[1], explode(' ', explode('-', $course->started_at)[2])[0], explode('-', $course->started_at)[0])) }}</h5>
          </div>

          <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
            <h4 class="font-medium text-dark">Finished At</h4>

            <h5 class="text-secondary lg:col-span-3">{{ date("F j, Y", mktime(0,0,0,explode('-', $course->finished_at)[1], explode(' ', explode('-', $course->finished_at)[2])[0], explode('-', $course->finished_at)[0])) }}</h5>
          </div>

          <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
            <h4 class="font-medium text-dark">Thumbnail</h4>

            <div id="course_thumbnail" name="course_thumbnail" class="flex flex-wrap mt-4 lg:col-span-3">
              <div class="w-32 h-24 rounded-md overflow-hidden shrink-0">
                <img src="{{ $course->thumbnail }}" alt="Web Design" class="w-full h-full object-cover">
              </div>
            </div>
          </div>

          <div class="grid grid-cols-1 gap-2 lg:px-4 lg:grid-cols-4 lg:gap-4">
            <h4 class="font-medium text-dark">Preview</h4>

            <div id="course_preview" name="course_preview" class="flex flex-wrap mt-4 lg:col-span-3">
              @foreach (explode(',', $course->images) as $courseImage)
              <div class="inline w-32 h-24 rounded-md overflow-hidden mr-4 mb-4 shrink-0">
                <img src="{{ $courseImage }}" alt="Web Design" class="inline w-full h-full object-cover">
              </div>
              @endforeach
            </div>
          </div>
        </div>
        {{-- Information Tab End --}}

        {{-- Materials Tab Start --}}
        <div x-show="open == 'materials'" x-transition class="block">
          <div class="flex flex-nowrap items-center justify-end my-4 space-x-2">
            <h4 class="font-medium text-xs text-dark italic">Last updated</h4>
            <span class="flex flex-nowrap items-center space-x-1">
              <h5 class="text-xs text-secondary italic">:</h5>
              <h5 class="text-xs text-secondary italic">{{ date("F j, Y", mktime(0,0,0,explode('-', $course->material->updated_at)[1], explode(' ', explode('-', $course->material->updated_at)[2])[0], explode('-', $course->material->updated_at)[0])) }}</h5>
            </span>
          </div>

          <div class="grid grid-cols-1 gap-8 lg:grid-cols-2">
            <div class="block px-4 space-y-4">
              @foreach ($course->material->materialSections as $index => $materialSection)
              @if ($index < $loop->count / 2)
                <div x-data="{open: false}" x-bind:class="open ? 'border-primary' : 'border-secondary/70'" class="block border-b py-2 space-y-2">
                  <button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full">
                    <h4 class="font-medium text-dark text-sm">{{ $materialSection->name }}</h4>

                    <span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
                      expand_more
                    </span>
                  </button>

                  <div x-show="open" x-transition class="px-4 space-y-2">
                    @foreach ($materialSection->materialSessions as $key => $materialSession)
                    <a href="{{ route('student.my-courses.course.material.show', ['course' => $course, 'session' => $materialSession->id]) }}" target="_blank" x-on:click="edit = 'Meeting 1'" class="inline-block w-full text-dark text-sm text-left py-2 hover:text-primary">{{ $materialSession->title }}</a>
                    @endforeach
                  </div>
                </div>
                {{$index}}
                @endif
                @endforeach
            </div>

            <div class="block px-4 space-y-4">
              @foreach ($course->material->materialSections as $index => $materialSection)
              @if ($index >= $loop->count / 2)
              <div x-data="{open: false}" x-bind:class="open ? 'border-primary' : 'border-secondary/70'" class="block border-b py-2 space-y-2">
                <button x-on:click="open = !open" type="button" class="flex items-center justify-between w-full">
                  <h4 class="font-medium text-dark text-sm">Week 1</h4>

                  <span x-bind:class="{'rotate-180 text-primary': open}" class="material-symbols-outlined text-lg transition duration-300">
                    expand_more
                  </span>
                </button>

                <div x-show="open" x-transition class="px-4 space-y-2">
                  @foreach ($materialSection->materialSessions as $key => $materialSession)
                  <a href="{{ route('student.my-courses.course.material.show', ['course' => $course, 'session' => $materialSession->id]) }}" target="_blank" x-on:click="edit = 'Meeting 1'" class="inline-block w-full text-dark text-sm text-left py-2 hover:text-primary">{{ $materialSession->title }}</a>
                  @endforeach
                </div>
              </div>
              {{$index}}
              @endif
              @endforeach
            </div>
          </div>
        </div>
        {{-- Materials Tab End --}}

        {{-- Assignments Tab Start --}}
        <div x-show="open == 'assignments'" x-transition class="block">
          <div class="block my-4 rounded-lg shadow-md overflow-x-auto scrollbar">
            <table class="w-full sm:table-fixed">
              <thead class="bg-slate-100 text-xs text-dark text-center uppercase">
                <tr>
                  <th class="px-4 py-3 w-full">Assignment</th>
                  <th class="hidden px-4 py-3 w-1/3 md:table-cell">Duration</th>
                  <th class="hidden px-4 py-3 w-1/3 md:table-cell">Actions</th>
                </tr>
              </thead>

              @foreach ($course->material->assignments as $index => $assignment)
              <tbody x-data="{expand: false}" x-on:click="expand = !expand" class="text-secondary text-left border-b">
                <tr>
                  <td class="px-4 py-4">
                    <div class="flex flex-nowrap items-center justify-between space-x-8 sm:space-x-4">
                      <h4 class="font-medium text-dark text-md">{{ $assignment->name }}</h4>

                      <button type="button" x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
                        expand_more
                      </button>
                    </div>
                  </td>
                  <td class="hidden px-4 py-4 text-secondary text-center md:table-cell">{{ $durations[$index] }}</td>
                  <td class="hidden px-4 py-4 text-center md:table-cell">
                    <a href="{{ route('student.my-courses.course.assignment.show', ['course' => $course->id, 'assignment' => $assignment->id]) }}" class="bg-primary font-medium text-sm text-white text-center m-auto px-4 py-2 rounded-full hover:opacity-80 hover:shadow-md hover:ring hover:ring-primary/70">Start</a>
                  </td>
                </tr>

                <tr x-show="expand" x-transition class="md:hidden">
                  <td colspan="3">
                    <div class="flex flex-nowrap px-4 py-2 space-x-4">
                      <h4 class="font-medium text-dark">Duration</h4>
                      <span class="flex flex-nowrap space-x-1">
                        <h5 class="text-secondary">:</h5>
                        <h5 class="text-secondary">{{ $durations[$index] }}</h5>
                      </span>
                    </div>

                    <div class="flex flex-wrap items-center px-4 mb-4">
                      <a href="{{ route('student.my-courses.course.assignment.show', ['course' => $course->id, 'assignment' => $assignment->id]) }}" x-on:click.stop class="bg-primary font-medium text-sm text-white text-center px-4 py-2 rounded-full hover:opacity-80 hover:shadow-md hover:ring hover:ring-primary/70">Start</a>
                    </div>
                  </td>
                </tr>
              </tbody>
              @endforeach
            </table>
          </div>
        </div>
        {{-- Assignments Tab End --}}

        {{-- Assessment Tab Start --}}
        <div x-show="open == 'assessment'" x-transition class="block space-y-12">
          <div class="block space-y-4">
            <div class="flex flex-nowrap items-start justify-end my-4 space-x-2">
              <h4 class="font-medium text-xs text-dark italic whitespace-nowrap">Last updated</h4>
              <span class="flex flex-nowrap items-start space-x-1">
                <h5 class="text-xs text-secondary italic">:</h5>
                <h5 class="text-xs text-secondary italic">08:08:08, July 18<sup>th</sup>, 2022</h5>
              </span>
            </div>

            <div class="space-y-2">
              <h4 class="font-medium text-dark">Total Score (Max 100)</h4>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-8/12">65.65</div>
              </div>
            </div>
          </div>

          <div class="grid grid-cols-1 mt-8 py-2 gap-8 sm:gap-12 sm:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4">
            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>

            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>

            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>

            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>

            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>

            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>

            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>

            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>

            <div class="space-y-2">
              <h5 class="font-medium text-sm text-secondary">Submission 1</h5>
              <div class="w-full bg-slate-200 rounded-full">
                <div class="bg-green-500 text-xs font-medium text-white text-center p-0.5 leading-none rounded-full w-7/12">5.55/10.00</div>
              </div>
            </div>
          </div>
        </div>
        {{-- Assessment Tab End --}}

        {{-- Ranking Tab Start --}}
        <div x-show="open == 'ranking'" x-transition class="block space-y-2">
          <div class="block space-y-4">
            <div class="flex flex-nowrap items-start justify-end my-4 space-x-2">
              <h4 class="font-medium text-xs text-dark italic whitespace-nowrap">Last updated</h4>
              <span class="flex flex-nowrap items-start space-x-1">
                <h5 class="text-xs text-secondary italic">:</h5>
                <h5 class="text-xs text-secondary italic">08:08:08, July 18<sup>th</sup>, 2022</h5>
              </span>
            </div>

            <h5 class="font-medium text-lg text-dark">Rank 33 of {{ count($course->myCourses) }}</h5>
          </div>

          <div class="block my-4 rounded-lg shadow-md overflow-x-auto scrollbar">
            <table class="w-full sm:table-fixed">
              <thead class="bg-slate-100 text-xs text-dark text-center uppercase">
                <tr>
                  <th class="px-4 py-3 sm:w-1/6">Rank</th>
                  <th class="px-4 py-3 sm:w-full">Student</th>
                  <th class="hidden px-4 py-3 sm:w-1/6 md:table-cell">Score</th>
                </tr>
              </thead>

              <tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
                <tr>
                  <td class="relative px-4 py-4 text-center text-dark">9999</td>
                  <td class="px-4 py-4">
                    <div class="flex flex-nowrap items-center justify-between space-x-16">
                      <div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
                        <div class="rounded-full overflow-hidden shrink-0">
                          <img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
                        </div>

                        <div class="block">
                          <h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
                          <h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
                        </div>
                      </div>

                      <div class="block">
                        <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
                          expand_more
                        </span>
                      </div>
                    </div>
                  </td>
                  <td class="hidden px-4 py-4 text-dark text-center md:table-cell">98.89</td>
                </tr>

                <tr x-show="expand" x-transition class="md:hidden">
                  <td colspan="3">
                    <div class="flex flex-nowrap items-start px-4 py-2 space-x-4">
                      <h4 class="font-medium text-dark whitespace-nowrap">Total Score</h4>
                      <span class="flex flex-nowrap items-start space-x-2">
                        <h5>:</h5>
                        <h5 class="text-secondary">98.89</h5>
                      </span>
                    </div>
                  </td>
                </tr>
              </tbody>

              <tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
                <tr>
                  <td class="relative px-4 py-4 text-center text-dark">9999</td>
                  <td class="px-4 py-4">
                    <div class="flex flex-nowrap items-center justify-between space-x-16">
                      <div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
                        <div class="rounded-full overflow-hidden shrink-0">
                          <img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
                        </div>

                        <div class="block">
                          <h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
                          <h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
                        </div>
                      </div>

                      <div class="block">
                        <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
                          expand_more
                        </span>
                      </div>
                    </div>
                  </td>
                  <td class="hidden px-4 py-4 text-dark text-center md:table-cell">98.89</td>
                </tr>

                <tr x-show="expand" x-transition class="md:hidden">
                  <td colspan="3">
                    <div class="flex flex-nowrap items-start px-4 py-2 space-x-4">
                      <h4 class="font-medium text-dark whitespace-nowrap">Total Score</h4>
                      <span class="flex flex-nowrap items-start space-x-2">
                        <h5>:</h5>
                        <h5 class="text-secondary">98.89</h5>
                      </span>
                    </div>
                  </td>
                </tr>
              </tbody>

              <tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
                <tr>
                  <td class="relative px-4 py-4 text-center text-dark">9999</td>
                  <td class="px-4 py-4">
                    <div class="flex flex-nowrap items-center justify-between space-x-16">
                      <div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
                        <div class="rounded-full overflow-hidden shrink-0">
                          <img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
                        </div>

                        <div class="block">
                          <h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
                          <h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
                        </div>
                      </div>

                      <div class="block">
                        <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
                          expand_more
                        </span>
                      </div>
                    </div>
                  </td>
                  <td class="hidden px-4 py-4 text-dark text-center md:table-cell">98.89</td>
                </tr>

                <tr x-show="expand" x-transition class="md:hidden">
                  <td colspan="3">
                    <div class="flex flex-nowrap items-start px-4 py-2 space-x-4">
                      <h4 class="font-medium text-dark whitespace-nowrap">Total Score</h4>
                      <span class="flex flex-nowrap items-start space-x-2">
                        <h5>:</h5>
                        <h5 class="text-secondary">98.89</h5>
                      </span>
                    </div>
                  </td>
                </tr>
              </tbody>

              <tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
                <tr>
                  <td class="relative px-4 py-4 text-center text-dark">9999</td>
                  <td class="px-4 py-4">
                    <div class="flex flex-nowrap items-center justify-between space-x-16">
                      <div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
                        <div class="rounded-full overflow-hidden shrink-0">
                          <img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
                        </div>

                        <div class="block">
                          <h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
                          <h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
                        </div>
                      </div>

                      <div class="block">
                        <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
                          expand_more
                        </span>
                      </div>
                    </div>
                  </td>
                  <td class="hidden px-4 py-4 text-dark text-center md:table-cell">98.89</td>
                </tr>

                <tr x-show="expand" x-transition class="md:hidden">
                  <td colspan="3">
                    <div class="flex flex-nowrap items-start px-4 py-2 space-x-4">
                      <h4 class="font-medium text-dark whitespace-nowrap">Total Score</h4>
                      <span class="flex flex-nowrap items-start space-x-2">
                        <h5>:</h5>
                        <h5 class="text-secondary">98.89</h5>
                      </span>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        {{-- Ranking Tab End --}}
      </div>
      {{-- Main Page End --}}
    </div>
  </div>
</div>
@endsection