@extends('layouts.master-dashboard')

@section('title', 'Dashboard')

@section('content')
	{{-- Main Dashboard Start --}}
	<div class="pt-24 xl:px-32">
		<div class="container">
			<div class="flex flex-nowrap overflow-x-scroll items-center scrollbar sm:justify-around sm:flex-wrap lg:flex-nowrap lg:overflow-x-visible">
				<div class="relative block m-4 bg-white w-40 h-40 overflow-hidden shrink-0 rounded-2xl shadow-xl -z-20">
					<div class="absolute -right-1 -bottom-4 w-full h-full scale-[4.0] -z-10">
						<svg id="sw-js-blob-svg1" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
							<defs>
								<linearGradient id="sw-gradient1" x1="0" x2="1" y1="1" y2="0">
									<stop id="stop1" stop-color="rgba(59, 130, 246, 1)" offset="0%"></stop>
									<stop id="stop2" stop-color="rgba(251, 113, 133, 1)" offset="100%"></stop>
								</linearGradient>
							</defs>
							<path fill="url(#sw-gradient1)" d="M12.3,-24.1C13.4,-20.7,9.8,-12.2,13.9,-7.3C17.9,-2.4,29.5,-1.2,35.1,3.2C40.7,7.7,40.3,15.4,35.6,19.1C31,22.9,22.1,22.8,15.4,23.1C8.8,23.3,4.4,23.9,-0.2,24.2C-4.8,24.5,-9.6,24.7,-14.4,23.4C-19.2,22.1,-24,19.4,-25.8,15.3C-27.5,11.1,-26.1,5.6,-21.3,2.7C-16.6,-0.1,-8.5,-0.1,-4.7,-0.8C-1,-1.4,-1.6,-2.6,-1.5,-6.6C-1.5,-10.6,-0.7,-17.3,2.4,-21.5C5.6,-25.8,11.3,-27.5,12.3,-24.1Z" width="100%" height="100%" transform="translate(50 50)" stroke-width="0" style="transition: all 0.3s ease 0s;"></path>
						</svg>
					</div>
					
					<div class="flex flex-nowrap justify-between w-full py-3">
						<span class="material-symbols-outlined text-7xl bg-clip-text text-transparent bg-gradient-to-tr from-blue-500 to-rose-400">
							book_online
						</span>
						
						<div class="block pr-2">
							<div class="font-semibold text-right text-slate-100 text-sm mb-2">
								Courses
							</div>
							
							<div class="font-semibold text-right text-white text-2xl">
								88888
							</div>
							
							<div class="flex flex-nowrap items-center float-right space-x-1">
								<span class="material-symbols-outlined font-bold text-right text-sm text-blue-200">
									keyboard_double_arrow_up
								</span>
								
								<span class="font-semibold text-right text-xs text-blue-200">
									34
								</span>
							</div>
						</div>
					</div>
					
					<div class="border-t border-blue-200 mx-2 mt-5"></div>
					
					<div class="mx-2 mt-1">
						<span class="font-medium text-white text-xs">Updated yesterday</span>
					</div>
				</div>
				
				<div class="relative block m-4 bg-white w-40 h-40 overflow-hidden shrink-0 rounded-2xl shadow-xl -z-20">
					<div class="absolute -right-1 -bottom-4 w-full h-full scale-[4.0] -z-10">
						<svg id="sw-js-blob-svg2" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
							<defs>
								<linearGradient id="sw-gradient2" x1="0" x2="1" y1="1" y2="0">
									<stop id="stop1" stop-color="rgba(34, 197, 94, 1)" offset="0%"></stop>
									<stop id="stop2" stop-color="rgba(56, 189, 248, 1)" offset="100%"></stop>
								</linearGradient>
							</defs>
							<path fill="url(#sw-gradient2)" d="M12.3,-24.1C13.4,-20.7,9.8,-12.2,13.9,-7.3C17.9,-2.4,29.5,-1.2,35.1,3.2C40.7,7.7,40.3,15.4,35.6,19.1C31,22.9,22.1,22.8,15.4,23.1C8.8,23.3,4.4,23.9,-0.2,24.2C-4.8,24.5,-9.6,24.7,-14.4,23.4C-19.2,22.1,-24,19.4,-25.8,15.3C-27.5,11.1,-26.1,5.6,-21.3,2.7C-16.6,-0.1,-8.5,-0.1,-4.7,-0.8C-1,-1.4,-1.6,-2.6,-1.5,-6.6C-1.5,-10.6,-0.7,-17.3,2.4,-21.5C5.6,-25.8,11.3,-27.5,12.3,-24.1Z" width="100%" height="100%" transform="translate(50 50)" stroke-width="0" style="transition: all 0.3s ease 0s;"></path>
						</svg>
					</div>
					
					<div class="flex flex-nowrap justify-between w-full py-3">
						<span class="material-symbols-outlined text-7xl bg-clip-text text-transparent bg-gradient-to-tr from-green-500 to-sky-400">
							sensor_occupied
						</span>
						
						<div class="block pr-2">
							<div class="font-semibold text-right text-slate-100 text-sm mb-2">
								Students
							</div>
							
							<div class="font-semibold text-right text-white text-2xl">
								88888
							</div>
							
							<div class="flex flex-nowrap items-center float-right space-x-1">
								<span class="material-symbols-outlined font-bold text-right text-sm text-green-200">
									keyboard_double_arrow_up
								</span>
								
								<span class="font-semibold text-right text-xs text-green-200">
									34
								</span>
							</div>
						</div>
					</div>
					
					<div class="border-t border-green-200 mx-2 mt-5"></div>
					
					<div class="mx-2 mt-1">
						<span class="font-medium text-white text-xs">Updated yesterday</span>
					</div>
				</div>
				
				<div class="relative block m-4 bg-white w-40 h-40 overflow-hidden shrink-0 rounded-2xl shadow-xl -z-20">
					<div class="absolute -right-1 -bottom-4 w-full h-full scale-[4.0] -z-10">
						<svg id="sw-js-blob-svg3" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
							<defs>
								<linearGradient id="sw-gradient3" x1="0" x2="1" y1="1" y2="0">
									<stop id="stop1" stop-color="rgba(249, 115, 22, 1)" offset="0%"></stop>
									<stop id="stop2" stop-color="rgba(163, 230, 53, 1)" offset="100%"></stop>
								</linearGradient>
							</defs>
							<path fill="url(#sw-gradient3)" d="M12.3,-24.1C13.4,-20.7,9.8,-12.2,13.9,-7.3C17.9,-2.4,29.5,-1.2,35.1,3.2C40.7,7.7,40.3,15.4,35.6,19.1C31,22.9,22.1,22.8,15.4,23.1C8.8,23.3,4.4,23.9,-0.2,24.2C-4.8,24.5,-9.6,24.7,-14.4,23.4C-19.2,22.1,-24,19.4,-25.8,15.3C-27.5,11.1,-26.1,5.6,-21.3,2.7C-16.6,-0.1,-8.5,-0.1,-4.7,-0.8C-1,-1.4,-1.6,-2.6,-1.5,-6.6C-1.5,-10.6,-0.7,-17.3,2.4,-21.5C5.6,-25.8,11.3,-27.5,12.3,-24.1Z" width="100%" height="100%" transform="translate(50 50)" stroke-width="0" style="transition: all 0.3s ease 0s;"></path>
						</svg>
					</div>
					
					<div class="flex flex-nowrap justify-between w-full py-3">
						<span class="material-symbols-outlined text-7xl bg-clip-text text-transparent bg-gradient-to-tr from-orange-500 to-lime-400">
							payments
						</span>
						
						<div class="block pr-2">
							<div class="font-semibold text-right text-slate-100 text-sm">
								Revenue
							</div>
							
							<div class="font-semibold text-right text-white text-2xl leading-none before:content-['\(x_1.000.000\)'] before:text-slate-100 before:text-[8px]">
								88888
							</div>
							
							<div class="flex flex-nowrap items-center float-right space-x-1 mt-1">
								<span class="material-symbols-outlined font-bold text-right text-sm text-orange-200">
									keyboard_double_arrow_up
								</span>
								
								<span class="font-semibold text-right text-xs text-orange-200">
									34
								</span>
							</div>
						</div>
					</div>
					
					<div class="border-t border-orange-200 mx-2 mt-2"></div>
					
					<div class="mx-2 mt-1">
						<span class="font-medium text-white text-xs">Updated yesterday</span>
					</div>
				</div>
				
				<div class="relative block m-4 bg-white w-40 h-40 overflow-hidden shrink-0 rounded-2xl shadow-xl -z-20">
					<div class="absolute -right-1 -bottom-4 w-full h-full scale-[4.0] -z-10">
						<svg id="sw-js-blob-svg4" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
							<defs>
								<linearGradient id="sw-gradient4" x1="0" x2="1" y1="1" y2="0">
									<stop id="stop1" stop-color="rgba(236, 72, 153, 1)" offset="0%"></stop>
									<stop id="stop2" stop-color="rgba(250, 204, 21, 1)" offset="100%"></stop>
								</linearGradient>
							</defs>
							<path fill="url(#sw-gradient4)" d="M12.3,-24.1C13.4,-20.7,9.8,-12.2,13.9,-7.3C17.9,-2.4,29.5,-1.2,35.1,3.2C40.7,7.7,40.3,15.4,35.6,19.1C31,22.9,22.1,22.8,15.4,23.1C8.8,23.3,4.4,23.9,-0.2,24.2C-4.8,24.5,-9.6,24.7,-14.4,23.4C-19.2,22.1,-24,19.4,-25.8,15.3C-27.5,11.1,-26.1,5.6,-21.3,2.7C-16.6,-0.1,-8.5,-0.1,-4.7,-0.8C-1,-1.4,-1.6,-2.6,-1.5,-6.6C-1.5,-10.6,-0.7,-17.3,2.4,-21.5C5.6,-25.8,11.3,-27.5,12.3,-24.1Z" width="100%" height="100%" transform="translate(50 50)" stroke-width="0" style="transition: all 0.3s ease 0s;"></path>
						</svg>
					</div>
					
					<div class="flex flex-nowrap justify-between w-full py-3">
						<span class="material-symbols-outlined text-7xl bg-clip-text text-transparent bg-gradient-to-tr from-pink-500 to-yellow-400">
							stars
						</span>
						
						<div class="block pr-2">
							<div class="font-semibold text-right text-slate-100 text-sm mb-2">
								Rating
							</div>
							
							<div class="font-semibold text-right text-white text-2xl">
								4.8
							</div>
							
							<div class="flex flex-nowrap items-center float-right space-x-1">
								<span class="material-symbols-outlined font-bold text-right text-sm text-pink-200">
									keyboard_double_arrow_up
								</span>
								
								<span class="font-semibold text-right text-xs text-pink-200">
									34
								</span>
							</div>
						</div>
					</div>
					
					<div class="border-t border-pink-200 mx-2 mt-5"></div>
					
					<div class="mx-2 mt-1">
						<span class="font-medium text-white text-xs">Updated yesterday</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- Main Dashboard End --}}
	
	{{-- Main Courses Start --}}
	<div class="pt-20 mb-28 xl:px-28">
		<div class="container">
			<div class="px-4 grid grid-cols-1 gap-4 lg:grid-cols-3">
				{{-- Recently Viewed Courses Start --}}
				<div class="block space-y-3">
					<h4 class="text-dark font-bold text-lg">Recently Viewed</h4>
					
					<div class="flex flex-wrap items-center w-full bg-white p-2 space-y-1 rounded-lg h-64 overflow-y-scroll scrollbar">
						<a href="#" class="flex flex-nowrap items-center space-x-2 w-full p-2 rounded-lg hover:bg-cyan-100">
							<div class="w-1/3 h-auto rounded-md overflow-hidden shrink-0">
								<img src="{{ url('https://source.unsplash.com/360x240?web+design') }}" alt="Web Design" class="w-full object-cover">
							</div>
							
							<div class="block items-center">
								<span class="font-semibold text-dark text-md leadning-none">Build a Simple Web Design Using Figma</span>
								
								<div class="flex items-center space-x-3 pr-4">
									<span class="font-semibold text-secondary text-xs">Rp 249.999,-</span>
									
									<span class="flex items-center space-x-1">
										<span class="material-symbols-outlined font-semibold text-xs text-blue-500">
											person_4
										</span>
										<span class="text-xs font-semibold text-blue-500">99</span>
									</span>
								</div>
							</div>
						</a>
						
						<div class="h-0.5 w-full bg-slate-200"></div>
						
						<a href="#" class="flex flex-nowrap items-center space-x-2 w-full p-2 rounded-lg hover:bg-cyan-100">
							<div class="w-1/3 h-auto rounded-md overflow-hidden shrink-0">
								<img src="{{ url('https://source.unsplash.com/360x240?web+design') }}" alt="Web Design" class="w-full object-cover">
							</div>
							
							<div class="block items-center">
								<span class="font-semibold text-dark text-md leadning-none">Build a Simple Web Design Using Figma</span>
								
								<div class="flex items-center space-x-3 pr-4">
									<span class="font-semibold text-secondary text-xs">Rp 249.999,-</span>
									
									<span class="flex items-center space-x-1">
										<span class="material-symbols-outlined font-semibold text-xs text-blue-500">
											person_4
										</span>
										<span class="text-xs font-semibold text-blue-500">99</span>
									</span>
								</div>
							</div>
						</a>
						
						<div class="h-0.5 w-full bg-slate-200"></div>
						
						<a href="#" class="flex flex-nowrap items-center space-x-2 w-full p-2 rounded-lg hover:bg-cyan-100">
							<div class="w-1/3 h-auto rounded-md overflow-hidden shrink-0">
								<img src="{{ url('https://source.unsplash.com/360x240?web+design') }}" alt="Web Design" class="w-full object-cover">
							</div>
							
							<div class="block items-center">
								<span class="font-semibold text-dark text-md leadning-none">Build a Simple Web Design Using Figma</span>
								
								<div class="flex items-center space-x-3 pr-4">
									<span class="font-semibold text-secondary text-xs">Rp 249.999,-</span>
									
									<span class="flex items-center space-x-1">
										<span class="material-symbols-outlined font-semibold text-xs text-blue-500">
											person_4
										</span>
										<span class="text-xs font-semibold text-blue-500">99</span>
									</span>
								</div>
							</div>
						</a>
						
						<div class="h-0.5 w-full bg-slate-200"></div>
						
						<a href="#" class="flex flex-nowrap items-center space-x-2 w-full p-2 rounded-lg hover:bg-cyan-100">
							<div class="w-1/3 h-auto rounded-md overflow-hidden shrink-0">
								<img src="{{ url('https://source.unsplash.com/360x240?web+design') }}" alt="Web Design" class="w-full object-cover">
							</div>
							
							<div class="block items-center">
								<span class="font-semibold text-dark text-md leadning-none">Build a Simple Web Design Using Figma</span>
								
								<div class="flex items-center space-x-3 pr-4">
									<span class="font-semibold text-secondary text-xs">Rp 249.999,-</span>
									
									<span class="flex items-center space-x-1">
										<span class="material-symbols-outlined font-semibold text-xs text-blue-500">
											person_4
										</span>
										<span class="text-xs font-semibold text-blue-500">99</span>
									</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				{{-- Recently Viewed Courses End --}}
				
				{{-- Popular Courses Start --}}
				<div class="block space-y-3 lg:col-span-2">
					<div class="flex justify-between">
						<h4 class="text-dark font-bold text-lg">Popular Courses</h4>
						<a href="#" class="font-semibold text-md text-sky-500 hover:opacity-80">View all</a>
					</div>
					
					<div class="flex flex-wrap w-full space-y-1.5">
						<div x-data="{ open: false }" class="px-4 py-2 w-full h-auto block justify-between rounded-lg bg-white lg:flex lg:px-8 lg:py-4 lg:h-20">
							<div class="flex items-center space-x-3">
								<div class="w-12 h-12 rounded-md overflow-hidden shrink-0">
									<img src="{{ url('https://source.unsplash.com/250x250?web+design') }}" alt="Web Design" class="w-full object-cover">
								</div>
								
								<div class="block w-full">
									<h3 class="font-semibold text-md text-dark">Develop Digital School Using Laravel 9</h3>
									
									<div class="flex items-center space-x-2">
										<span class="material-symbols-outlined text-secondary font-medium text-xs">
											calendar_month
										</span>
										
										<span class="text-secondary font-medium text-xs">May, 33 2022 - Present</span>
									</div>
								</div>
								
								<button class="float-right lg:hidden">
									<span x-on:click="open = true" x-show="!open" class="material-symbols-outlined">
										expand_more
									</span>
									
									<span x-on:click="open = false" x-show="open" class="material-symbols-outlined">
										expand_less
									</span>
								</button>
							</div>
							
							{{-- Desktop Version --}}
							<div class="hidden items-center space-x-3 lg:flex">
								<div class="block">
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-secondary font-medium text-xs">
											visibility
										</span>
										
										<span class="text-secondary font-medium text-xs">11.111</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-red-500 font-medium text-xs">
											favorite
										</span>
										
										<span class="text-secondary font-medium text-xs">5.000</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-sky-500 font-medium text-xs">
											person
										</span>
										
										<span class="text-secondary font-medium text-xs">1.111</span>
									</div>
								</div>
								
								<div class="block">
									<a href="#" class="block py-1 px-2 text-dark text-md font-medium border border-secondary hover:border-primary hover:text-white hover:bg-primary">Details</a>
								</div>
							</div>
							
							{{-- Mobile Version --}}
							<div x-show="open" x-transition class="flex items-center justify-between border-t mt-2 pt-2 lg:hidden">
								<div class="flex items-center justify-evenly space-x-3 sm:space-x-5">
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-secondary font-medium text-sm sm:text-md">
											visibility
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">11.111</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-red-500 font-medium text-sm sm:text-md">
											favorite
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">5.000</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-sky-500 font-medium text-sm sm:text-md">
											person
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">1.111</span>
									</div>
								</div>
								
								<div class="block">
									<a href="#" class="block py-1 px-2 text-dark text-md font-medium border border-secondary hover:border-primary hover:text-white hover:bg-primary">Details</a>
								</div>
							</div>
						</div>
						
						<div x-data="{ open: false }" class="px-4 py-2 w-full h-auto block justify-between rounded-lg bg-white lg:flex lg:px-8 lg:py-4 lg:h-20">
							<div class="flex items-center space-x-3">
								<div class="w-12 h-12 rounded-md overflow-hidden shrink-0">
									<img src="{{ url('https://source.unsplash.com/250x250?web+design') }}" alt="Web Design" class="w-full object-cover">
								</div>
								
								<div class="block w-full">
									<h3 class="font-semibold text-md text-dark">Develop Digital School Using Laravel 9</h3>
									
									<div class="flex items-center space-x-2">
										<span class="material-symbols-outlined text-secondary font-medium text-xs">
											calendar_month
										</span>
										
										<span class="text-secondary font-medium text-xs">May, 33 2022 - Present</span>
									</div>
								</div>
								
								<button class="float-right lg:hidden">
									<span x-on:click="open = true" x-show="!open" class="material-symbols-outlined">
										expand_more
									</span>
									
									<span x-on:click="open = false" x-show="open" class="material-symbols-outlined">
										expand_less
									</span>
								</button>
							</div>
							
							{{-- Desktop Version --}}
							<div class="hidden items-center space-x-3 lg:flex">
								<div class="block">
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-secondary font-medium text-xs">
											visibility
										</span>
										
										<span class="text-secondary font-medium text-xs">11.111</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-red-500 font-medium text-xs">
											favorite
										</span>
										
										<span class="text-secondary font-medium text-xs">5.000</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-sky-500 font-medium text-xs">
											person
										</span>
										
										<span class="text-secondary font-medium text-xs">1.111</span>
									</div>
								</div>
								
								<div class="block">
									<a href="#" class="block py-1 px-2 text-dark text-md font-medium border border-secondary hover:border-primary hover:text-white hover:bg-primary">Details</a>
								</div>
							</div>
							
							{{-- Mobile Version --}}
							<div x-show="open" x-transition class="flex items-center justify-between border-t mt-2 pt-2 lg:hidden">
								<div class="flex items-center justify-evenly space-x-3 sm:space-x-5">
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-secondary font-medium text-sm sm:text-md">
											visibility
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">11.111</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-red-500 font-medium text-sm sm:text-md">
											favorite
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">5.000</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-sky-500 font-medium text-sm sm:text-md">
											person
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">1.111</span>
									</div>
								</div>
								
								<div class="block">
									<a href="#" class="block py-1 px-2 text-dark text-md font-medium border border-secondary hover:border-primary hover:text-white hover:bg-primary">Details</a>
								</div>
							</div>
						</div>
						
						<div x-data="{ open: false }" class="px-4 py-2 w-full h-auto block justify-between rounded-lg bg-white lg:flex lg:px-8 lg:py-4 lg:h-20">
							<div class="flex items-center space-x-3">
								<div class="w-12 h-12 rounded-md overflow-hidden shrink-0">
									<img src="{{ url('https://source.unsplash.com/250x250?web+design') }}" alt="Web Design" class="w-full object-cover">
								</div>
								
								<div class="block w-full">
									<h3 class="font-semibold text-md text-dark">Develop Digital School Using Laravel 9</h3>
									
									<div class="flex items-center space-x-2">
										<span class="material-symbols-outlined text-secondary font-medium text-xs">
											calendar_month
										</span>
										
										<span class="text-secondary font-medium text-xs">May, 33 2022 - Present</span>
									</div>
								</div>
								
								<button class="float-right lg:hidden">
									<span x-on:click="open = true" x-show="!open" class="material-symbols-outlined">
										expand_more
									</span>
									
									<span x-on:click="open = false" x-show="open" class="material-symbols-outlined">
										expand_less
									</span>
								</button>
							</div>
							
							{{-- Desktop Version --}}
							<div class="hidden items-center space-x-3 lg:flex">
								<div class="block">
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-secondary font-medium text-xs">
											visibility
										</span>
										
										<span class="text-secondary font-medium text-xs">11.111</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-red-500 font-medium text-xs">
											favorite
										</span>
										
										<span class="text-secondary font-medium text-xs">5.000</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-sky-500 font-medium text-xs">
											person
										</span>
										
										<span class="text-secondary font-medium text-xs">1.111</span>
									</div>
								</div>
								
								<div class="block">
									<a href="#" class="block py-1 px-2 text-dark text-md font-medium border border-secondary hover:border-primary hover:text-white hover:bg-primary">Details</a>
								</div>
							</div>
							
							{{-- Mobile Version --}}
							<div x-show="open" x-transition class="flex items-center justify-between border-t mt-2 pt-2 lg:hidden">
								<div class="flex items-center justify-evenly space-x-3 sm:space-x-5">
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-secondary font-medium text-sm sm:text-md">
											visibility
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">11.111</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-red-500 font-medium text-sm sm:text-md">
											favorite
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">5.000</span>
									</div>
									
									<div class="flex flex-nowrap space-x-1">
										<span class="material-symbols-outlined text-sky-500 font-medium text-sm sm:text-md">
											person
										</span>
										
										<span class="text-secondary font-medium text-sm sm:text-md">1.111</span>
									</div>
								</div>
								
								<div class="block">
									<a href="#" class="block py-1 px-2 text-dark text-md font-medium border border-secondary hover:border-primary hover:text-white hover:bg-primary">Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				{{-- Popular Courses End --}}
			</div>
		</div>
	</div>
	{{-- Main Courses End --}}
	
	{{-- My Drafts Start --}}
	<div class="pb-28 xl:px-28">
		<div class="container">
			asd
		</div>
	</div>
	{{-- My Drafts End --}}
	
	{{-- Floating Button Start --}}
	@livewire('dashboard.components.creator-floating-button')
	{{-- Floating Button End --}}
@endsection