<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $types = [
        [
          'name' => 'Intensive Course'
        ],
        [
          'name' => 'Lifetime Course'
        ],
        [
          'name' => 'Subscription Course'
        ]
      ];
      
      Type::insert($types);
    }
}
