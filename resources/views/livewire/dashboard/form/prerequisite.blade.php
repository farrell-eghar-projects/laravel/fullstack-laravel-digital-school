<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
	<label for="prerequisite" class="lg:col-span-2">Knowledge Prerequisite</label>
	<div class="space-y-2 lg:col-span-5">
		@foreach ($prerequisites as $index => $prerequisite)
			<div wire:key="prerequisite-field-{{ $prerequisite->id }}" class="space-y-2">
				<div class="flex flex-nowrap items-center space-x-2">
					<input type="text" id="prerequisite" wire:model.lazy="prerequisites.{{ $index }}.prerequisite" placeholder="Student should be able to speak" class="basis-11/12 border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
					
					@if ($loop->count > 1)
						<button type="button" wire:click.prevent="deletePrerequisite({{ $prerequisite->id }})" class="flex items-center p-0 bg-red-500 rounded-full hover:opacity-70">
							<span class="material-symbols-outlined text-white text-center rounded-full">
								cancel
							</span>
						</button>
					@else
						<button type="button" wire:click.prevent="deletePrerequisite({{ $prerequisite->id }})" disabled class="flex items-center p-0 bg-slate-200 rounded-full hover:opacity-70">
							<span class="material-symbols-outlined text-white text-center rounded-full">
								cancel
							</span>
						</button>	
					@endif
				</div>
				
				@if ($loop->last)
					<div class="flex flex-nowrap items-center space-x-4">
						<button type="button" wire:click="addPrerequisite" class="my-auto flex items-center px-4 py-1 bg-green-400 rounded-full hover:opacity-70">
							<span class="material-symbols-outlined text-white text-center rounded-full">
								add
							</span>
						</button>
						<span wire:loading.delay wire:target="addPrerequisite">Processing...</span> 
					</div>
				@endif
			</div>
		@endforeach
	</div>
</div>