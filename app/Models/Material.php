<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use HasFactory;
	
	protected $guarded = [];
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	
	public function courses()
	{
		return $this->hasMany(Course::class);
	}
	
	public function assignments()
	{
		return $this->hasMany(Assignment::class);
	}
	
	public function materialSections()
	{
		return $this->hasMany(MaterialSection::class);
	}
}
