<div wire:ignore x-data {{ $attributes }}>
	<script src='https://cdn.tiny.cloud/1/9oknuj0o7vtszi11w9cso0ujnojx2gtx9u2hewuzj0m3rqaw/tinymce/5/tinymce.min.js' referrerpolicy="origin"></script>
	<script>
		tinymce.init({
			path_absolute : "/",
			target: $refs.tinymce,
			relative_urls: false,
			resize: 'both',
			min_height: 500,
			min_width: 300,
			width: 300,
			menubar: false,
			inline: true,
			plugins: [
				'advlist', 'autoresize', 'autolink', 'lists', 'link', 'image', 'charmap', 'print', 'preview', 'hr', 'pagebreak',
				'anchor', 'searchreplace', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'quickbars ', 'codesample',
				'insertdatetime', 'media', 'nonbreaking', 'save', 'table', 'help', 'wordcount', 'directionality',
				'emoticons',  'template', 'paste', 'textpattern', 'powerpaste', 'imagetools'
			],
			codesample_languages: [
				{text: 'HTML/XML', value: 'markup'},
				{text: 'JavaScript', value: 'javascript'},
				{text: 'CSS', value: 'css'},
				{text: 'PHP', value: 'php'},
				{text: 'Ruby', value: 'ruby'},
				{text: 'Python', value: 'python'},
				{text: 'Java', value: 'java'},
				{text: 'C', value: 'c'},
				{text: 'C#', value: 'csharp'},
				{text: 'C++', value: 'cpp'}
			],
			toolbar: 'undo redo codesample code preview | styleselect blocks removeformat emoticons | ' +
				'bold italic backcolor table | alignleft aligncenter ' +
				'alignright alignjustify | bullist numlist outdent indent | ' +
				'link image media help',
			content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
			contextmenu: 'undo redo | inserttable | cell row column deletetable | help',
			quickbars_insert_toolbar: false,
			quickbars_selection_toolbar: 'bold italic underline | styleselect blocks | bullist numlist | backcolor quicklink',
			quickbars_image_toolbar: 'alignleft aligncenter alignright | rotateleft rotateright | imageoptions',
			powerpaste_word_import: 'clean',
			powerpaste_html_import: 'clean',
			
			setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });

                // This section says that when you leave the text edit area, it will set whatever livewire variable you like to the currnt contents
                editor.on('blur', function (e) {
                    @this.set('your_livewire_variable', editor.getContent());
                });
            },
			
			file_picker_callback : function(callback, value, meta) {
				var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
				var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
	 
				var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
				if (meta.filetype == 'image') {
					cmsURL = cmsURL + "&type=Images";
				} else {
					cmsURL = cmsURL + "&type=Files";
				}
	 
				tinyMCE.activeEditor.windowManager.openUrl({
					url : cmsURL,
					title : 'Filemanager',
					width : x * 0.8,
					height : y * 0.8,
					resizable : "yes",
					close_previous : "no",
					onMessage: (api, message) => {
					callback(message.content);
					}
				});
			}
		});
		
	</script>
	
   <textarea x-ref="tinymce" class="h-screen p-2" id="mytextarea" name="mytextarea"></textarea>
</div>