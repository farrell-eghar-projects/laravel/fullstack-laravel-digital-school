<div x-data="{joinCourse: false}" class="bg-white flex flex-col items-center justify-center rounded-md py-8 space-y-4">
  <div class="space-x-2">
    <h5 class="font-semibold text-sm text-dark align-text-top">Only</h5>
    <h5 class="font-semibold text-2xl text-dark">Rp 999.000,-</h5>
  </div>
  
  <button type="button" x-on:click="joinCourse = true" class="bg-primary p-8 font-semibold text-white rounded-lg shadow-md transition duration-300 hover:bg-gradient-to-br hover:from-primary hover:to-teal-400 active:ring active:ring-primary/70">Join Course</button>
  
  <div x-show="joinCourse" x-on:click="joinCourse = false" class="fixed inset-0 bg-secondary/70 animate-fadeIn z-10">
    <div x-on:click.stop class="mx-auto bg-white w-3/4 p-8 space-y-12 rounded-md shadow-2xl translate-y-1/2 z-20 md:w-1/2">
      <span x-on:click="joinCourse = false" class="material-symbols-outlined block float-right text-dark">
        close
      </span>
      
      <div class="space-y-8">
        <div class="space-y-4">
          <div class="{{ $attr }} space-y-2">
            <div class="block font-medium text-red-600">Something went wrong!</div>
            <ul class="list-inside list-disc text-sm text-red-600">
              <li>Course Code or Password is invalid</li>
            </ul>
          </div>

          <div class="space-y-2">
            <label for="code" class="block font-medium text-dark">Course Code</label>
            <input type="text" id="code" wire:model.lazy="code" autocomplete="off" class="default-input block w-full">
          </div>

          <div class="space-y-2">
            <label for="password" class="block font-medium text-dark">Course Password</label>
            <input type="text" id="password" wire:model.lazy="password" autocomplete="off" class="default-input block w-full">
          </div>
        </div>
        
        <div class="flex items-center justify-end">
          <button type="button" wire:click="save" class="bg-primary text-white px-4 py-2 rounded-md float-right hover:opacity-70">Save</button>
        </div>
      </div>
    </div>
  </div>
</div>