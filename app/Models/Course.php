<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
	
	protected $guarded = [];
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	
	public function material()
	{
		return $this->belongsTo(Material::class);
	}
	
	public function type()
	{
		return $this->belongsTo(Type::class);
	}
	
	public function courseAssistants()
	{
		return $this->hasMany(CourseAssistant::class);
	}
	
	public function myCourses()
	{
		return $this->hasMany(MyCourse::class);
	}
	
	public function courseAssignments()
	{
		return $this->hasMany(CourseAssignment::class);
	}
	
	public function coursePrerequisites()
	{
		return $this->hasMany(CoursePrerequisite::class);
	}
	
	public function courseSuitableTargets()
	{
		return $this->hasMany(CourseSuitableTarget::class);
	}
}
