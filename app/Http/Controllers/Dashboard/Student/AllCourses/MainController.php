<?php

namespace App\Http\Controllers\Dashboard\Student\AllCourses;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
	{
    $courses = Course::where('published', true)->get();
    $myCourses = auth()->user()->myCourses;
    $myCoursesID = array();

    foreach ($myCourses as $index => $myCourse) {
      array_push($myCoursesID, $myCourse->id);
    }
    
		return view('pages.dashboard.student.all-courses.index', compact('courses', 'myCoursesID'));
	}
	
	public function show(Course $course)
	{
    $from = Carbon::parse(date('Y-m-d', strtotime($course->started_at)));
    $to = Carbon::parse(date('Y-m-d', strtotime($course->finished_at)));
    $days = $from->diffInDays($to);

    $sessions = array();
		
		foreach ($course->material->materialSections as $index => $materialSection) {
			array_push($sessions, count($materialSection->materialSessions));
		}
    
    $sessionCount = 0;
    foreach ($sessions as $index => $session) {
      $sessionCount += $session;
    }

    $studentCount = 0;
    foreach ($course->user->courses as $index => $userCourse) {
      $studentCount += count($userCourse->myCourses);
    }

    $assistantStudent = array();
    foreach ($course->courseAssistants as $index => $courseAssistant) {
      array_push($assistantStudent, 0);
      foreach ($courseAssistant->user->courses as $key => $course) {
        $assistantStudent[$index] = count($course->myCourses);
      }
    }
    
		return view('pages.dashboard.student.all-courses.show', compact('course', 'days', 'sessionCount', 'studentCount', 'assistantStudent'));
	}
}
