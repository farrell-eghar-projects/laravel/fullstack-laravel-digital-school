<?php

namespace App\Http\Livewire\Dashboard\Material;

use App\Models\Assignment;
use App\Models\AssignmentQuestion;
use App\Models\Material;
use App\Models\MaterialSection;
use App\Models\MaterialSession;
use Livewire\Component;

class MaterialEditor extends Component
{
  public Material $material;
  public $materialSection;
  public $materialSession;
  public $materialAssignment;
  public $assignmentQuestion;
  public $name;

  protected $listeners = [
    'contentUpdate' => 'contentUpdate',
    'refreshComponent' => '$refresh'
  ];

  public function contentUpdate($content)
  {
    $content[0] ? $this->updateSessionContent($content[1]) : $this->updateQuestion($content[1]);
  }

  public function addSection()
  {
    MaterialSection::create([
      'material_id' => $this->material->id,
      'name' => 'Topic Name'
    ]);

    $this->material->refresh();
  }

  public function editSection(MaterialSection $materialSection)
  {
    $this->materialSection = $materialSection;
    $this->name = $this->materialSection->name;
  }

  public function updateSection()
  {
    $this->materialSection->name = $this->name;
    $this->materialSection->save();
    $this->material->refresh();
  }

  public function deleteSection(MaterialSection $materialSection)
  {
    $this->materialSection = '';
    $materialSection->delete();
    $this->material->refresh();
  }

  public function addSession(MaterialSection $materialSection)
  {
    MaterialSession::create([
      'material_section_id' => $materialSection->id,
      'title' => 'Session Title',
      'content' => ''
    ]);

    $this->material->refresh();
  }

  public function editSessionTitle(MaterialSession $materialSession)
  {
    $this->materialSession = $materialSession;
    $this->name = $this->materialSession->title;
  }

  public function editSessionContent(MaterialSession $materialSession)
  {
    $this->materialSession = $materialSession;
    $this->dispatchBrowserEvent('edit-content', ['part' => true, 'content' => $this->materialSession->content]);
  }

  public function updateSessionTitle()
  {
    $this->materialSession->title = $this->name;
    $this->materialSession->save();
    $this->material->refresh();
  }

  public function updateSessionContent($content)
  {
    if ($this->materialSession != '') {
      $this->materialSession->content = $content;
      $this->materialSession->save();
      $this->material->refresh();
    }
  }

  public function deleteSession(MaterialSession $materialSession)
  {
    $this->materialSession = '';
    $materialSession->delete();
    $this->material->refresh();
  }

  public function addAssignment()
  {
    $assignment = Assignment::create([
      'material_id' => $this->material->id,
      'name' => 'Assignment Name'
    ]);

    foreach ($this->material->courses as $index => $course) {
      $course->courseAssignments()->create([
        'assignment_id' => $assignment->id
      ]);
    }

    $this->material->refresh();
  }

  public function editAssignment(Assignment $assignment)
  {
    $this->materialAssignment = $assignment;
    $this->name = $this->materialAssignment->name;
  }

  public function updateAssignment()
  {
    $this->materialAssignment->name = $this->name;
    $this->materialAssignment->save();
    $this->material->refresh();
  }

  public function deleteAssignment(Assignment $assignment)
  {
    $this->materialAssignment = '';
    $assignment->delete();
    $this->material->refresh();
  }

  public function addQuestion(Assignment $assignment)
  {
    AssignmentQuestion::create([
      'assignment_id' => $assignment->id,
      'question' => ''
    ]);

    $this->material->refresh();
  }

  public function editQuestion(AssignmentQuestion $assignmentQuestion)
  {
    $this->assignmentQuestion = $assignmentQuestion;
    $this->dispatchBrowserEvent('edit-content', ['part' => false, 'content' => $this->assignmentQuestion->question]);
  }

  public function updateQuestion($content)
  {
    if ($this->assignmentQuestion != '') {
      $this->assignmentQuestion->question = $content;
      $this->assignmentQuestion->save();
      $this->material->refresh();
    }
  }

  public function deleteQuestion(AssignmentQuestion $assignmentQuestion)
  {
    $this->assignmentQuestion = '';
    $assignmentQuestion->delete();
    $this->material->refresh();
  }

  public function render()
  {
    return view('livewire.dashboard.material.material-editor');
  }
}
