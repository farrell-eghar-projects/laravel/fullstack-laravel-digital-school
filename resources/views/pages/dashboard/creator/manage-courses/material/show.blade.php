@extends('layouts.master-dashboard')

@section('title', 'Material')

@section('content')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/prism.css') }}">
@endpush

{{-- Breadcrumb Start --}}
@livewire('dashboard.components.breadcrumb', ['routes' => [
[
'link' => route('creator.manage-courses.index'),
'name' => 'Manage Courses'
],
[
'link' => route('creator.manage-courses.material.show', ['material' => $material->id]),
'name' => 'Material Details'
]
]
])
{{-- Breadcrumb End --}}

<div class="lg:px-32">
  @livewire('dashboard.material.material-editor', ['material' => $material])
</div>

@push('scripts')
<script src="{{ asset('js/prism.js') }}"></script>
<script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
@endpush
@endsection