<?php

namespace App\Http\Livewire\Dashboard\Components;

use Livewire\Component;

class Breadcrumb extends Component
{
	public $routes;
	
	public function mount($routes)
	{
		foreach ($routes as $key => $route) {
			$routes[$key] = (object)$route;
		}
		
		$this->routes = $routes;
	}
	
    public function render()
    {
		return view('livewire.dashboard.components.breadcrumb');
    }
}
