<?php

namespace App\Http\Controllers\Dashboard\Creator\ManageCourses;

use Carbon\Carbon;
use App\Models\Course;
use App\Models\Assignment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssignmentController extends Controller
{
  public function show(Course $course, Assignment $assignment)
  {
    $courseAssignment = $assignment->courseAssignments()->where('course_id', $course->id)->first();
    $assignmentSubmissions = $courseAssignment->assignmentSubmissions()->orderBy('status', 'desc')->get();
    $scoredSubmission = $assignmentSubmissions->where('score', !null)->count();
    $submissionDurations = array();
    $submissionStatus = array();

    foreach ($assignmentSubmissions as $index => $assignmentSubmission) {
      $from = Carbon::parse(date('H:i:s.u', strtotime(explode(' ', Carbon::now())[1])));
      $to = Carbon::parse(date('H:i:s', strtotime($assignmentSubmission->duration)));

      if ($assignmentSubmission->status || $from > $to) {
        $submissionDurations[] = date('H:i:s', strtotime("00:00:00"));
        $submissionStatus[] = "Submitted";
      } else {
        $from = Carbon::parse(date('H:i:s.u', strtotime(explode(' ', Carbon::now())[1])));
        $to = Carbon::parse(date('H:i:s', strtotime($assignmentSubmission->duration)));
        $hour = (int) ($from->diffInHours($to) % 24);
        $minute = (int) ($from->diffInMinutes($to) % 60);
        $second = (int) ($from->diffInSeconds($to) % 60);
        $submissionDurations[] = date('H:i:s', strtotime($hour . ":" . $minute . ":" . $second));
        $submissionStatus[] = "On Work";
      }
    }

    return view('pages.dashboard.creator.manage-courses.course.assignment.show', compact('course', 'assignment', 'courseAssignment', 'assignmentSubmissions', 'scoredSubmission', 'submissionDurations', 'submissionStatus'));
  }
}
