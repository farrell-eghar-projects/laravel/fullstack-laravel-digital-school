<?php

namespace App\Http\Livewire\Dashboard\Creator\ManageCourse\Course;

use App\Models\Course;
use Livewire\Component;

class AssignmentEdit extends Component
{
  public Course $course;
  public $assignments;

  protected $rules = [
    'course.code' => 'required|string',
    'course.password' => 'required|string',
    'course.description' => 'required|string',
    'course.student_targets' => 'required|string',
    'assignments.*.score' => 'required|string',
    'assignments.*.status' => 'required|string',
    'assignments.*.duration' => 'required|integer'
  ];

  public function mount()
  {
    $this->assignments = $this->course->courseAssignments;
  }

  public function updatedAssignments($value, $key)
  {
    $this->assignments[explode('.', $key)[0]]->save();
    $this->assignments[explode('.', $key)[0]]->refresh();
  }

  public function render()
  {
    return view('livewire.dashboard.creator.manage-course.course.assignment-edit');
  }
}
