<?php

namespace App\Http\Livewire\Dashboard\Form;

use App\Models\Course;
use App\Models\CourseSuitableTarget;
use Livewire\Component;

class SuitableTarget extends Component
{
	public Course $course;
	public $suitableTargets;
	
	protected $rules = [
        'suitableTargets.*.target' => 'required|string',
    ];
	
	protected $listeners = [
		'refreshComponent' => 'getTargets',
	];
	
	public function mount(){
        $this->suitableTargets = CourseSuitableTarget::where('course_id', $this->course->id)->get();
    }

	public function updatedSuitableTargets()
	{
		$this->suitableTargets->each->save();
	}
	
	public function getTargets()
	{
		$this->suitableTargets = CourseSuitableTarget::where('course_id', $this->course->id)->get();
	}
	
	public function deleteTarget(CourseSuitableTarget $target)
	{
		$target->delete();
		$this->emit('refreshComponent');
	}
	
	public function addTarget()
	{
		$this->validate();
		
		CourseSuitableTarget::create([
			'course_id' => $this->course->id
		]);
		
		$this->emit('refreshComponent');
	}
	
    public function render()
    {
        return view('livewire.dashboard.form.suitable-target');
    }
}
