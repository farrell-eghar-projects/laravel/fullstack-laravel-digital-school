<?php

use App\Models\MaterialSection;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_sessions', function (Blueprint $table) {
            $table->id();
			$table->foreignIdFor(MaterialSection::class)->nullable();
			$table->string('title')->nullable();
			$table->longText('content')->nullable();
			$table->foreign('material_section_id')->references('id')->on('material_sections')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_sessions');
    }
};
