<div class="pt-24 lg:pt-28 lg:px-32">
	<div class="container">
		<div class="block px-4">
			@foreach ($routes as $route)
				@if ($loop->first)
					<a href="{{ $route->link }}" class="font-semibold text-xs capitalize underline underline-offset-2 text-primary hover:text-cyan-600">{{ $route->name }}</a>
					@continue
				@endif
				
				@if ($loop->last)
					<span class="font-semibold text-secondary text-xs">/</span>
					<span class="font-semibold text-xs capitalize text-secondary">{{ $route->name }}</span>
					@continue
				@endif
				
				<span class="font-semibold text-secondary text-xs">/</span>
				<a href="{{ $route->link }}" class="font-semibold text-xs capitalize underline underline-offset-2 text-primary hover:text-cyan-600">{{ $route->name }}</a>
			@endforeach
		</div>
	</div>
</div>