const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
	darkMode: 'class',
	
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
		container: {
			center: true,
			padding: '16px'
		},
        extend: {
            fontFamily: {
                sans: ['Rubik', ...defaultTheme.fontFamily.sans],
				logo: ['Fresca', ...defaultTheme.fontFamily.serif]
            },
			colors: {
				primary: '#06b6d4',
				secondary: '#64748b',
				dark: '#0f172a'
			},
			boxShadow: {
				innerBottom: 'inset 10px 10px 20px 0 rgba(0, 0, 0, 0.3)'
			},
			keyframes: {
				slideIn: {
					from: {
						transform: 'translateX(100%)',
						opacity: '0'
					},
					to: {
						transform: 'translateX(0%)',
						opacity: '1'
					}
				},
				switchSlideIn: {
					from: {
						transform: 'translateX(50%)',
						opacity: '0'
					},
					to: {
						transform: 'translateX(-50%)',
						opacity: '1'
					}
				},
				switchSlideOut: {
					from: {
						transform: 'translateX(-50%)',
						opacity: '1'
					},
					to: {
						transform: 'translateX(-150%)',
						opacity: '0'
					}
				},
				slideInFromLeft: {
					from: {
						transform: 'translateX(-100%)',
						opacity: '0'
					},
					to: {
						transform: 'translateX(0%)',
						opacity: '1'
					}
				},
				fadeIn: {
					from: { opacity: '0' },
					to: { opacity:'1' }
				},
				feedbackSlides: {
					'0%': {
						transform: 'translateX(-100%)',
						opacity: '0'
					},
					'10%': {
						transform: 'translateX(0%)',
						opacity: '1'
					},
					'90%': {
						transform: 'translateX(0%)',
						opacity: '1'
					},
					'100%': {
						transform: 'translateX(100%)',
						opacity: '0'
					}
				},
				rotate: {
					from: { transform: 'rotate(0deg)' },
					to: { transform: 'rotate(-180deg)' }
				}
			},
			animation: {
				slideIn: 'slideIn 0.2s ease-in-out 1',
				switchSlideIn: 'switchSlideIn 0.5s ease-in-out 1',
				switchSlideOut: 'switchSlideOut 0.5s ease-in-out 1',
				slideInFromLeft: 'slideInFromLeft 0.2s ease-in-out 1',
				fadeIn: 'fadeIn 0.2s ease-in-out 1',
				feedbackSlides: 'feedbackSlides 5s ease-in-out 1',
				rotate: 'rotate 0.5s linear 1 '
			}
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography'), require('@tailwindcss/line-clamp')],
};
