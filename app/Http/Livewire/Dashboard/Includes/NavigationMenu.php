<?php

namespace App\Http\Livewire\Dashboard\Includes;

use Livewire\Component;

class NavigationMenu extends Component
{
    public function render()
    {
        return view('livewire.dashboard.includes.navigation-menu');
    }
}
