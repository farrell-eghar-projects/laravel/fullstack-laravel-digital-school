<?php

namespace App\Http\Controllers\Dashboard\Student\MyCourses;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Material;
use App\Models\MaterialSession;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
  public function show(Course $course, MaterialSession $session)
  {
    return view('pages.dashboard.student.my-courses.course.material.show', compact('course', 'session'));
  }
}
