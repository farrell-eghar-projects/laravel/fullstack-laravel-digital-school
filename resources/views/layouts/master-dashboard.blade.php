<!DOCTYPE html>
<html class="dark" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>SDS | @yield('title')</title>

        <!-- Fonts -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Fresca&family=Rubik:wght@300;400;500;600;700;800;900&display=swap">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>
		@stack('css')

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @livewireStyles

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased bg-slate-100">
		{{-- Tailwind Supporter --}}
		<span class="hidden animate-rotate"></span>
		<span class="hidden animate-switchSlideIn"></span>
		<span class="hidden animate-switchSlideOut"></span>
		<span class="hidden decoration-[3px] underline-offset-[12px] decoration-primary underline"></span>
		<span class="hidden grayscale hover:grayscale-0"></span>
		{{-- Tailwind Supporter --}}
		
		{{-- Header Start --}}
		@livewire('dashboard.includes.navigation-menu')
		{{-- Header End --}}
		
		@yield('content')
		
		@livewireScripts
		@stack('scripts')
		@stack('script-customize')
		<script src="{{ asset('js/dashboard/script.js') }}"></script>
    </body>
</html>
