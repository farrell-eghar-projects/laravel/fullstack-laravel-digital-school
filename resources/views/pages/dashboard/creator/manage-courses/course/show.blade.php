@extends('layouts.master-dashboard')

@section('title', 'Course Details')
	
@section('content')
	@push('css')
		<link rel="stylesheet" type="text/css" href="{{ asset('css/prism.css') }}">
	@endpush

	{{-- Breadcrumb Start --}}
	@livewire('dashboard.components.breadcrumb', ['routes' => [
			[
				'link' => route('creator.manage-courses.index'),
				'name' => 'Manage Courses'
			],
			[
				'link' => route('creator.manage-courses.course.show', ['course' => 'tapi-boong-wkwkwk']),
				'name' => 'Course Details'
			]
		]
	])
	{{-- Breadcrumb End --}}
	
	<div class="lg:px-32">
		<div class="container">
			<div x-data="{open: 'information'}" class="block bg-white my-2 px-8 py-4 rounded-md shadow-lg">
				{{-- Header Page Start --}}
				<div class="block">
					<h3 class="font-semibold text-xl text-dark text-center p-4 max-w-xl mx-auto lg:text-2xl">Easy Interview with Native Speaker</h3>
					
					<div class="flex items-center justify-between border-b border-slate-200">
						<div class="flex flex-wrap items-center">
							<button type="button" x-bind:class="open == 'information' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'information'" class="px-4 py-8 mr-4 border-b-2">
								Information
							</button>
							
							<button type="button" x-bind:class="open == 'students' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'students'" class="px-4 py-8 mr-4 border-b-2">
								Students
							</button>
							
							<button type="button" x-bind:class="open == 'assignments' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'assignments'" class="px-4 py-8 mr-4 border-b-2">
								Assignments
							</button>
							
							<button type="button" x-bind:class="open == 'certification' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'certification'" class="px-4 py-8 mr-4 border-b-2">
								Certification
							</button>
						</div>
					</div>
				</div>
				{{-- Header Page End --}}
				
				{{-- Main Page Start --}}
				<div class="block p-4">
					{{-- Information Tab Start --}}
					@livewire('dashboard.creator.manage-course.course.information-edit', ['course' => $course])
					{{-- Information Tab End --}}
					
					{{-- Students Tab Start --}}
					<div x-show="open == 'students'" x-transition class="block">
						<div class="flex items-center justify-end my-4">
							<button type="button" class="bg-primary px-4 py-2 text-white rounded-md transition duration-300 hover:ring hover:ring-primary/70">Update Score</button>
						</div>
						
						<div class="block my-4 rounded-lg shadow-md overflow-x-auto scrollbar">
							<table class="w-full sm:table-fixed">
								<thead class="bg-slate-100 text-xs text-dark text-center uppercase">
									<tr>
										<th class="px-4 py-3 sm:w-1/6">Rank</th>
										<th class="px-4 py-3 sm:w-full">Student</th>
										<th class="hidden px-4 py-3 sm:w-1/6 md:table-cell">Score</th>
									</tr>
								</thead>
								
								<tbody x-data="{expand: false}" x-on:click="expand = !expand" class="text-md text-secondary text-left border-b cursor-pointer hover:bg-slate-100">
									<tr>
										<td class="px-4 py-4 text-dark text-center">99999</td>
										<td class="px-4 py-4">
											<div class="flex flex-nowrap items-center justify-between space-x-16">
												<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
													<div class="rounded-full overflow-hidden shrink-0">
														<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
													</div>
													
													<div class="block">
														<h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
														<h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
													</div>
												</div>
												
												<div class="block">
													<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
														expand_more
													</span>
												</div>
											</div>
										</td>
										<td class="hidden px-4 py-4 text-center md:table-cell">98.89</td>
									</tr>
									
									<tr x-show="expand" x-transition>
										<td colspan="3">
											<div class="flex flex-nowrap items-center px-4 py-2 space-x-8 md:hidden">
												<h4 class="font-medium text-dark">Total Score</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5>98.89</h5>
												</span>
											</div>
											
											<div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
												<div class="px-4 py-2 font-medium text-dark sm:col-span-2 lg:col-span-3 xl:col-span-4">Assessment Details</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet.</div>
												<div class="px-4 py-2">Lorem ipsum, dolor sit amet consectetur adipisicing.</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam?</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, deleniti sapiente?</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. In, unde sequi!</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing.</div>
											</div>
										</td>
									</tr>
								</tbody>
								
								<tbody x-data="{expand: false}" x-on:click="expand = !expand" class="text-md text-secondary text-left border-b cursor-pointer hover:bg-slate-100">
									<tr>
										<td class="px-4 py-4 text-dark text-center">99999</td>
										<td class="px-4 py-4">
											<div class="flex flex-nowrap items-center justify-between space-x-16">
												<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
													<div class="rounded-full overflow-hidden shrink-0">
														<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
													</div>
													
													<div class="block">
														<h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
														<h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
													</div>
												</div>
												
												<div class="block">
													<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
														expand_more
													</span>
												</div>
											</div>
										</td>
										<td class="hidden px-4 py-4 text-center md:table-cell">98.89</td>
									</tr>
									
									<tr x-show="expand" x-transition>
										<td colspan="3">
											<div class="flex flex-nowrap items-center px-4 py-2 space-x-8 md:hidden">
												<h4 class="font-medium text-dark">Total Score</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5>98.89</h5>
												</span>
											</div>
											
											<div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
												<div class="px-4 py-2 font-medium text-dark sm:col-span-2 lg:col-span-3 xl:col-span-4">Assessment Details</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet.</div>
												<div class="px-4 py-2">Lorem ipsum, dolor sit amet consectetur adipisicing.</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam?</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, deleniti sapiente?</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. In, unde sequi!</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing.</div>
											</div>
										</td>
									</tr>
								</tbody>
								
								<tbody x-data="{expand: false}" x-on:click="expand = !expand" class="text-md text-secondary text-left border-b cursor-pointer hover:bg-slate-100">
									<tr>
										<td class="px-4 py-4 text-dark text-center">99999</td>
										<td class="px-4 py-4">
											<div class="flex flex-nowrap items-center justify-between space-x-16">
												<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
													<div class="rounded-full overflow-hidden shrink-0">
														<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
													</div>
													
													<div class="block">
														<h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
														<h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
													</div>
												</div>
												
												<div class="block">
													<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
														expand_more
													</span>
												</div>
											</div>
										</td>
										<td class="hidden px-4 py-4 text-center md:table-cell">98.89</td>
									</tr>
									
									<tr x-show="expand" x-transition>
										<td colspan="3">
											<div class="flex flex-nowrap items-center px-4 py-2 space-x-8 md:hidden">
												<h4 class="font-medium text-dark">Total Score</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5>98.89</h5>
												</span>
											</div>
											
											<div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
												<div class="px-4 py-2 font-medium text-dark sm:col-span-2 lg:col-span-3 xl:col-span-4">Assessment Details</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet.</div>
												<div class="px-4 py-2">Lorem ipsum, dolor sit amet consectetur adipisicing.</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam?</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, deleniti sapiente?</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. In, unde sequi!</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing.</div>
											</div>
										</td>
									</tr>
								</tbody>
								
								<tbody x-data="{expand: false}" x-on:click="expand = !expand" class="text-md text-secondary text-left border-b cursor-pointer hover:bg-slate-100">
									<tr>
										<td class="px-4 py-4 text-dark text-center">99999</td>
										<td class="px-4 py-4">
											<div class="flex flex-nowrap items-center justify-between space-x-16">
												<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
													<div class="rounded-full overflow-hidden shrink-0">
														<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
													</div>
													
													<div class="block">
														<h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
														<h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
													</div>
												</div>
												
												<div class="block">
													<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
														expand_more
													</span>
												</div>
											</div>
										</td>
										<td class="hidden px-4 py-4 text-center md:table-cell">98.89</td>
									</tr>
									
									<tr x-show="expand" x-transition>
										<td colspan="3">
											<div class="flex flex-nowrap items-center px-4 py-2 space-x-8 md:hidden">
												<h4 class="font-medium text-dark">Total Score</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5>98.89</h5>
												</span>
											</div>
											
											<div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
												<div class="px-4 py-2 font-medium text-dark sm:col-span-2 lg:col-span-3 xl:col-span-4">Assessment Details</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet.</div>
												<div class="px-4 py-2">Lorem ipsum, dolor sit amet consectetur adipisicing.</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam?</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, deleniti sapiente?</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. In, unde sequi!</div>
												<div class="px-4 py-2">Lorem ipsum dolor sit amet consectetur adipisicing.</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					{{-- Students Tab End --}}
					
					{{-- Assignments Tab Start --}}
					@livewire('dashboard.creator.manage-course.course.assignment-edit', ['course' => $course])
					{{-- Assignments Tab End --}}
					
					{{-- Certification Tab Start --}}
					<div x-show="open == 'certification'" x-transition class="block space-y-8">
						<h3 class="font-semibold text-lg text-dark whitespace-nowrap md:col-span-2 xl:col-span-1">Certification Criteria</h3>
						
						<div x-data="{drop: false}" class="grid grid-cols-1 gap-4 my-4 -space-y-1 sm:flex sm:flex-nowrap sm:space-x-4 md:space-x-8">
							<div x-on:click="drop = !drop" class="block w-full -space-y-1 sm:w-48">
								<span class="flex flex-nowrap space-x-2 -space-y-1 float-left pointer-events-none">
									<input type="checkbox" x-bind:checked="drop" name="mastered" id="mastered" class="default-input-checkbox-rounded">
									<label x-bind:class="drop ? 'text-dark': 'text-secondary'" class="font-medium whitespace-nowrap md:col-span-2 xl:col-span-1">Mastered</label>
								</span>
								
								<span x-bind:class="drop ? 'text-primary': 'text-secondary rotate-90'" class="hidden material-symbols-outlined text-right float-right pointer-events-none transition duration-300 sm:inline">
									arrow_right
								</span>
								
								<span x-bind:class="drop ? 'text-primary rotate-180': 'text-secondary'" class="material-symbols-outlined text-right float-right pointer-events-none transition duration-300 sm:hidden">
									expand_more
								</span>
							</div>
							
							<div x-show="drop" x-transition class="grid grid-cols-1 gap-4 ml-4 sm:ml-0 xl:gap-6 2xl:gap-8">
								<div x-data="{active: false}" class="flex flex-nowrap items-center space-x-2">
									<input type="checkbox" x-on:click="active = !active" id="mastered_min_score" class="default-input-checkbox">
									<label for="mastered_min_score" x-bind:class="active ? 'text-dark' : 'text-secondary'">Minimum score</label>
									<input type="text" x-bind:disabled="!active" name="min_score" class="w-10 border-0 border-b-2 border-dark text-center px-1 py-0 focus:ring-0 focus:border-primary disabled:border-secondary/70">
								</div>
								
								<div class="grid grid-cols-1 gap-4 xl:grid-cols-2 xl:gap-6 2xl:gap-8">
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="mastered_assignment1" class="default-input-checkbox">
										<label for="mastered_assignment1" x-bind:class="active ? 'text-dark' : 'text-secondary'">First Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="mastered_assignment2" class="default-input-checkbox">
										<label for="mastered_assignment2" x-bind:class="active ? 'text-dark' : 'text-secondary'">Second Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="mastered_assignment3" class="default-input-checkbox">
										<label for="mastered_assignment3" x-bind:class="active ? 'text-dark' : 'text-secondary'">Third Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="mastered_assignment4" class="default-input-checkbox">
										<label for="mastered_assignment4" x-bind:class="active ? 'text-dark' : 'text-secondary'">Forth Week assignment is required</label>
									</div>
								</div>
							</div>
						</div>
						
						<div x-data="{drop: false}" class="grid grid-cols-1 gap-4 my-4 -space-y-1 sm:flex sm:flex-nowrap sm:space-x-4 md:space-x-8">
							<div x-on:click="drop = !drop" class="block w-full -space-y-1 sm:w-48">
								<span class="flex flex-nowrap space-x-2 -space-y-1 float-left pointer-events-none">
									<input type="checkbox" x-bind:checked="drop" name="mastered" id="mastered" class="default-input-checkbox-rounded">
									<label x-bind:class="drop ? 'text-dark': 'text-secondary'" class="font-medium whitespace-nowrap md:col-span-2 xl:col-span-1">Completed</label>
								</span>
								
								<span x-bind:class="drop ? 'text-primary': 'text-secondary rotate-90'" class="hidden material-symbols-outlined text-right float-right pointer-events-none transition duration-300 sm:inline">
									arrow_right
								</span>
								
								<span x-bind:class="drop ? 'text-primary rotate-180': 'text-secondary'" class="material-symbols-outlined text-right float-right pointer-events-none transition duration-300 sm:hidden">
									expand_more
								</span>
							</div>
							
							<div x-show="drop" x-transition class="grid grid-cols-1 gap-4 ml-4 sm:ml-0 xl:gap-6 2xl:gap-8">
								<div x-data="{active: false}" class="flex flex-nowrap items-center space-x-2">
									<input type="checkbox" x-on:click="active = !active" id="completed_min_score" class="default-input-checkbox">
									<label for="completed_min_score" x-bind:class="active ? 'text-dark' : 'text-secondary'">Minimum score</label>
									<input type="text" x-bind:disabled="!active" name="min_score" class="w-10 border-0 border-b-2 border-dark text-center px-1 py-0 focus:ring-0 focus:border-primary disabled:border-secondary/70">
								</div>
								
								<div class="grid grid-cols-1 gap-4 xl:grid-cols-2 xl:gap-6 2xl:gap-8">
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="completed_assignment1" class="default-input-checkbox">
										<label for="completed_assignment1" x-bind:class="active ? 'text-dark' : 'text-secondary'">First Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="completed_assignment2" class="default-input-checkbox">
										<label for="completed_assignment2" x-bind:class="active ? 'text-dark' : 'text-secondary'">Second Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="completed_assignment3" class="default-input-checkbox">
										<label for="completed_assignment3" x-bind:class="active ? 'text-dark' : 'text-secondary'">Third Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="completed_assignment4" class="default-input-checkbox">
										<label for="completed_assignment4" x-bind:class="active ? 'text-dark' : 'text-secondary'">Forth Week assignment is required</label>
									</div>
								</div>
							</div>
						</div>
						
						<div x-data="{drop: false}" class="grid grid-cols-1 gap-4 my-4 -space-y-1 sm:flex sm:flex-nowrap sm:space-x-4 md:space-x-8">
							<div x-on:click="drop = !drop" class="block w-full -space-y-1 sm:w-48">
								<span class="flex flex-nowrap space-x-2 -space-y-1 float-left pointer-events-none">
									<input type="checkbox" x-bind:checked="drop" name="mastered" id="mastered" class="default-input-checkbox-rounded">
									<label x-bind:class="drop ? 'text-dark': 'text-secondary'" class="font-medium whitespace-nowrap md:col-span-2 xl:col-span-1">Participated</label>
								</span>
								
								<span x-bind:class="drop ? 'text-primary': 'text-secondary rotate-90'" class="hidden material-symbols-outlined text-right float-right pointer-events-none transition duration-300 sm:inline">
									arrow_right
								</span>
								
								<span x-bind:class="drop ? 'text-primary rotate-180': 'text-secondary'" class="material-symbols-outlined text-right float-right pointer-events-none transition duration-300 sm:hidden">
									expand_more
								</span>
							</div>
							
							<div x-show="drop" x-transition class="grid grid-cols-1 gap-4 ml-4 sm:ml-0 xl:gap-6 2xl:gap-8">
								<div x-data="{active: false}" class="flex flex-nowrap items-center space-x-2">
									<input type="checkbox" x-on:click="active = !active" id="participated_min_score" class="default-input-checkbox">
									<label for="participated_min_score" x-bind:class="active ? 'text-dark' : 'text-secondary'">Minimum score</label>
									<input type="text" x-bind:disabled="!active" name="min_score" class="w-10 border-0 border-b-2 border-dark text-center px-1 py-0 focus:ring-0 focus:border-primary disabled:border-secondary/70">
								</div>
								
								<div class="grid grid-cols-1 gap-4 xl:grid-cols-2 xl:gap-6 2xl:gap-8">
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="participated_assignment1" class="default-input-checkbox">
										<label for="participated_assignment1" x-bind:class="active ? 'text-dark' : 'text-secondary'">First Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="participated_assignment2" class="default-input-checkbox">
										<label for="participated_assignment2" x-bind:class="active ? 'text-dark' : 'text-secondary'">Second Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="participated_assignment3" class="default-input-checkbox">
										<label for="participated_assignment3" x-bind:class="active ? 'text-dark' : 'text-secondary'">Third Week assignment is required</label>
									</div>
									
									<div x-data="{active: false}" class="flex flex-nowrap space-x-2 -space-y-1">
										<input type="checkbox" x-on:click="active = !active" id="participated_assignment4" class="default-input-checkbox">
										<label for="participated_assignment4" x-bind:class="active ? 'text-dark' : 'text-secondary'">Forth Week assignment is required</label>
									</div>
								</div>
							</div>
						</div>
						
						<div class="block my-4 rounded-lg shadow-md overflow-x-auto scrollbar">
							<table class="w-full sm:table-fixed">
								<thead class="bg-slate-100 text-xs text-dark text-center uppercase">
									<tr>
										<th class="px-4 py-3 sm:w-full">Student</th>	
										<th class="hidden px-4 py-3 sm:w-3/6 sm:table-cell">Certificate</th>
										<th class="hidden px-4 py-3 sm:w-3/6 md:table-cell">Released At</th>
									</tr>
								</thead>
								
								<tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
									<tr>
										<td class="px-4 py-4">
											<div class="flex flex-nowrap items-center justify-between space-x-16">
												<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
													<div class="rounded-full overflow-hidden shrink-0">
														<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
													</div>
													
													<div class="block">
														<h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
														<h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
													</div>
												</div>
												
												<div class="block">
													<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary sm:hidden">
														expand_more
													</span>
												</div>
											</div>
										</td>
										<td class="hidden relative px-4 py-4 text-center text-dark sm:table-cell">
											Mastered
											
											<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined absolute top-1/2 -right-0.5 -translate-y-1/2 transition duration-300 hover:text-primary md:hidden">
												expand_more
											</span>
										</td>
										<td class="hidden px-4 py-4 text-dark text-center md:table-cell">2022-07-18 18:18:18</td>
									</tr>
									
									<tr x-show="expand" x-transition class="md:hidden">
										<td colspan="3">
											<div class="grid grid-cols-2 px-4 py-2 sm:hidden">
												<h4 class="font-medium text-dark whitespace-nowrap">Certificate</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5 class="text-secondary">Mastered</h5>
												</span>
											</div>
											
											<div class="grid grid-cols-2 px-4 py-2 sm:flex sm:flex-nowrap">
												<h4 class="basis-32 font-medium text-dark whitespace-nowrap">Released At</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5 class="text-secondary">2022-07-18 18:18:18</h5>
												</span>
											</div>
										</td>
									</tr>
								</tbody>
								
								<tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
									<tr>
										<td class="px-4 py-4">
											<div class="flex flex-nowrap items-center justify-between space-x-16">
												<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
													<div class="rounded-full overflow-hidden shrink-0">
														<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
													</div>
													
													<div class="block">
														<h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
														<h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
													</div>
												</div>
												
												<div class="block">
													<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary sm:hidden">
														expand_more
													</span>
												</div>
											</div>
										</td>
										<td class="hidden relative px-4 py-4 text-center text-dark sm:table-cell">
											Mastered
											
											<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined absolute top-1/2 -right-0.5 -translate-y-1/2 transition duration-300 hover:text-primary md:hidden">
												expand_more
											</span>
										</td>
										<td class="hidden px-4 py-4 text-dark text-center md:table-cell">2022-07-18 18:18:18</td>
									</tr>
									
									<tr x-show="expand" x-transition class="md:hidden">
										<td colspan="3">
											<div class="grid grid-cols-2 px-4 py-2 sm:hidden">
												<h4 class="basis-32 font-medium text-dark whitespace-nowrap">Certificate</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5 class="text-secondary">Mastered</h5>
												</span>
											</div>
											
											<div class="grid grid-cols-2 px-4 py-2 sm:flex sm:flex-nowrap">
												<h4 class="basis-32 font-medium text-dark whitespace-nowrap">Released At</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5 class="text-secondary">2022-07-18 18:18:18</h5>
												</span>
											</div>
										</td>
									</tr>
								</tbody>
								
								<tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
									<tr>
										<td class="px-4 py-4">
											<div class="flex flex-nowrap items-center justify-between space-x-16">
												<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
													<div class="rounded-full overflow-hidden shrink-0">
														<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
													</div>
													
													<div class="block">
														<h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
														<h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
													</div>
												</div>
												
												<div class="block">
													<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary sm:hidden">
														expand_more
													</span>
												</div>
											</div>
										</td>
										<td class="hidden relative px-4 py-4 text-center text-dark sm:table-cell">
											Mastered
											
											<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined absolute top-1/2 -right-0.5 -translate-y-1/2 transition duration-300 hover:text-primary md:hidden">
												expand_more
											</span>
										</td>
										<td class="hidden px-4 py-4 text-dark text-center md:table-cell">2022-07-18 18:18:18</td>
									</tr>
									
									<tr x-show="expand" x-transition class="md:hidden">
										<td colspan="3">
											<div class="grid grid-cols-2 px-4 py-2 sm:hidden">
												<h4 class="basis-32 font-medium text-dark whitespace-nowrap">Certificate</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5 class="text-secondary">Mastered</h5>
												</span>
											</div>
											
											<div class="grid grid-cols-2 px-4 py-2 sm:flex sm:flex-nowrap">
												<h4 class="basis-32 font-medium text-dark whitespace-nowrap">Released At</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5 class="text-secondary">2022-07-18 18:18:18</h5>
												</span>
											</div>
										</td>
									</tr>
								</tbody>
								
								<tbody x-data="{expand: false}" x-on:click="expand = !expand" class="border-b cursor-pointer hover:bg-slate-100">
									<tr>
										<td class="px-4 py-4">
											<div class="flex flex-nowrap items-center justify-between space-x-16">
												<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
													<div class="rounded-full overflow-hidden shrink-0">
														<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
													</div>
													
													<div class="block">
														<h4 class="font-medium text-dark text-md">Mohammed Farrell Eghar Syahputra</h4>
														<h5 class="text-secondary text-sm">farrell.eghar@serpihan.id</h5>
													</div>
												</div>
												
												<div class="block">
													<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary sm:hidden">
														expand_more
													</span>
												</div>
											</div>
										</td>
										<td class="hidden relative px-4 py-4 text-center text-dark sm:table-cell">
											Mastered
											
											<span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined absolute top-1/2 -right-0.5 -translate-y-1/2 transition duration-300 hover:text-primary md:hidden">
												expand_more
											</span>
										</td>
										<td class="hidden px-4 py-4 text-dark text-center md:table-cell">2022-07-18 18:18:18</td>
									</tr>
									
									<tr x-show="expand" x-transition class="md:hidden">
										<td colspan="3">
											<div class="grid grid-cols-2 px-4 py-2 sm:hidden">
												<h4 class="basis-32 font-medium text-dark whitespace-nowrap">Certificate</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5 class="text-secondary">Mastered</h5>
												</span>
											</div>
											
											<div class="grid grid-cols-2 px-4 py-2 sm:flex sm:flex-nowrap">
												<h4 class="basis-32 font-medium text-dark whitespace-nowrap">Released At</h4>
												<span class="flex flex-nowrap space-x-2">
													<h5>:</h5>
													<h5 class="text-secondary">2022-07-18 18:18:18</h5>
												</span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					{{-- Certification Tab End --}}
				</div>
				{{-- Main Page End --}}
			</div>
		</div>
	</div>
	
	@push('scripts')
		<script src="{{ asset('js/prism.js') }}"></script>
		<script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
	@endpush
@endsection