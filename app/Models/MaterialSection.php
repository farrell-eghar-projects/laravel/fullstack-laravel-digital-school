<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaterialSection extends Model
{
    use HasFactory;
	
	protected $guarded = [];
	
	public function material()
	{
		return $this->belongsTo(Material::class);
	}
	
	public function materialSessions()
	{
		return $this->hasMany(MaterialSession::class);
	}
}
