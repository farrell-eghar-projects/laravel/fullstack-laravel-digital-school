<?php

use App\Models\Assignment;
use App\Models\Course;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_assignments', function (Blueprint $table) {
            $table->id();
			$table->foreignIdFor(Course::class)->nullable();
			$table->foreignIdFor(Assignment::class)->nullable();
			$table->integer('score')->nullable();
			$table->boolean('status')->nullable();
			$table->integer('duration')->nullable();
			$table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
			$table->foreign('assignment_id')->references('id')->on('assignments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_assignments');
    }
};
