// Dashboard Avatar and Notification
const dashboardAvatar = document.querySelector('#avatar')
const avatarMenu = document.querySelector('#avatar-menu')
const dashboardNotif = document.querySelector('#notification')
const notifMenu = document.querySelector('#notification-menu')

dashboardAvatar.addEventListener('click', function () {
	avatarMenu.classList.toggle('hidden')
	if (!notifMenu.classList.contains('hidden')) {
		notifMenu.classList.toggle('hidden')
	} 
})

dashboardNotif.addEventListener('click', function () {
	notifMenu.classList.toggle('hidden')
	if (!avatarMenu.classList.contains('hidden')) {
		avatarMenu.classList.toggle('hidden')
	}
})

// Dashboard Switch Mode
const switchMode = document.querySelector('#switch-mode')
const studentMode = document.querySelector('#student-mode')
const creatorMode = document.querySelector('#creator-mode')
let mode = studentMode.classList.contains('flex')

switchMode.addEventListener('click', function () {
	switchMode.classList.toggle('animate-rotate')
	
	if (mode) {
		creatorMode.classList.remove('hidden')
		creatorMode.classList.add('flex')
		creatorMode.classList.add('animate-switchSlideIn')
		studentMode.classList.add('animate-switchSlideOut')
		setTimeout(switchToCreator, 500)
	} else {
		studentMode.classList.remove('hidden')
		studentMode.classList.add('flex')
		studentMode.classList.add('animate-switchSlideIn')
		creatorMode.classList.add('animate-switchSlideOut')
		setTimeout(switchToStudent, 500)
	}
	
	mode = !mode
	setTimeout(switchModeToggle, 500)
})

function switchToCreator() {
	studentMode.classList.add('hidden')
	studentMode.classList.remove('flex')
	studentMode.classList.remove('animate-switchSlideOut')
	studentMode.classList.remove('animate-switchSlideIn')
}

function switchToStudent() {
	creatorMode.classList.add('hidden')
	creatorMode.classList.remove('flex')
	creatorMode.classList.remove('animate-switchSlideOut')
	creatorMode.classList.remove('animate-switchSlideIn')
}

function switchModeToggle() {
	switchMode.classList.toggle('animate-rotate')
}

// Dashboard Hamburger
const hamburger = document.querySelector('#hamburger')
const hamburgerClose = document.querySelector('#hamburger-close')
const navMenu = document.querySelector('#nav-sm')

hamburger.addEventListener('click', function () {
	navMenu.classList.toggle('hidden')
})

hamburgerClose.addEventListener('click', function () {
	navMenu.classList.toggle('hidden')
})

// Windows Onclick Events
window.onclick = function(event) {
	// Dashboard Avatar & Notification
	if (!avatarMenu.classList.contains('hidden') && event.target != dashboardAvatar && event.target != avatarMenu) {
		avatarMenu.classList.add('hidden')
	}
	
	if (!notifMenu.classList.contains('hidden') && event.target != dashboardNotif && event.target != notifMenu) {
		notifMenu.classList.add('hidden')
	}
	
	// Dashboard Hamburger
	if (event.target == navMenu) {
		navMenu.classList.add('hidden')
	}
}