<?php

namespace App\Http\Controllers\Dashboard\Student\MyCourses;

use App\Http\Controllers\Controller;
use App\Models\Assignment;
use App\Models\AssignmentSubmission;
use App\Models\Course;
use App\Models\CourseAssignment;
use App\Models\SubmissionAnswer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AssignmentController extends Controller
{
  public function show(Course $course, Assignment $assignment)
  {
    $courseAssignment = $course->courseAssignments()->where('assignment_id', $assignment->id)->first();
    $assignmentSubmission = $courseAssignment->assignmentSubmissions()->where('user_id', auth()->id())->first();

    if (empty($assignmentSubmission)) {
      $assignmentSubmission = AssignmentSubmission::create([
        'user_id' => auth()->id(),
        'course_assignment_id' => $courseAssignment->id,
        'duration' => Carbon::now()->addHours((int) ($courseAssignment->duration / 60))->addMinutes($courseAssignment->duration % 60)
      ]);

      foreach ($assignment->assignmentQuestions as $index => $assignmentQuestion) {
        $assignmentSubmission->submissionAnswers()->create([
          'assignment_question_id' => $assignmentQuestion->id,
          'answer' => ''
        ]);
      }
    }

    // if (Carbon::now() > $assignmentSubmission->duration || ) {
    //   dd(Carbon::now()->diffInMilliseconds($assignmentSubmission->duration));
    // }

    return view('pages.dashboard.student.my-courses.course.assignment.show', compact('course', 'assignment', 'courseAssignment', 'assignmentSubmission'));
  }
}
