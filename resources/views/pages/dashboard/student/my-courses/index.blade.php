@extends('layouts.master-dashboard')

@section('title', 'My Courses')

@section('content')
	{{-- Header Page Start --}}
	<div class="pt-24 lg:pt-28 lg:px-32">
		<div class="container">
			<div class="block shadow-md">
				<h3 class="font-semibold text-lg text-white bg-primary px-4 py-2 rounded-t-md">Latest events</h3>
				
				<div class="flex items-center w-full divide-x py-4 bg-white rounded-b-md">
					<div class="block w-full px-8 space-y-6 lg:w-1/2">
						<div class="group flex items-center space-x-4 cursor-default">
							<span class="material-symbols-outlined font-medium text-5xl text-secondary group-hover:text-cyan-200">
								assignment_turned_in
							</span>
							
							<div class="block">
								<h5 class="text-dark group-hover:text-cyan-400">
									<span class="font-semibold group-hover:text-primary">You</span>
									have submitted assignment 1 of
									<span class="font-semibold group-hover:text-primary">Easy English Interview with Native Speaker</span>
								</h5>
								
								<p class="font-medium text-secondary text-xs group-hover:text-cyan-200">2022-06-22 13:25 WIB</p>
							</div>
						</div>
						
						<div class="group flex items-center space-x-4 cursor-default">
							<span class="material-symbols-outlined font-medium text-5xl text-secondary group-hover:text-cyan-200">
								assignment_turned_in
							</span>
							
							<div class="block">
								<h5 class="text-dark group-hover:text-cyan-400">
									<span class="font-semibold group-hover:text-primary">You</span>
									have submitted assignment 1 of
									<span class="font-semibold group-hover:text-primary">Easy English Interview with Native Speaker</span>
								</h5>
								
								<p class="font-medium text-secondary text-xs group-hover:text-cyan-200">2022-06-22 13:25 WIB</p>
							</div>
						</div>
						
						<div class="group flex items-center space-x-4 cursor-default">
							<span class="material-symbols-outlined font-medium text-5xl text-secondary group-hover:text-cyan-200">
								assignment_turned_in
							</span>
							
							<div class="block">
								<h5 class="text-dark group-hover:text-cyan-400">
									<span class="font-semibold group-hover:text-primary">You</span>
									have submitted assignment 1 of
									<span class="font-semibold group-hover:text-primary">Easy English Interview with Native Speaker</span>
								</h5>
								
								<p class="font-medium text-secondary text-xs group-hover:text-cyan-200">2022-06-22 13:25 WIB</p>
							</div>
						</div>
						
						<div class="group flex items-center space-x-4 cursor-default">
							<span class="material-symbols-outlined font-medium text-5xl text-secondary group-hover:text-cyan-200">
								assignment_turned_in
							</span>
							
							<div class="block">
								<h5 class="text-dark group-hover:text-cyan-400">
									<span class="font-semibold group-hover:text-primary">You</span>
									have submitted assignment 1 of
									<span class="font-semibold group-hover:text-primary">Easy English Interview with Native Speaker</span>
								</h5>
								
								<p class="font-medium text-secondary text-xs group-hover:text-cyan-200">2022-06-22 13:25 WIB</p>
							</div>
						</div>
					</div>
					
					<div class="hidden w-1/2 px-8 space-y-6 lg:block">
						<div class="group flex items-center space-x-4 cursor-default">
							<span class="material-symbols-outlined font-medium text-5xl text-secondary group-hover:text-cyan-200">
								assignment_turned_in
							</span>
							
							<div class="block">
								<h5 class="text-dark group-hover:text-cyan-400">
									<span class="font-semibold group-hover:text-primary">You</span>
									have submitted assignment 1 of
									<span class="font-semibold group-hover:text-primary">Easy English Interview with Native Speaker</span>
								</h5>
								
								<p class="font-medium text-secondary text-xs group-hover:text-cyan-200">2022-06-22 13:25 WIB</p>
							</div>
						</div>
						
						<div class="group flex items-center space-x-4 cursor-default">
							<span class="material-symbols-outlined font-medium text-5xl text-secondary group-hover:text-cyan-200">
								assignment_turned_in
							</span>
							
							<div class="block">
								<h5 class="text-dark group-hover:text-cyan-400">
									<span class="font-semibold group-hover:text-primary">You</span>
									have submitted assignment 1 of
									<span class="font-semibold group-hover:text-primary">Easy English Interview with Native Speaker</span>
								</h5>
								
								<p class="font-medium text-secondary text-xs group-hover:text-cyan-200">2022-06-22 13:25 WIB</p>
							</div>
						</div>
						
						<div class="group flex items-center space-x-4 cursor-default">
							<span class="material-symbols-outlined font-medium text-5xl text-secondary group-hover:text-cyan-200">
								assignment_turned_in
							</span>
							
							<div class="block">
								<h5 class="text-dark group-hover:text-cyan-400">
									<span class="font-semibold group-hover:text-primary">You</span>
									have submitted assignment 1 of
									<span class="font-semibold group-hover:text-primary">Easy English Interview with Native Speaker</span>
								</h5>
								
								<p class="font-medium text-secondary text-xs group-hover:text-cyan-200">2022-06-22 13:25 WIB</p>
							</div>
						</div>
						
						<div class="group flex items-center space-x-4 cursor-default">
							<span class="material-symbols-outlined font-medium text-5xl text-secondary group-hover:text-cyan-200">
								assignment_turned_in
							</span>
							
							<div class="block">
								<h5 class="text-dark group-hover:text-cyan-400">
									<span class="font-semibold group-hover:text-primary">You</span>
									have submitted assignment 1 of
									<span class="font-semibold group-hover:text-primary">Easy English Interview with Native Speaker</span>
								</h5>
								
								<p class="font-medium text-secondary text-xs group-hover:text-cyan-200">2022-06-22 13:25 WIB</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- Header Page End --}}
	
	{{-- Main Content Start --}}
	<div class="lg:px-32">
		<div class="container">
			<div x-data="{open: 'my-courses'}" class="bg-white shadow-md rounded-md px-12 my-8">
				<div class="flex items-center justify-between border-b border-slate-200">
					<div class="flex flex-wrap items-center">
						<button type="button" x-bind:class="open == 'my-courses' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'my-courses'" class="px-4 py-8 mr-4 border-b-2">
							My Courses
						</button>
						
						<button type="button" x-bind:class="open == 'my-certificates' ? 'text-primary border-primary' : 'text-secondary border-transparent hover:border-secondary hover:text-dark'" x-on:click="open = 'my-certificates'" class="px-4 py-8 mr-4 border-b-2">
							My Certificates
						</button>
					</div>
					
					<div class="hidden items-center py-4 lg:flex">
						<select x-bind:class="open == 'my-courses' ? '' : 'hidden'" name="course_sort" id="course_sort" class="bg-transparent text-secondary border-0 focus:outline-none focus:ring-0">
							<option value="all">All Courses</option>
							<option value="lifetime">Lifetime</option>
							<option value="intensive">Intensive</option>
							<option value="subscription">Subscription</option>
						</select>
					</div> 
				</div>
				
				<div class="py-4 lg:py-8">
					{{-- My Courses Start --}}
					<div x-show="open == 'my-courses'" x-transition class="block">
						<div class="flex items-center justify-end py-4 lg:hidden">
							<select name="course_sort" id="course_sort" class="bg-transparent text-secondary border-0 border-b border-slate-200 focus:outline-none focus:ring-0 focus:border-slate-100">
								<option value="all">All Courses</option>
								<option value="lifetime">Lifetime</option>
								<option value="intensive">Intensive</option>
								<option value="subscription">Subscription</option>
							</select>
						</div>
						
						<div class="grid grid-cols-1 gap-8 py-4 sm:grid-cols-2 xl:grid-cols-3">
              @foreach ($myCourses as $index => $myCourse)
                <a href="{{ route('student.my-courses.course.show', ['course' => $myCourse->course->id]) }}" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
                  <div class="w-full rounded-md overflow-hidden shrink-0">
                    <img src="{{ $myCourse->course->thumbnail }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
                  </div>
                  
                  <div class="p-4 space-y-2">
                    <h3 class="font-semibold text-lg text-dark">{{ $myCourse->course->title }}</h3>
                    
                    <div class="block space-y-1">
                      <div class="flex flex-nowrap items-center space-x-2">
                        <span class="material-symbols-outlined text-green-400">
                          sticky_note_2
                        </span>
                        
                        <span class="w-1/2 font-semibold text-green-400 text-xs whitespace-nowrap">{{ $sessionCount[$index] }} Sessions</span>
                      </div>
                      
                      <div class="flex flex-nowrap items-center space-x-2">
                        <span class="material-symbols-outlined text-yellow-400">
                          assignment
                        </span>
                        
                        <span class="w-1/2 font-semibold text-yellow-400 text-xs whitespace-nowrap">{{ count($myCourse->course->material->assignments) }} Assignments</span>
                      </div>
                    </div>
                  </div>
                </a>
              @endforeach
						</div>
					</div>
					{{-- My Courses End --}}
					
					{{-- My Certificates Start --}}
					<div x-show="open == 'my-certificates'" x-transition class="block">
						<div class="grid grid-cols-1 gap-8 sm:grid-cols-2 xl:grid-cols-3">
							<a href="#" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
								<div class="w-full rounded-md overflow-hidden shrink-0">
									<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
								</div>
								
								<div class="p-4 space-y-2">
									<h3 class="font-semibold text-lg text-dark">Easy English Interview with Native Speaker</h3>
									
									<div class="flex flex-nowrap items-center space-x-2">
										<span class="material-symbols-outlined font-semibold text-blue-400">
											verified
										</span>
										
										<p class="font-semibold text-sm text-secondary opacity-80">Permanent Certification</p>
									</div>
								</div>
							</a>
							
							<a href="#" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
								<div class="w-full rounded-md overflow-hidden shrink-0">
									<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
								</div>
								
								<div class="p-4 space-y-2">
									<h3 class="font-semibold text-lg text-dark">Easy English Interview with Native Speaker</h3>
									
									<div class="flex flex-nowrap items-center space-x-2">
										<span class="material-symbols-outlined font-semibold text-blue-400">
											verified
										</span>
										
										<p class="font-semibold text-sm text-secondary opacity-80">Permanent Certification</p>
									</div>
								</div>
							</a>
							
							<a href="#" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
								<div class="w-full rounded-md overflow-hidden shrink-0">
									<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
								</div>
								
								<div class="p-4 space-y-2">
									<h3 class="font-semibold text-lg text-dark">Easy English Interview with Native Speaker</h3>
									
									<div class="flex flex-nowrap items-center space-x-2">
										<span class="material-symbols-outlined font-semibold text-blue-400">
											verified
										</span>
										
										<p class="font-semibold text-sm text-secondary opacity-80">Permanent Certification</p>
									</div>
								</div>
							</a>
							
							<a href="#" class="group w-full rounded-md shadow-lg transition duration-500 hover:shadow-2xl">
								<div class="w-full rounded-md overflow-hidden shrink-0">
									<img src="{{ url('https://source.unsplash.com/500x250?web+design') }}" alt="Web Design" class="w-full object-cover transition duration-500 group-hover:scale-125">
								</div>
								
								<div class="p-4 space-y-2">
									<h3 class="font-semibold text-lg text-dark">Easy English Interview with Native Speaker</h3>
									
									<div class="flex flex-nowrap items-center space-x-2">
										<span class="material-symbols-outlined font-semibold text-blue-400">
											verified
										</span>
										
										<p class="font-semibold text-sm text-secondary opacity-80">Permanent Certification</p>
									</div>
								</div>
							</a>
						</div>
					</div>
					{{-- My Certificates End --}}
				</div>
			</div>
			
		</div>
	</div>
	{{-- Main Content End --}}
@endsection