<!DOCTYPE html>
<html class="dark" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Fresca&family=Rubik:wght@300;400;500;600;700;800;900&display=swap">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"/>
		@stack('css')

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @livewireStyles

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
		{{-- Header Start --}}
		<header class="bg-transparent backdrop-blur-sm absolute top-0 left-0 w-full flex items-center z-10">
			<div class="container">
				<div class="flex items-center justify-between relative px-4 py-1">
					<a href="#" class="flex shrink-0 space-x-3 items-center">
						<img src="{{ asset('assets/logo.png') }}" alt="Logo" class="h-12 w-12">
						<div class="font-logo items-center">
							<p class="font-bold text-primary text-lg leading-tight">Serpihan</p>
							<p class="font-medium text-base leading-tight text-secondary">Digital School</p>
						</div>
					</a>
					
					<nav id="nav-lg" class="hidden font-serif space-x-16 lg:flex lg:justify-end lg:items-center">
						{{--
						<div class="space-x-6">
							<a href="#" class="font-semibold rounded-lg py-1 px-2 text-dark hover:text-primary hover:scale-125 active:outline-dotted">Home</a>
							<a href="#" class="font-semibold rounded-lg py-1 px-2 text-dark hover:text-primary hover:scale-125 active:outline-dotted">Home</a>
							<a href="#" class="font-semibold rounded-lg py-1 px-2 text-dark hover:text-primary hover:scale-125 active:outline-dotted">Home</a>
							<a href="#" class="font-semibold rounded-lg py-1 px-2 text-dark hover:text-primary hover:scale-125 active:outline-dotted">Home</a>
							<a href="#" class="font-semibold rounded-lg py-1 px-2 text-dark hover:text-primary hover:scale-125 active:outline-dotted">Home</a>
						</div>
						 --}}
						 
						<div class="flex flex-nowrap items-center space-x-4">
							@auth
								<a href="{{ route('student.my-courses.index') }}" class="mx-4 py-2 px-4 ease-in-out font-medium bg-sky-400 text-white rounded-lg transition hover:shadow-xl active:opacity-70 active:ring active:ring-primary">
									Dashboard
								</a>
								
								<button type="button" x-data="{trigger: false}" id="avatar" name="avatar" class="flex items-center justify-center relative w-8 h-8 rounded-full active:ring active:ring-slate-400">
									<img x-on:click="trigger = !trigger" src="{{ url('https://randomuser.me/api/portraits/men/62.jpg') }}" alt="User Profile" class="w-8 h-8 rounded-full">
									<span class="absolute right-0 bottom-0 w-[10px] h-[10px] rounded-full bg-green-500 outline outline-2 outline-slate-600 opacity-90 pointer-events-none"></span>
									
									<div x-show="trigger" id="avatar-menu" name="avatar_menu" class="absolute right-0 top-full mt-3 px-3 w-40 rounded-lg bg-white shadow-md divide-y cursor-default md:w-56 md:px-6">
										<div class="block py-2">
											<h4 class="font-semibold text-md text-left text-dark truncate">Mohammed Farrell Eghar Syahputra</h4>
											<p class="font-medium text-xs text-left text-secondary truncate">farrell.eghar@serpihan.id</p>
										</div>
										
										<div class="block space-y-1 text-left py-2">
											<a href="#" class="flex items-center space-x-2 text-secondary hover:text-primary">
												<span class="material-symbols-outlined text-sm">
													person
												</span>
												
												<span class="font-medium text-sm">
													My Profile
												</span>
											</a>
											
											<a href="#" class="flex items-center space-x-2 text-secondary hover:text-primary">
												<span class="material-symbols-outlined text-sm">
													settings
												</span>
												
												<span class="font-medium text-sm">
													Settings
												</span>
											</a>
											
											<a href="{{ route('logout') }}" class="flex items-center space-x-2 text-secondary hover:text-primary">
												<span class="material-symbols-outlined text-sm">
													logout
												</span>
												
												<span class="font-medium text-sm">
													Logout
												</span>
											</a>
										</div>
									</div>
								</button>
							@endauth
							
							@guest
								<a href="{{ route('login') }}" class="py-2 px-4 ease-in-out font-medium bg-sky-400 text-white rounded-lg transition hover:shadow-xl active:opacity-70 active:ring active:ring-primary">
									Sign In
								</a>
								
								<a href="{{ route('register') }}" class="py-2 px-4 ease-in-out font-medium bg-teal-400 text-white rounded-lg transition hover:shadow-xl active:opacity-70 active:ring active:bg-teal-400">
									Sign Up
								</a>
							@endguest
						</div>
					</nav>
					
					<div class="flex items-center z-20 lg:hidden">
						<button type="button" id="hamburger" name="hamburger" class="block z-30">
							<span class="hamburger-line transition duration-300 ease-in-out origin-top-left"></span>
							<span class="hamburger-line transition duration-300 ease-in-out"></span>
							<span class="hamburger-line transition duration-300 ease-in-out origin-bottom-left"></span>
						</button>
						
						<nav id="nav-sm" class="fixed hidden top-0 right-0 h-full w-full bg-transparent backdrop-blur-sm animate-fadeIn">
							<div class="float-right h-full w-3/4 animate-slideIn sm:w-1/2">
								<div class="bg-white px-4">
									<a href="#" class="flex shrink-0 space-x-3 items-center">
										<img src="{{ asset('assets/logo.png') }}" alt="Logo" class="h-12 w-12">
										<div class="font-logo items-center">
											<p class="font-bold text-primary text-lg leading-tight">Serpihan</p>
											<p class="font-medium text-base leading-tight text-secondary">Digital School</p>
										</div>
									</a>
								</div>
								
								<div class="py-1 px-4 h-full divide-y bg-gradient-to-br from-teal-500 via-primary to-sky-500">
									{{-- 
									<div class="font-semibold text-white max-w-full">
										<a href="#" class="block py-1 px-3 my-3 rounded-full items-center relative hover:text-primary hover:bg-white active:ring active:ring-cyan-200">
											<span class="material-symbols-outlined absolute top-1/2 -translate-y-1/2">
												home
											</span>
											<span class="px-10">Part 1</span>
										</a>
										
										<a href="#" class="block py-1 px-3 my-3 rounded-full items-center relative hover:text-primary hover:bg-white active:ring active:ring-cyan-200">
											<span class="material-symbols-outlined absolute top-1/2 -translate-y-1/2">
												home
											</span>
											<span class="px-10">Part 2</span>
										</a>
										
										<a href="#" class="block py-1 px-3 my-3 rounded-full items-center relative hover:text-primary hover:bg-white active:ring active:ring-cyan-200">
											<span class="material-symbols-outlined absolute top-1/2 -translate-y-1/2">
												home
											</span>
											<span class="px-10">Part 3</span>
										</a>
										
										<a href="#" class="block py-1 px-3 my-3 rounded-full items-center relative hover:text-primary hover:bg-white active:ring active:ring-cyan-200">
											<span class="material-symbols-outlined absolute top-1/2 -translate-y-1/2">
												home
											</span>
											<span class="px-10">Part 4</span>
										</a>
										
										<a href="#" class="block py-1 px-3 my-3 rounded-full items-center relative hover:text-primary hover:bg-white active:ring active:ring-cyan-200">
											<span class="material-symbols-outlined absolute top-1/2 -translate-y-1/2">
												home
											</span>
											<span class="px-10">Part 5</span>
										</a>
									</div>
									--}}
									
									<div class="py-3 font-semibold text-white max-w-full space-y-4">
										<a href="{{ route('login') }}" class="block py-1 px-3 rounded-full items-center relative hover:text-primary hover:bg-white active:ring active:ring-cyan-200">
											<span class="material-symbols-outlined absolute top-1/2 -translate-y-1/2">
												login
											</span>
											<p class="text-center">Login</p>
										</a>
										
										<a href="{{ route('register') }}" class="block py-1 px-3 rounded-full items-center relative hover:text-primary hover:bg-white active:ring active:ring-cyan-200">
											<span class="material-symbols-outlined absolute top-1/2 -translate-y-1/2">
												login
											</span>
											<p class="text-center">Register</p>
										</a>
									</div>
								</div>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</header>
		{{-- Header End --}}
		
        {{-- Hero Section Start --}}
		<section id="home" class="relative pt-24 lg:pt-32 lg:pb-24">
			<div class="container">
				<div class="flex flex-wrap">
					<div class="w-full px-4 mb-10 lg:w-1/2">
						<div class="relative items-center mb-16 lg:w-9/12 lg:pr-5">
							<label for="search" class="material-symbols-outlined absolute top-1/2 -translate-y-1/2 p-2 text-slate-600">
								search
							</label>
							<input type="text" id="search" autocomplete="off" class="w-full bg-slate-200 text-dark px-10 py-3 border-slate-400 rounded-lg  focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:bg-slate-100" placeholder="Search Subject...">
						</div>
						
						<div class="w-full lg:pr-5">
							<h4 class="flex flex-nowrap font-medium text-xs text-slate-600 mb-1 lg:text-sm">
								<span class="pr-3">#ElementarySchool</span>
								<span class="pr-3">#HighSchool</span>
								<span class="pr-3">#Speciality</span>
							</h4>
							
							<h2 class="font-semibold tracking-wide leading-tight text-5xl text-dark capitalize mb-4 lg:text-6xl">Lorem ipsum dolor sit amet.</h2>
							<p class="text-base text-secondary text-justify mb-10 lg:w-3/4 md:text-lg">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto expedita aliquam voluptatibus itaque nemo rem aliquid eius magni ipsum?</p>
							<a href="#" class="py-4 px-8 ease-in-out font-semibold bg-gradient-to-r from-sky-400 via-primary to-teal-400 text-white rounded-lg transition hover:shadow-xl hover:bg-gradient-to-l active:opacity-70 active:ring active:ring-primary">Get Started</a>
						</div>
					</div>
					
					<div class="w-full px-4 mb-10 lg:w-1/2">
						<img src="{{ asset('assets/landing-main.png') }}" alt="Freepik Illustration" class="mx-auto">
					</div>
				</div>
			</div>
			
			<div class="lg:container lg:px-8 lg:absolute lg:left-1/2 lg:-translate-x-1/2 lg:-bottom-16">
				<div class="grid grid-cols-2 justify-items-center justify-center bg-slate-100 gap-y-8 py-8 lg:grid-cols-4 lg:rounded-2xl lg:shadow-md">
					<div class="text-center text-dark">
						<h2 class="font-semibold text-4xl leading-tight">99+</h2>
						<h4 class="font-medium text-sm">Courses</h4>
					</div>
					<div class="text-center text-dark">
						<h2 class="font-semibold text-4xl leading-tight">99+</h2>
						<h4 class="font-medium text-sm">Mentors</h4>
					</div>
					<div class="text-center text-dark">
						<h2 class="font-semibold text-4xl leading-tight">99+</h2>
						<h4 class="font-medium text-sm">Students</h4>
					</div>
					<div class="text-center text-dark">
						<h2 class="font-semibold text-4xl leading-tight">4.9/5.0</h2>
						<h4 class="font-medium text-sm">99+ reviews</h4>
					</div>
				</div>
			</div>
		</section>
		{{-- Hero Section End --}}
		
		{{-- Course Section Start --}}
		<section id="course" class="bg-sky-700 pb-24 pt-24 lg:pt-32">
			<div class="container">
				<div class="mx-auto text-center max-w-xl mb-12">
					<h2 class="font-semibold text-lg text-primary mb-2">Courses</h2>
					<h4 class="font-bold text-white text-3xl mb-5 sm:4xl lg:5xl">Most Valuable Courses</h4>
					<p class="font-medium text-base text-slate-400 md:text-lg">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Veniam repellat asperiores adipisci? Pariatur eum quasi perspiciatis. Voluptatum aut quo beatae error neque!</p>
				</div>
				
				<div class="flex flex-wrap justify-center xl:space-x-8 2xl:space-x-12">
					<a href="#" class="w-full px-4 lg:w-1/2 xl:w-1/3">
						<div class="bg-white rounded-xl shadow-lg overflow-hidden mb-10">
							<div class="max-h-min overflow-hidden">
								<img src="https://source.unsplash.com/360x200?programming" alt="Programming" class="w-full transition duration-300 hover:scale-125">
							</div>
							
							<div class="py-8 px-6">
								<h3 class="block mb-3 font-semibold text-xl text-slate-900 hover:text-primary truncate">Programming Starter Kit</h3> 
								<p class="font-medium text-base text-secondary mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur nostrum eaque blanditiis minus?</p>
								
								<div class="group flex relative rounded-lg max-w-max bg-gradient-to-br from-emerald-400 to-cyan-400">
									<p class="absolute w-full -z-10 text-right pr-8 top-1/2 -translate-y-1/2 underline underline-offset-4 font-semibold text-base text-white group-hover:z-0">Course Details</p>
									
									<span class="material-symbols-outlined absolute w-full -z-10 pr-2 top-1/2 -translate-y-1/2 text-right text-xl text-white group-hover:z-0">
										next_plan
									</span>
									
									<div class="flex items-center space-x-4 bg-primary py-2 px-4 rounded-lg max-w-max origin-top-left transition duration-300 group-hover:-rotate-[40deg]">
										<img src="{{ url('https://randomuser.me/api/portraits/men/81.jpg') }}" alt="Random User" class="rounded-full shrink-0 w-10 h-10">
										
										<div class="text-white">
											<p class="font-semibold text-base">Coach Bened</p>
											<p class="font-medium text-sm">Software Engineer</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</a>
					
					<a href="#" class="w-full px-4 lg:w-1/2 xl:w-1/3">
						<div class="bg-white rounded-xl shadow-lg overflow-hidden mb-10">
							<div class="max-h-min overflow-hidden">
								<img src="https://source.unsplash.com/360x200?calculus" alt="Mechanical Keyboard" class="w-full transition duration-300 hover:scale-125">
							</div>
							
							<div class="py-8 px-6">
								<h3 class="block mb-3 font-semibold text-xl text-slate-900 hover:text-primary truncate">Calculus Shorthand</h3>
								<p class="font-medium text-base text-secondary mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur nostrum eaque blanditiis minus?</p>
								
								<div class="group flex relative rounded-lg max-w-max bg-gradient-to-br from-emerald-400 to-cyan-400">
									<p class="absolute w-full text-right -z-10 pr-8 top-1/2 -translate-y-1/2 underline underline-offset-4 font-semibold text-base text-white group-hover:z-0">Course Details</p>
									
									<span class="material-symbols-outlined absolute w-full -z-10 pr-2 top-1/2 -translate-y-1/2 text-right text-xl text-white group-hover:z-0">
										next_plan
									</span>
									
									<div class="flex items-center space-x-4 bg-primary py-2 px-4 rounded-lg max-w-max origin-top-left transition duration-300 group-hover:-rotate-[40deg]">
										<img src="{{ url('https://randomuser.me/api/portraits/men/81.jpg') }}" alt="Random User" class="rounded-full shrink-0 w-10 h-10">
										
										<div class="text-white">
											<p class="font-semibold text-base">Coach Bened</p>
											<p class="font-medium text-sm">Software Engineer</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</a>
					
					<a href="#" class="w-full px-4 lg:w-1/2 xl:w-1/3">
						<div class="bg-white rounded-xl shadow-lg overflow-hidden mb-10">
							<div class="max-h-min overflow-hidden">
								<img src="https://source.unsplash.com/360x200?coffee" alt="Coffee" class="w-full transition duration-300 hover:scale-125">
							</div>
							
							<div class="py-8 px-6">
								<h3 class="block mb-3 font-semibold text-xl text-slate-900 hover:text-primary truncate">Mastering Job Interview</h3> 
								<p class="font-medium text-base text-secondary mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur nostrum eaque blanditiis minus?</p>
								
								<div class="group flex relative rounded-lg max-w-max bg-gradient-to-br from-emerald-400 to-cyan-400">
									<p class="absolute w-full text-right -z-10 pr-8 top-1/2 -translate-y-1/2 underline underline-offset-4 font-semibold text-base text-white group-hover:z-0">Course Details</p>
									
									<span class="material-symbols-outlined absolute w-full -z-10 pr-2 top-1/2 -translate-y-1/2 text-right text-xl text-white group-hover:z-0">
										next_plan
									</span>
									
									<div class="flex items-center space-x-4 bg-primary py-2 px-4 rounded-lg max-w-max origin-top-left transition duration-300 group-hover:-rotate-[40deg]">
										<img src="{{ url('https://randomuser.me/api/portraits/men/81.jpg') }}" alt="Random User" class="rounded-full shrink-0 w-10 h-10">
										
										<div class="text-white">
											<p class="font-semibold text-base">Coach Bened</p>
											<p class="font-medium text-sm">Software Engineer</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</section>
		{{-- Courses Section End --}}
		
		{{-- Quotes Section Start --}}
		<section id="quotes" class="py-20 lg:py-28">
			<div class="container">
				<div class="text-center max-w-3xl space-y-10 mx-auto">
					<h2 class="hidden font-semibold text-3xl text-dark italic lg:block">"The great secret of education is to use exercise of mind and body as relaxation one to the other."</h2>
					<h2 class="font-semibold text-2xl text-dark italic lg:hidden">"Plants are shaped by cultivation, and men by education."</h2>
					<p class="font-medium text-md text-secondary italic lg:text-lg">~ Jean-Jacques Rousseau ~</p>
					{{-- What wisdom can you find greater than kindness. --}}
				</div>
			</div>
		</section>
		{{-- Quotes Section End --}}
		
		{{-- Feedback Section Start --}}
		<section id="feedback" class="py-28 px-4 mx-auto overflow-hidden">
			<div class="container">
				<div class="text-center space-y-3">
					<h2 class="font-semibold text-md text-primary md:text-lg">Testimonial</h2>
					<h4 class="font-bold text-3xl lg:text-4xl">What people say about us</h4>
				</div>
				
				<div class="w-full py-16">
					<div class="feedback animate-feedbackSlides flex flex-nowrap items-center justify-evenly space-x-6">
						<div class="block w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 sm:w-1/2 xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
						
						<div class="hidden w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 sm:block sm:w-1/2 xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
						
						<div class="hidden w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 xl:block xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
					</div>
					
					<div class="feedback animate-feedbackSlides flex flex-nowrap items-center justify-evenly space-x-6">
						<div class="block w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 sm:w-1/2 xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
						
						<div class="hidden w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 sm:block sm:w-1/2 xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
						
						<div class="hidden w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 xl:block xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
					</div>
					
					<div class="feedback animate-feedbackSlides flex flex-nowrap items-center justify-evenly space-x-6">
						<div class="block w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 sm:w-1/2 xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
						
						<div class="hidden w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 sm:block sm:w-1/2 xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
						
						<div class="hidden w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 xl:block xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
					</div>
					
					<div class="feedback animate-feedbackSlides flex flex-nowrap items-center justify-evenly space-x-6">
						<div class="block w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 sm:w-1/2 xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
						
						<div class="hidden w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 sm:block sm:w-1/2 xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
						
						<div class="hidden w-full border self-center shadow-lg rounded-lg overflow-hidden space-y-3 xl:block xl:w-1/3">
							<div class="shrink-0 self-center">
								<img src="{{ url('https://source.unsplash.com/280x80?pattern') }}" alt="Pattern" class="w-full object-cover">
							</div>
							
							<div class="relative items-center text-center p-8">
								<img src="{{ url('https://randomuser.me/api/portraits/men/32.jpg') }}" alt="" class="w-16 h-16 absolute -top-12 left-1/2 -translate-x-1/2 rounded-full">
								<h3 class="text-lg font-semibold text-dark">Johnson Bellemond</h3>
								<h4 class="text-sm font-semibold text-primary">Backend Engineer</h4>
								<p class="text-md font-medium text-secondary py-8"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae non nulla numquam, neque porro amet explicabo eum tenetur at! Ab illo ex quia iusto id aliquid itaque, vel saepe nostrum!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{{-- Feedback Section End --}}
        
		{{-- Footer Section Start --}}
		<footer id="footer" class="bg-dark pb-12 pt-24">
			<div class="container">
				<div class="flex flex-wrap">
					<div class="w-full px-4 mb-12 text-slate-300 font-medium md:w-1/3">
						<a href="#" class="flex shrink-0 space-x-3 items-center pb-6">
							<img src="{{ asset('assets/logo.png') }}" alt="Logo" class="h-12 w-12">
							<div class="font-logo items-center">
								<p class="font-bold text-primary text-lg leading-tight">Serpihan</p>
								<p class="font-medium text-base leading-tight text-secondary">Digital School</p>
							</div>
						</a>
						<p>Sidoarjo, East Java, 61218</p>
						<p>+62 895-1453-7876</p>
					</div>
					
					<div class="w-full px-4 mb-12 md:w-1/3">
						<h3 class="font-semibold text-xl text-white mb-5">Course Categories</h3>
						<ul class="text-slate-300">
							<li>
								<a href="#" class="inline-block text-base mb-3 hover:text-primary">Mathematics</a>
							</li>
							
							<li>
								<a href="#" class="inline-block text-base mb-3 hover:text-primary">Digital Marketing</a>
							</li>
							
							<li>
								<a href="#" class="inline-block text-base mb-3 hover:text-primary">Religion</a>
							</li>
						</ul>
					</div>
					
					<div class="w-full px-4 mb-12 md:w-1/3">
						<h3 class="font-semibold text-xl text-white mb-5">Credits</h3>
						<ul class="text-slate-300">
							<li>
								<a href="{{ url('https://canva.com') }}" class="inline-block text-base mb-3 hover:text-primary">FlyBets Logo</a>
							</li>
							
							<li>
								<a href="{{ url('https://source.unsplash.com') }}" class="inline-block text-base mb-3 hover:text-primary">Image Source</a>
							</li>
							
							<li>
								<a href="{{ url('https://pngtree.com/') }}" class="inline-block text-base mb-3 hover:text-primary">Illustrations</a>
							</li>
							
							<li>
								<a href="{{('https://dribbble.com/shots/18284158-Courses')}}" class="inline-block text-base mb-3 hover:text-primary">UI Reference</a>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="w-full pt-10 border-t border-slate-700">
					<div class="flex items-center justify-center mb-5">
						<!-- Instagram -->
						<a href="#" target="_blank" class="w-9 h-9 mr-3 rounded-full flex justify-center items-center border border-slate-300 text-slate-400 hover:border-primary hover:bg-primary hover:text-white">
							<svg role="img" width="20" class="fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Instagram</title><path d="M12 0C8.74 0 8.333.015 7.053.072 5.775.132 4.905.333 4.14.63c-.789.306-1.459.717-2.126 1.384S.935 3.35.63 4.14C.333 4.905.131 5.775.072 7.053.012 8.333 0 8.74 0 12s.015 3.667.072 4.947c.06 1.277.261 2.148.558 2.913.306.788.717 1.459 1.384 2.126.667.666 1.336 1.079 2.126 1.384.766.296 1.636.499 2.913.558C8.333 23.988 8.74 24 12 24s3.667-.015 4.947-.072c1.277-.06 2.148-.262 2.913-.558.788-.306 1.459-.718 2.126-1.384.666-.667 1.079-1.335 1.384-2.126.296-.765.499-1.636.558-2.913.06-1.28.072-1.687.072-4.947s-.015-3.667-.072-4.947c-.06-1.277-.262-2.149-.558-2.913-.306-.789-.718-1.459-1.384-2.126C21.319 1.347 20.651.935 19.86.63c-.765-.297-1.636-.499-2.913-.558C15.667.012 15.26 0 12 0zm0 2.16c3.203 0 3.585.016 4.85.071 1.17.055 1.805.249 2.227.415.562.217.96.477 1.382.896.419.42.679.819.896 1.381.164.422.36 1.057.413 2.227.057 1.266.07 1.646.07 4.85s-.015 3.585-.074 4.85c-.061 1.17-.256 1.805-.421 2.227-.224.562-.479.96-.899 1.382-.419.419-.824.679-1.38.896-.42.164-1.065.36-2.235.413-1.274.057-1.649.07-4.859.07-3.211 0-3.586-.015-4.859-.074-1.171-.061-1.816-.256-2.236-.421-.569-.224-.96-.479-1.379-.899-.421-.419-.69-.824-.9-1.38-.165-.42-.359-1.065-.42-2.235-.045-1.26-.061-1.649-.061-4.844 0-3.196.016-3.586.061-4.861.061-1.17.255-1.814.42-2.234.21-.57.479-.96.9-1.381.419-.419.81-.689 1.379-.898.42-.166 1.051-.361 2.221-.421 1.275-.045 1.65-.06 4.859-.06l.045.03zm0 3.678c-3.405 0-6.162 2.76-6.162 6.162 0 3.405 2.76 6.162 6.162 6.162 3.405 0 6.162-2.76 6.162-6.162 0-3.405-2.76-6.162-6.162-6.162zM12 16c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm7.846-10.405c0 .795-.646 1.44-1.44 1.44-.795 0-1.44-.646-1.44-1.44 0-.794.646-1.439 1.44-1.439.793-.001 1.44.645 1.44 1.439z"/></svg>
						</a>
						
						<!-- Telegram -->
						<a href="#" target="_blank" class="w-9 h-9 mr-3 rounded-full flex justify-center items-center border border-slate-300 text-slate-400 hover:border-primary hover:bg-primary hover:text-white">
							<svg role="img" width="20" class="fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Telegram</title><path d="M11.944 0A12 12 0 0 0 0 12a12 12 0 0 0 12 12 12 12 0 0 0 12-12A12 12 0 0 0 12 0a12 12 0 0 0-.056 0zm4.962 7.224c.1-.002.321.023.465.14a.506.506 0 0 1 .171.325c.016.093.036.306.02.472-.18 1.898-.962 6.502-1.36 8.627-.168.9-.499 1.201-.82 1.23-.696.065-1.225-.46-1.9-.902-1.056-.693-1.653-1.124-2.678-1.8-1.185-.78-.417-1.21.258-1.91.177-.184 3.247-2.977 3.307-3.23.007-.032.014-.15-.056-.212s-.174-.041-.249-.024c-.106.024-1.793 1.14-5.061 3.345-.48.33-.913.49-1.302.48-.428-.008-1.252-.241-1.865-.44-.752-.245-1.349-.374-1.297-.789.027-.216.325-.437.893-.663 3.498-1.524 5.83-2.529 6.998-3.014 3.332-1.386 4.025-1.627 4.476-1.635z"/></svg>
						</a>
						
						<!-- LinkedIn -->
						<a href="#" target="_blank" class="w-9 h-9 mr-3 rounded-full flex justify-center items-center border border-slate-300 text-slate-400 hover:border-primary hover:bg-primary hover:text-white">
							<svg role="img" width="20" class="fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>LinkedIn</title><path d="M20.447 20.452h-3.554v-5.569c0-1.328-.027-3.037-1.852-3.037-1.853 0-2.136 1.445-2.136 2.939v5.667H9.351V9h3.414v1.561h.046c.477-.9 1.637-1.85 3.37-1.85 3.601 0 4.267 2.37 4.267 5.455v6.286zM5.337 7.433c-1.144 0-2.063-.926-2.063-2.065 0-1.138.92-2.063 2.063-2.063 1.14 0 2.064.925 2.064 2.063 0 1.139-.925 2.065-2.064 2.065zm1.782 13.019H3.555V9h3.564v11.452zM22.225 0H1.771C.792 0 0 .774 0 1.729v20.542C0 23.227.792 24 1.771 24h20.451C23.2 24 24 23.227 24 22.271V1.729C24 .774 23.2 0 22.222 0h.003z"/></svg>
						</a>
						
						<!-- Facebook -->
						<a href="#" target="_blank" class="w-9 h-9 mr-3 rounded-full flex justify-center items-center border border-slate-300 text-slate-400 hover:border-primary hover:bg-primary hover:text-white">
							<svg role="img" width="20" class="fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Facebook</title><path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"/></svg>
						</a>
						
						<!-- Line -->
						<a href="#" target="_blank" class="w-9 h-9 mr-3 rounded-full flex justify-center items-center border border-slate-300 text-slate-400 hover:border-primary hover:bg-primary hover:text-white">
							<svg role="img" width="20" class="fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>LINE</title><path d="M19.365 9.863c.349 0 .63.285.63.631 0 .345-.281.63-.63.63H17.61v1.125h1.755c.349 0 .63.283.63.63 0 .344-.281.629-.63.629h-2.386c-.345 0-.627-.285-.627-.629V8.108c0-.345.282-.63.63-.63h2.386c.346 0 .627.285.627.63 0 .349-.281.63-.63.63H17.61v1.125h1.755zm-3.855 3.016c0 .27-.174.51-.432.596-.064.021-.133.031-.199.031-.211 0-.391-.09-.51-.25l-2.443-3.317v2.94c0 .344-.279.629-.631.629-.346 0-.626-.285-.626-.629V8.108c0-.27.173-.51.43-.595.06-.023.136-.033.194-.033.195 0 .375.104.495.254l2.462 3.33V8.108c0-.345.282-.63.63-.63.345 0 .63.285.63.63v4.771zm-5.741 0c0 .344-.282.629-.631.629-.345 0-.627-.285-.627-.629V8.108c0-.345.282-.63.63-.63.346 0 .628.285.628.63v4.771zm-2.466.629H4.917c-.345 0-.63-.285-.63-.629V8.108c0-.345.285-.63.63-.63.348 0 .63.285.63.63v4.141h1.756c.348 0 .629.283.629.63 0 .344-.282.629-.629.629M24 10.314C24 4.943 18.615.572 12 .572S0 4.943 0 10.314c0 4.811 4.27 8.842 10.035 9.608.391.082.923.258 1.058.59.12.301.079.766.038 1.08l-.164 1.02c-.045.301-.24 1.186 1.049.645 1.291-.539 6.916-4.078 9.436-6.975C23.176 14.393 24 12.458 24 10.314"/></svg>
						</a>
					</div>
					
					<p class="font-medium text-sm text-slate-500 text-center">Made with
						<span class="text-pink-500"> ❤ </span>by
						<a href="#" target="_blank" class="font-bold text-primary"> Farrell Eghar</a>, using
						<a href="https://tailwindcss.com" target="_blank" class="font-bold bg-clip-text bg-gradient-to-r from-blue-500 to-teal-400 text-transparent"> TailwindCSS </a>
					</p>
				</div>
			</div>
		</footer>
		{{-- Footer Section End --}}
		
		@livewireScripts
		@stack('scripts')
		<script src="{{ asset('js/landing/script.js') }}"></script>
    </body>
</html>
