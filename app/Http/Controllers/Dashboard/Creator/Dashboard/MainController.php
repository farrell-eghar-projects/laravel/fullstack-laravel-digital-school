<?php

namespace App\Http\Controllers\Dashboard\Creator\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
	{
		return view('pages.dashboard.creator.dashboard.index');
	}
}
