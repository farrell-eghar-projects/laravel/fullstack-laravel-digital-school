<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\Student\AllCourses\MainController as AllCoursesMain;
use App\Http\Controllers\Dashboard\Student\MyCourses\MainController as MyCoursesMain;
use App\Http\Controllers\Dashboard\Student\MyCourses\CourseController as StdCourseController;
use App\Http\Controllers\Dashboard\Student\MyCourses\MaterialController as StdMaterialController;
use App\Http\Controllers\Dashboard\Student\MyCourses\AssignmentController as StdAssignmentController;
use App\Http\Controllers\Dashboard\Creator\Dashboard\MainController as CreatorDashboardMain;
use App\Http\Controllers\Dashboard\Creator\ManageCourses\MainController as ManageCoursesMain;
use App\Http\Controllers\Dashboard\Creator\ManageCourses\CourseController as CrtCourseController;
use App\Http\Controllers\Dashboard\Creator\ManageCourses\MaterialController as CrtMaterialController;
use App\Http\Controllers\Dashboard\Creator\ManageCourses\AssignmentController;
use App\Http\Controllers\Dashboard\Creator\ManageCourses\SubmissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('layouts.master-landing');
});

Route::get('/me', function () {
  return view('layouts.master-dashboard');
});

Route::group([
  'as'  => 'student.',
  'prefix' => 'student'
], function () {
  Route::get('/dashboard', function () {
    return view('layouts.master-dashboard');
  })->name('dashboard.index');

  Route::get('/my-courses', [MyCoursesMain::class, 'index'])->name('my-courses.index');
  Route::get('/my-courses/{course}', [StdCourseController::class, 'show'])->name('my-courses.course.show');
  Route::get('/my-courses/{course}/material/{session}', [StdMaterialController::class, 'show'])->name('my-courses.course.material.show');
  Route::get('/my-courses/{course}/assignment/{assignment}', [StdAssignmentController::class, 'show'])->name('my-courses.course.assignment.show');

  Route::get('/all-courses', [AllCoursesMain::class, 'index'])->name('all-courses.index');
  Route::get('/all-courses/{course}', [AllCoursesMain::class, 'show'])->name('all-courses.show');
});

Route::group([
  'as'  => 'creator.',
  'prefix' => 'creator'
], function () {
  Route::get('/dashboard', [CreatorDashboardMain::class, 'index'])->name('dashboard.index');

  Route::get('/manage-courses', [ManageCoursesMain::class, 'index'])->name('manage-courses.index');

  Route::post('/manage-courses/course', [CrtCourseController::class, 'store'])->name('manage-courses.course.store');
  Route::get('/manage-courses/course/{course}', [CrtCourseController::class, 'show'])->name('manage-courses.course.show');
  Route::put('/manage-courses/course/{course}', [CrtCourseController::class, 'update'])->name('manage-courses.course.update');
  Route::delete('manage-courses/course/{course}', [CrtCourseController::class, 'destroy'])->name('manage-courses.course.destroy');
  Route::get('/manage-courses/course/{course}/create', [CrtCourseController::class, 'create'])->name('manage-courses.course.create');

  Route::get('/manage-courses/course/{course}/{assignment}', [AssignmentController::class, 'show'])->name('manage-courses.course.assignment.show');
  Route::get('/manage-courses/course/{course}/{assignment}/{assignmentSubmission}', [SubmissionController::class, 'show'])->name('manage-courses.course.assignment.submission.show');

  Route::get('/manage-courses/material/{material}', [CrtMaterialController::class, 'show'])->name('manage-courses.material.show');

  Route::get('/revenue', function () {
    return view('layouts.master-dashboard');
  })->name('revenue.index');
});

Route::group([
  'as'  => 'admin.',
  'prefix' => 'admin'
], function () {
  Route::get('/user-control', function () {
    return view('layouts.master-dashboard');
  })->name('user-control.index');
  Route::get('/transaction-history', function () {
    return view('layouts.master-dashboard');
  })->name('transaction-history.index');
});

Route::middleware([
  'auth:sanctum',
  config('jetstream.auth_session'),
  'verified'
])->group(function () {
  Route::get('/dashboard', function () {
    return view('dashboard');
  })->name('dashboard');
});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web']], function () {
  \UniSharp\LaravelFilemanager\Lfm::routes();
});
