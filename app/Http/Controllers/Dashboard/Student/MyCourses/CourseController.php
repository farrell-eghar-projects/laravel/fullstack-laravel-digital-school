<?php

namespace App\Http\Controllers\Dashboard\Student\MyCourses;

use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CourseController extends Controller
{
  public function show(Course $course)
  {
    $durations = array();

    foreach ($course->courseAssignments as $index => $courseAssignment) {
      $min = $courseAssignment->duration % 60;
      if ($min < 10) {
        $min = 0 . $min;
      }

      $time = (int) ($courseAssignment->duration / 60) . ':' . $min;
      array_push($durations, Carbon::createFromFormat('H:i', $time)->format('H:i:s'));
    }

    return view('pages.dashboard.student.my-courses.course.show', compact('course', 'durations'));
  }
}
