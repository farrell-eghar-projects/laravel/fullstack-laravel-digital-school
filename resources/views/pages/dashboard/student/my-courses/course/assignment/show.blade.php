@extends('layouts.master-dashboard')

@section('title', 'Assignment Start')

@section('content')
	@push('css')
		<link rel="stylesheet" type="text/css" href="{{ asset('css/prism.css') }}">
	@endpush
	
	{{-- Breadcrumb Start --}}
	@livewire('dashboard.components.breadcrumb', ['routes' => [
			[
				'link' => route('student.my-courses.index'),
				'name' => 'My Courses'
			],
			[
				'link' => route('student.my-courses.course.show', ['course' => $course->id]),
				'name' => 'Course Details'
			],
			[
				'link' => route('student.my-courses.course.assignment.show', ['course' => $course->id, 'assignment' => $assignment->id]),
				'name' => 'Assignment Details'
			]
		]
	])
	{{-- Breadcrumb End --}}
	
	<div class="lg:px-32">
		<div class="container">
			@livewire('dashboard.student.my-course.assignment-submission', ['course' => $course, 'assignment' => $assignment, 'courseAssignment' => $courseAssignment, 'assignmentSubmission' => $assignmentSubmission])
		</div>
	</div>
	
	@push('scripts')
		<script src="{{ asset('js/prism.js') }}"></script>
		<script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
	@endpush
@endsection