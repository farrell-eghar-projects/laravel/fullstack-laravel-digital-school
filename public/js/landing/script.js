// Landing Page Fixed Navbar & Hamburger
const header = document.querySelector('header')
const fixedNav = header.offsetTop

window.onscroll = function() {
	if (window.scrollY > fixedNav) {
		header.classList.add('navbar-fixed')
	} else {
		header.classList.remove('navbar-fixed')
	}
}

const hamburger = document.querySelector('#hamburger')
const navMenu = document.querySelector('#nav-sm')

window.onclick = function(event) {
	if (event.target == navMenu) {
		hamburger.classList.toggle('hamburger-active')
		header.classList.toggle('backdrop-blur-sm')
		navMenu.classList.add('hidden')
	}
}

hamburger.addEventListener('click', function () {
	hamburger.classList.toggle('hamburger-active')
	header.classList.toggle('backdrop-blur-sm')
	navMenu.classList.toggle('hidden')
})

// Landing Page Feedback Slides
const feedback = document.querySelectorAll('.feedback')
let slideIndex = 0
feedbackSlides()

function feedbackSlides() {
	for (let i = 0; i < feedback.length; i++) {
		feedback[i].classList.add('hidden')
	}
	
	slideIndex++
	if (slideIndex > feedback.length) { slideIndex = 1 }
	feedback[slideIndex - 1].classList.remove('hidden')
	setTimeout(feedbackSlides, 5000)
}