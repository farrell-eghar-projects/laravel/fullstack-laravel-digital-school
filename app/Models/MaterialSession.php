<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaterialSession extends Model
{
    use HasFactory;
	
	protected $guarded = [];
	
	public function materialSection()
	{
		return $this->belongsTo(MaterialSection::class);
	}
}
