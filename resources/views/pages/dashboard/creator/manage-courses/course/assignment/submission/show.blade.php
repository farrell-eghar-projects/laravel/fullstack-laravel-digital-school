@extends('layouts.master-dashboard')

@section('title', 'Submission Details')

@section('content')
	{{-- Breadcrumb Start --}}
	@livewire('dashboard.components.breadcrumb', ['routes' => [
			[
				'link' => route('creator.manage-courses.index'),
				'name' => 'Manage Courses'
			],
			[
				'link' => route('creator.manage-courses.course.show', ['course' => $course->id]),
				'name' => 'Course Details'
			],
			[
				'link' => route('creator.manage-courses.course.assignment.show', ['course' => $course->id, 'assignment' => $assignment->id]),
				'name' => 'Assignment Submissions'
			],
			[
				'link' => route('creator.manage-courses.course.assignment.submission.show', ['course' => $course->id, 'assignment' => $assignment->id, 'assignmentSubmission' => $assignmentSubmission->id]),
				'name' => 'Submission Details'
			]
		]
	])
	{{-- Breadcrumb End --}}
	
	<div class="lg:px-32">
		<div class="container">
			<div class="block bg-white my-2 px-8 py-4 rounded-md shadow-lg">
				{{-- Header Page Start --}}
				<div class="block">
					<h3 class="font-semibold text-xl text-dark text-center p-4 max-w-xl mx-auto lg:text-2xl">Submission Details</h3>
					
					<div class="block space-y-4 mt-8 sm:space-y-6">
						<div class="flex flex-nowrap items-center space-x-2 lg:space-x-4">
							<div class="rounded-full overflow-hidden shrink-0">
								<img src="{{ url('https://randomuser.me/api/portraits/men/33.jpg') }}" alt="Student Profile" class="w-10 h-10 rounded-full overflow-hidden object-cover">
							</div>
							
							<div class="block">
								<h4 class="font-medium text-dark">{{ $assignmentSubmission->user->name }}</h4>
								<h5 class="text-secondary text-sm">{{ $assignmentSubmission->user->email }}</h5>
							</div>
						</div>
						
						<button type="button" class="flex flex-nowrap items-center justify-center float-right w-40 my-2 p-1 space-x-2 bg-slate-200 rounded-md transition duration-300 hover:ring hover:ring-slate-200/70 hover:shadow-md">
							<span class="material-symbols-outlined text-secondary">
								task
							</span>
							
							<span class="font-medium text-sm text-secondary">Auto-score</span>
						</button>
						
						<div class="flex flex-nowrap space-x-2">
							<h4 class="font-medium text-sm text-dark">Total Score</h4>
							<span class="flex flex-nowrap space-x-1">
								<h5 class="text-sm text-secondary">:</h5>
								<h5 class="text-sm text-secondary">{{ $assignmentSubmission->score }}</h5>
							</span>
						</div>
					</div>
					
					<div class="flex items-end justify-between mt-8">
						<button type="button" class="flex flex-nowrap items-center justify-center my-2 p-1 bg-primary rounded-full transition duration-300 hover:ring hover:ring-primary-200/70 hover:shadow-md">
							<span class="material-symbols-outlined text-white">
								navigate_before
							</span>
							
							<span class="text-white mr-2">Prev</span>
						</button>
						
						<button type="button" class="flex flex-nowrap items-center justify-center my-2 p-1 bg-primary rounded-full transition duration-300 hover:ring hover:ring-primary-200/70 hover:shadow-md">
							<span class="text-white ml-2">Next</span>
							
							<span class="material-symbols-outlined text-white">
								navigate_next
							</span>
						</button>
					</div>	
				</div>
				{{-- Header Page End --}}
				
				{{-- Main Page Start --}}
				<div class="mx-auto my-8 max-w-lg xl:max-w-xl 2xl:max-w-2xl">
					<ol class="list-decimal space-y-36">
						<li class="space-y-8">
							<h4 class="text-dark text-justify">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum quae repudiandae ipsam, totam debitis impedit nisi ut maxime officia ullam provident! Laborum nobis non reprehenderit excepturi, ducimus quod nesciunt nisi fugiat ut.</h4>
							
							<div class="block my-4 space-y-4">
								<span class="p-4 text-dark text-center float-right">
									<input type="text" x-on:click.stop class="w-10 border-0 border-b-2 border-secondary text-center p-1 focus:ring-0 focus:border-primary">
									<h5 class="inline"> / 25</h5>
								</span>
								
								<div class="p-4 w-full max-h-64 overflow-y-auto scrollbar text-secondary shadow-lg rounded-lg border border-slate-100">
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem esse natus, sunt fuga enim facere impedit vero odio molestiae accusantium voluptas alias excepturi earum deleniti, quos, nemo quae et consequuntur quo assumenda? Debitis ut vitae eaque tempora. Explicabo ut commodi temporibus fugiat similique quasi dicta. Harum dolor iure reprehenderit. Quos illum, facilis qui fuga eos sed similique et atque eaque officiis tempore nobis praesentium dolore quae exercitationem, a fugit. Fugit tenetur aspernatur exercitationem aut? Optio tempora soluta ipsum neque natus, sed sunt officia obcaecati. Ipsa eum voluptates ducimus ratione enim voluptate assumenda laudantium nulla magni, cum rem maiores necessitatibus deleniti.
								</div>
							</div>
						</li>
						
						<li class="space-y-8">
							<h4 class="text-dark text-justify">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum quae repudiandae ipsam, totam debitis impedit nisi ut maxime officia ullam provident! Laborum nobis non reprehenderit excepturi, ducimus quod nesciunt nisi fugiat ut.</h4>
							
							<div class="block my-4 space-y-4">
								<span class="p-4 text-dark text-center float-right">
									<input type="text" x-on:click.stop class="w-10 border-0 border-b-2 border-secondary text-center p-1 focus:ring-0 focus:border-primary">
									<h5 class="inline"> / 25</h5>
								</span>
								
								<div class="p-4 w-full max-h-64 overflow-y-auto scrollbar text-secondary shadow-lg rounded-lg border border-slate-100">
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem esse natus, sunt fuga enim facere impedit vero odio molestiae accusantium voluptas alias excepturi earum deleniti, quos, nemo quae et consequuntur quo assumenda? Debitis ut vitae eaque tempora. Explicabo ut commodi temporibus fugiat similique quasi dicta. Harum dolor iure reprehenderit. Quos illum, facilis qui fuga eos sed similique et atque eaque officiis tempore nobis praesentium dolore quae exercitationem, a fugit. Fugit tenetur aspernatur exercitationem aut? Optio tempora soluta ipsum neque natus, sed sunt officia obcaecati. Ipsa eum voluptates ducimus ratione enim voluptate assumenda laudantium nulla magni, cum rem maiores necessitatibus deleniti.
								</div>
							</div>
						</li>
						
						<li class="space-y-8">
							<h4 class="text-dark text-justify">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum quae repudiandae ipsam, totam debitis impedit nisi ut maxime officia ullam provident! Laborum nobis non reprehenderit excepturi, ducimus quod nesciunt nisi fugiat ut.</h4>
							
							<div class="block my-4 space-y-4">
								<span class="p-4 text-dark text-center float-right">
									<input type="text" x-on:click.stop class="w-10 border-0 border-b-2 border-secondary text-center p-1 focus:ring-0 focus:border-primary">
									<h5 class="inline"> / 25</h5>
								</span>
								
								<div class="p-4 w-full max-h-64 overflow-y-auto scrollbar text-secondary shadow-lg rounded-lg border border-slate-100">
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem esse natus, sunt fuga enim facere impedit vero odio molestiae accusantium voluptas alias excepturi earum deleniti, quos, nemo quae et consequuntur quo assumenda? Debitis ut vitae eaque tempora. Explicabo ut commodi temporibus fugiat similique quasi dicta. Harum dolor iure reprehenderit. Quos illum, facilis qui fuga eos sed similique et atque eaque officiis tempore nobis praesentium dolore quae exercitationem, a fugit. Fugit tenetur aspernatur exercitationem aut? Optio tempora soluta ipsum neque natus, sed sunt officia obcaecati. Ipsa eum voluptates ducimus ratione enim voluptate assumenda laudantium nulla magni, cum rem maiores necessitatibus deleniti.
								</div>
							</div>
						</li>
						
						<li class="space-y-8">
							<h4 class="text-dark text-justify">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum quae repudiandae ipsam, totam debitis impedit nisi ut maxime officia ullam provident! Laborum nobis non reprehenderit excepturi, ducimus quod nesciunt nisi fugiat ut.</h4>
							
							<div class="block my-4 space-y-4">
								<span class="p-4 text-dark text-center float-right">
									<input type="text" x-on:click.stop class="w-10 border-0 border-b-2 border-secondary text-center p-1 focus:ring-0 focus:border-primary">
									<h5 class="inline"> / 25</h5>
								</span>
								
								<div class="p-4 w-full max-h-64 overflow-y-auto scrollbar text-secondary shadow-lg rounded-lg border border-slate-100">
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem esse natus, sunt fuga enim facere impedit vero odio molestiae accusantium voluptas alias excepturi earum deleniti, quos, nemo quae et consequuntur quo assumenda? Debitis ut vitae eaque tempora. Explicabo ut commodi temporibus fugiat similique quasi dicta. Harum dolor iure reprehenderit. Quos illum, facilis qui fuga eos sed similique et atque eaque officiis tempore nobis praesentium dolore quae exercitationem, a fugit. Fugit tenetur aspernatur exercitationem aut? Optio tempora soluta ipsum neque natus, sed sunt officia obcaecati. Ipsa eum voluptates ducimus ratione enim voluptate assumenda laudantium nulla magni, cum rem maiores necessitatibus deleniti.
								</div>
							</div>
						</li>
					</ol>
				</div>
				{{-- Main Page End --}}
				
				{{-- Footer Page Start --}}
				<div class="flex items-end justify-between mt-8">
					<button type="button" class="flex flex-nowrap items-center justify-center my-2 p-1 bg-primary rounded-full transition duration-300 hover:ring hover:ring-primary-200/70 hover:shadow-md">
						<span class="material-symbols-outlined text-white">
							navigate_before
						</span>
						
						<span class="text-white mr-2">Prev</span>
					</button>
					
					<button type="button" class="flex flex-nowrap items-center justify-center my-2 p-1 bg-primary rounded-full transition duration-300 hover:ring hover:ring-primary-200/70 hover:shadow-md">
						<span class="text-white ml-2">Next</span>
						
						<span class="material-symbols-outlined text-white">
							navigate_next
						</span>
					</button>
				</div>
				{{-- Footer Page End --}}
			</div>
		</div>
	</div>
@endsection