<?php

use App\Models\AssignmentQuestion;
use App\Models\AssignmentSubmission;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submission_answers', function (Blueprint $table) {
            $table->id();
			$table->foreignIdFor(AssignmentSubmission::class)->nullable();
			$table->foreignIdFor(AssignmentQuestion::class)->nullable();
			$table->longText('answer')->nullable();
			$table->float('score')->nullable();
			$table->foreign('assignment_submission_id')->references('id')->on('assignment_submissions')->onDelete('cascade');
			$table->foreign('assignment_question_id')->references('id')->on('assignment_questions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submission_answers');
    }
};
