<div x-show="open == 'assignments'" x-transition class="block">
  <div class="block my-4 rounded-lg shadow-md overflow-x-auto scrollbar">
    <table class="w-full sm:table-fixed">
      <thead class="bg-slate-100 text-xs text-dark text-center uppercase">
        <tr>
          <th class="px-4 py-3 w-1/3 md:1/5 lg:w-full xl:w-auto">Points</th>
          <th class="px-4 py-3 w-full xl:w-1/3 2xl:w-5/12">Assignment</th>
          <th class="hidden px-4 py-3 w-1/3 lg:w-full md:table-cell xl:w-auto">Duration<sub>/mins</sub></th>
          <th class="hidden px-4 py-3 w-full lg:table-cell xl:w-auto">Status</th>
          <th class="hidden px-4 py-3 w-full lg:table-cell xl:w-auto">Actions</th>
        </tr>
      </thead>

      @foreach ($assignments as $index => $assignment)
      <tbody wire:key="assignment-{{ $assignment->id }}" x-data="{expand: false}" x-on:click="expand = !expand" class="text-secondary text-left border-b">
        <tr>
          <td class="px-4 py-4 text-dark text-center">
            <input type="text" wire:model.lazy="assignments.{{ $index }}.score" x-on:click.stop class="w-10 border-0 border-b-2 border-secondary text-center p-1 focus:ring-0 focus:border-primary">
            <span class="hidden sm:inline"> / 100</span>
          </td>
          <td class="px-4 py-4">
            <div class="flex flex-nowrap items-center justify-between space-x-8 sm:space-x-4">
              <h4 class="font-medium text-dark text-md">{{ $assignment->assignment->name }}</h4>

              <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined transition duration-300 hover:text-primary md:hidden">
                expand_more
              </span>
            </div>
          </td>
          <td class="hidden px-4 py-4 text-center md:table-cell">
            <div class="block">
              <input type="text" wire:model.lazy="assignments.{{ $index }}.duration" x-on:click.stop class="w-10 border-0 border-b-2 border-secondary text-center p-1 focus:ring-0 focus:border-primary">

              <span x-bind:class="{'text-primary rotate-180': expand}" class="material-symbols-outlined hidden transition duration-300 float-right hover:text-primary md:inline lg:hidden">
                expand_more
              </span>
            </div>
          </td>
          <td class="hidden px-4 py-4 text-center lg:table-cell">
            <button type="button" wire:click="$set('assignments.{{ $index }}.status', {{ !$assignments[$index]->status }})" x-on:click.stop class="h-4 w-8 bg-slate-100 border rounded-full py-px px-px {{ $assignment->status ? 'border-primary/80' : 'border-secondary/80' }}">
              <div class="my-auto h-3 w-3 shadow-sm rounded-full transition duration-300 {{ $assignment->status ? 'translate-x-4 bg-primary/50' : 'bg-secondary/50' }}"></div>
            </button>

            <h5 class="{{ $assignment->status ? 'text-primary' : 'text-secondary' }}">{{ $assignment->status ? 'Opened' : 'Closed' }}</h5>
          </td>
          <td class="hidden px-4 py-4 text-center lg:table-cell">
            <div x-on:click.stop class="flex flex-nowrap items-center justify-center space-x-1 py-1">
              <a href="{{ route('creator.manage-courses.course.assignment.show', ['course' => $course->id, 'assignment' => $assignment->id]) }}" title="Submission" class="flex flex-nowrap items-center p-1 bg-primary rounded-md hover:opacity-80">
                <span class="material-symbols-outlined text-white">
                  task
                </span>
              </a>

              <a href="{{ route('creator.manage-courses.material.show', ['material' => $course->material->id]) }}" title="Edit" class="flex flex-nowrap items-center p-1 bg-orange-500 rounded-md hover:opacity-80">
                <span class="material-symbols-outlined text-white">
                  edit
                </span>
              </a>
            </div>

            <div class="flex flex-nowrap items-center justify-center space-x-1 py-1">
              <span class="material-symbols-outlined font-medium text-red-500">
                info
              </span>

              <h5 class="font-medium text-xs text-red-500 whitespace-nowrap">999 Unscored</h5>
            </div>
          </td>
        </tr>

        <tr x-show="expand" x-transition class="lg:hidden">
          <td colspan="5">
            <div class="grid grid-cols-2 gap-4 p-4 sm:grid-cols-4 md:hidden">
              <h4 class="font-medium text-dark">Duration</h4>
              <span class="flex flex-nowrap space-x-2">
                <input type="text" wire:model.lazy="assignments.{{ $index }}.duration" x-on:click.stop class="w-10 border-0 border-b-2 border-secondary text-center p-0 focus:ring-0 focus:border-primary">
                <span class="inline"> / MINS</span>
              </span>
            </div>

            <div class="grid grid-cols-2 gap-4 p-4 sm:grid-cols-4">
              <h4 class="font-medium text-dark">Status</h4>
              <button type="button" wire:click="$set('assignments.{{ $index }}.status', {{ !$assignments[$index]->status }})" x-on:click.stop class="h-4 w-8 bg-slate-100 border rounded-full py-px px-px {{ $assignment->status ? 'border-primary/80' : 'border-secondary/80' }}">
                <div class="my-auto h-3 w-3 shadow-sm rounded-full transition duration-300 {{ $assignment->status ? 'translate-x-4 bg-primary/50' : 'bg-secondary/50' }}"></div>
              </button>
            </div>

            <div class="flex flex-wrap items-center p-2">
              <a href="{{ route('creator.manage-courses.course.assignment.show', ['course' => $course->id, 'assignment' => $assignment->id]) }}" title="Submission" x-on:click.stop class="flex flex-nowrap items-center justify-center mx-2 my-1 p-1 w-32 space-x-1 bg-primary rounded-md hover:opacity-80 hover:shadow-md hover:ring hover:ring-primary/70">
                <span class="material-symbols-outlined text-white">
                  task
                </span>

                <span class="text-white">
                  Submission
                </span>
              </a>

              <a href="{{ route('creator.manage-courses.material.show', ['material' => $course->material->id]) }}" title="Edit" x-on:click.stop class="flex flex-nowrap items-center justify-center mx-2 my-1 p-1 w-32 space-x-1 bg-orange-500 rounded-md text-center hover:opacity-80 hover:shadow-md hover:ring hover:ring-orange-500/70">
                <span class="material-symbols-outlined text-white">
                  edit
                </span>

                <span class="text-white">
                  Edit
                </span>
              </a>

              <div class="flex flex-nowrap items-center justify-center space-x-1 mx-2 my-1">
                <span class="material-symbols-outlined font-medium text-red-500">
                  info
                </span>

                <h5 class="font-medium text-xs text-red-500 whitespace-nowrap">999 Unscored</h5>
              </div>
            </div>
          </td>
        </tr>
      </tbody>
      @endforeach
    </table>
  </div>
</div>