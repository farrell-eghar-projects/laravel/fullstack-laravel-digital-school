<div x-show="floatMaterial" x-on:click="floatMaterial = false" class="fixed inset-0 bg-secondary/70 animate-fadeIn z-10">
	<div x-on:click.stop class="mx-auto bg-white w-3/4 p-8 space-y-12 rounded-md shadow-2xl translate-y-1/2 z-20 md:w-1/2">
		<span x-on:click="floatMaterial = false" class="material-symbols-outlined block float-right text-dark">
			close
		</span>
		
		<div class="space-y-8">
			<div class="space-y-2">
				<label for="name" class="block font-medium text-dark">Material Name</label>
				<input type="text" id="name" wire:model="name" autocomplete="off" class="default-input block w-full">
				@error('name')
					<span class="font-medium text-xs text-red-400">{{ $message }}</span>
				@enderror
			</div>
			
			<div class="flex items-center justify-end">
				<button type="button" wire:click="save" class="bg-primary text-white px-4 py-2 rounded-md float-right hover:opacity-70">Save</button>
			</div>
		</div>
	</div>
</div>