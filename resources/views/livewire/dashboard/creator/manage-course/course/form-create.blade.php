<form action="{{ route('creator.manage-courses.course.update', ['course' => $course->id]) }}" method="POST" class="block mx-auto py-8 max-w-3xl">
	@method('PUT')
	@csrf
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-1 py-4 lg:grid-cols-7">
		<label for="title" class="lg:col-span-2">Course Title</label>
		<input type="text" id="title" wire:model.lazy="course.title" placeholder="Easy English Interview with Native Speaker" class="border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-1 py-4 lg:grid-cols-7">
		<label for="code" class="lg:col-span-2">Course ID</label>
		<input type="text" id="code" wire:model.lazy="course.code" placeholder="speaker123" class="border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-1 py-4 lg:grid-cols-7">
		<label for="password" class="lg:col-span-2">Course Password</label>
		<input type="text" id="password" wire:model.lazy="course.password" placeholder="abc213" class="border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
		<label for="student_target" class="lg:col-span-2">Student Target</label>
		<div class="flex flex-wrap items-center lg:col-span-5">
			<span class="basis-1/2 flex flex-nowrap items-center space-x-1 py-1 sm:py-1 sm:basis-1/3">
				<input type="checkbox" wire:model.defer="studentTargets" id="highschool" value="Highschool Student" class="default-input-checkbox">
				<label for="highschool">Highschool</label>
			</span>
			
			<span class="basis-1/2 flex flex-nowrap items-center space-x-1 py-1 sm:py-1 sm:basis-1/3">
				<input type="checkbox" wire:model.defer="studentTargets" id="college" value="College Student" class="default-input-checkbox">
				<label for="college">College</label>
			</span>
			
			<span class="basis-1/2 flex flex-nowrap items-center space-x-1 py-1 sm:py-1 sm:basis-1/3">
				<input type="checkbox" wire:model.defer="studentTargets" id="employee" value="Employee" class="default-input-checkbox">
				<label for="employee">Employee</label>
			</span>
		</div>
	</div>
	
  <div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
    <label for="suitable_target" class="lg:col-span-2">Suitable Target</label>
    <div class="space-y-2 lg:col-span-5">
      @foreach ($suitableTargets as $index => $suitableTarget)
        <div wire:key="course-courseSuitableTarget-field-{{ $suitableTarget->id }}" class="space-y-2">
          <div class="flex flex-nowrap items-center space-x-2">
            <input type="text" id="suitable_target{{ $index }}" wire:model.lazy="suitableTargets.{{ $index }}.target" placeholder="Those who prepare to work on international environment" class="basis-11/12 border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md">
            
            @if ($loop->count > 1)
              <button type="button" wire:click.prevent="deleteSuitableTarget({{ $suitableTarget->id }})" class="flex items-center p-0 bg-red-500 rounded-full hover:opacity-70">
                <span class="material-symbols-outlined text-white text-center rounded-full">
                  cancel
                </span>
              </button>
            @else
              <button type="button" wire:click.prevent="deleteSuitableTarget({{ $suitableTarget->id }})" disabled class="flex items-center p-0 bg-slate-200 rounded-full hover:opacity-70">
                <span class="material-symbols-outlined text-white text-center rounded-full">
                  cancel
                </span>
              </button>	
            @endif
          </div>
          
          @if ($loop->last)
            <div class="flex flex-nowrap items-center space-x-4">
              <button type="button" wire:click="addSuitableTarget" class="my-auto flex items-center px-4 py-1 bg-green-400 rounded-full hover:opacity-70">
                <span class="material-symbols-outlined text-white text-center rounded-full">
                  add
                </span>
              </button>
            </div>
          @endif
        </div>
      @endforeach
    </div>
  </div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
		<label for="prerequisite" class="lg:col-span-2">Knowledge Prerequisite</label>
		<div class="space-y-2 lg:col-span-5">
			@foreach ($prerequisites as $index => $prerequisite)
				<div wire:key="course-coursePrerequisite-field-{{ $prerequisite->id }}" class="space-y-2">
					<div class="flex flex-nowrap items-center space-x-2">
						<input type="text" id="prerequisite{{ $index }}" wire:model.lazy="prerequisites.{{ $index }}.prerequisite" placeholder="Student should be able to speak" class="basis-11/12 border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
						
						@if ($loop->count > 1)
							<button type="button" wire:click.prevent="deletePrerequisite({{ $prerequisite->id }})" class="flex items-center p-0 bg-red-500 rounded-full hover:opacity-70">
								<span class="material-symbols-outlined text-white text-center rounded-full">
									cancel
								</span>
							</button>
						@else
							<button type="button" wire:click.prevent="deletePrerequisite({{ $prerequisite->id }})" disabled class="flex items-center p-0 bg-slate-200 rounded-full hover:opacity-70">
								<span class="material-symbols-outlined text-white text-center rounded-full">
									cancel
								</span>
							</button>	
						@endif
					</div>
					
					@if ($loop->last)
						<div class="flex flex-nowrap items-center space-x-4">
							<button type="button" wire:click="addPrerequisite" class="my-auto flex items-center px-4 py-1 bg-green-400 rounded-full hover:opacity-70">
								<span class="material-symbols-outlined text-white text-center rounded-full">
									add
								</span>
							</button>
						</div>
					@endif
				</div>
			@endforeach
		</div>
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
		<label for="type" class="lg:col-span-2">Course Type</label>
		<select wire:model.lazy="course.type_id" id="type" class="border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
      <option hidden><-- Select course type --></option>	
      <option value="1">Intensive Class</option>
      <option value="2">Lifetime Class</option>
      <option value="3">Subscription Class</option>
		</select>
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
		<label for="started_at" class="lg:col-span-2">Started At</label>
		<input type="date" wire:model.lazy="course.started_at" id="started_at" class="border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
		<label for="finished_at" class="lg:col-span-2">Finished At</label>
		<input type="date" wire:model.lazy="course.finished_at" id="finished_at" class="border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
		<label for="material" class="lg:col-span-2">Course Material</label>
		<select wire:model.lazy="course.material_id" id="material" class="border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5">
			<option hidden><-- Choose one material --></option>
			@foreach ($materials as $index => $material)
				<option value="{{ $material->id }}">{{ $material->name }}</option>
			@endforeach
		</select>
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7" wire:ignore>
		<label for="description" class="lg:col-span-2">Description</label>
		<div name="description" id="description" class="default-editor overflow-y-auto h-56 p-4 border border-secondary rounded-lg focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md lg:col-span-5"></div>
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
		<label for="course_thumbnail" class="lg:col-span-2">Course Thumbnail</label>
		<div class="block w-full lg:col-span-5">
			<div class="relative w-full">
				<a wire:ignore id="lfm" data-input="course_thumbnail" data-preview="holder" class="absolute left-0 top-1/2 -translate-y-1/2 flex cursor-pointer p-2 space-x-1 border border-primary rounded-lg bg-primary text-white hover:ring-primary hover:ring-1 hover:border-primary hover:shadow-md">
					<span class="material-symbols-outlined">
						add_photo_alternate
					</span>
					
					<span class="font-semibold">
						Choose
					</span>
				</a>
				<input id="course_thumbnail" type="text" wire:model="course.thumbnail" disabled placeholder="Input one image only here" class="w-full pl-28 border border-secondary rounded-lg bg-slate-200 focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md">
			</div>
			<div wire:ignore id="holder" class="flex flex-wrap mt-4 max-h-24"></div>
		</div>
	</div>
	
	<div class="grid grid-cols-1 gap-x-4 gap-y-2 py-2 lg:grid-cols-7">
		<label for="course_images" class="lg:col-span-2">Course Preview</label>
		<div class="block w-full lg:col-span-5">
			<div class="relative w-full">
				<a wire:ignore id="lfm_multiple" data-input="course_preview" data-preview="holder2" class="absolute left-0 top-1/2 -translate-y-1/2 flex cursor-pointer p-2 space-x-1 border border-primary rounded-lg bg-primary text-white hover:outline-none hover:ring-primary hover:ring-1 hover:border-primary hover:shadow-md">
					<span class="material-symbols-outlined">
						add_photo_alternate
					</span>
					
					<span class="font-semibold">
						Choose
					</span>
				</a>
				<input id="course_preview" type="text" wire:model="course.images" placeholder="Input multiple images here" class="w-full pl-28 border border-secondary rounded-lg bg-slate-200 focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary focus:shadow-md">
			</div>
			<div wire:ignore id="holder2" class="flex flex-wrap mt-4 max-h-24"></div>
		</div>
	</div>
	
	<div class="flex items-center justify-between py-4">
		<button type="button" wire:click="deleteCourse" class="flex items-center bg-red-500 px-4 py-2 rounded-lg transition hover:opacity-80 active:ring active:ring-red-400">
			<span class="material-symbols-outlined text-white">
				delete
			</span>
		</button>
		
		<div class="flex flex-nowrap justify-center space-x-4">
			<button type="button" class="flex items-center bg-yellow-500 px-4 py-2 space-x-1 rounded-lg transition hover:opacity-80 active:ring active:ring-yellow-400">
				<span class="material-symbols-outlined text-white">
					save
				</span>
				
				<span class="text-white">
					Save
				</span>
			</button>
			
			<button type="button" wire:click="publishCourse" class="flex items-center bg-green-500 px-4 py-2 space-x-1 rounded-lg transition hover:opacity-80 active:ring active:ring-green-400">
				<span class="material-symbols-outlined text-white">
					language
				</span>
				
				<span class="text-white">
					Publish
				</span>
			</button>
		</div>
	</div>
	@push('script-customize')
		<script>
			var route_prefix = "/filemanager";
			
			var editor_config = {
				path_absolute : "/",
				selector: 'div.default-editor',
				relative_urls: false,
				resize: 'both',
				min_height: 500,
				min_width: 300,
				width: 300,
				menubar: false,
				inline: true,
				plugins: [
					'advlist', 'autoresize', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview', 'pagebreak',
					'anchor', 'searchreplace', 'visualblocks', 'visualchars', 'code', 'fullscreen', 'quickbars ', 'codesample',
					'insertdatetime', 'media', 'nonbreaking', 'save', 'table', 'help', 'wordcount', 'directionality',
					'emoticons',  'template'
				],
				codesample_languages: [
					{text: 'HTML/XML', value: 'markup'},
					{text: 'JavaScript', value: 'javascript'},
					{text: 'CSS', value: 'css'},
					{text: 'PHP', value: 'php'},
					{text: 'Ruby', value: 'ruby'},
					{text: 'Python', value: 'python'},
					{text: 'Java', value: 'java'},
					{text: 'C', value: 'c'},
					{text: 'C#', value: 'csharp'},
					{text: 'C++', value: 'cpp'}
				],
				toolbar: 'undo redo codesample code preview | styleselect blocks removeformat emoticons | ' +
					'bold italic backcolor table | alignleft aligncenter ' +
					'alignright alignjustify | bullist numlist outdent indent | ' +
					'link image media help',
				content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
				contextmenu: 'undo redo | inserttable | cell row column deletetable | help',
				quickbars_insert_toolbar: false,
				quickbars_selection_toolbar: 'bold italic underline | styleselect blocks | bullist numlist | backcolor quicklink',
				quickbars_image_toolbar: 'alignleft aligncenter alignright | rotateleft rotateright | imageoptions',
				powerpaste_word_import: 'clean',
				powerpaste_html_import: 'clean',
				
				setup: function (editor) {
					window.addEventListener('fetch-content', event => {
						editor.setContent(event.detail.content)
					})
					
					editor.on('init', function () {
            Livewire.emit('setContent');
						editor.save();
					});

					editor.on('change', function () {
						editor.save();
					});
					
					editor.on('blur', function (e) {
						Livewire.emit('contentUpdate', editor.getContent());
					});
				},
				
				file_picker_callback : function(callback, value, meta) {
					var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
					var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
	
					var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
					if (meta.filetype == 'image') {
						cmsURL = cmsURL + "&type=Images";
					} else {
						cmsURL = cmsURL + "&type=Files";
					}
	
					tinyMCE.activeEditor.windowManager.openUrl({
						url : cmsURL,
						title : 'Filemanager',
						width : x * 0.8,
						height : y * 0.8,
						resizable : "yes",
						close_previous : "no",
						onMessage: (api, message) => {
						callback(message.content);
						}
					});
				}
			};
			
			var lfm = function(part, id, type, options) {
				let button = document.getElementById(id);

        document.addEventListener("DOMContentLoaded", () => {
          Livewire.emit('setImage', part)
          console.log(part)
        });
        
        window.addEventListener('fetch-image', event => {
          if (part && event.detail.thumbnail) {
            let target_preview = document.getElementById('holder');
            let target_input = document.getElementById('course_thumbnail').value;
  
            if (target_input != '') {
              let items = target_input.split(',');
              let img = document.createElement('img')
              img.setAttribute('style', 'height: 5rem; margin: 4px;')
              img.setAttribute('src', items[0])
              target_preview.appendChild(img);
            }
            console.log(event.detail.thumbnail)
          } else if (!part && event.detail.images) {
            let target_preview = document.getElementById('holder2');
            let target_input = document.getElementById('course_preview').value;
  
            if (target_input != '') {
              let items = target_input.split(',');
    
              items.forEach(function (item) {
                let img = document.createElement('img')
                img.setAttribute('style', 'height: 5rem; margin: 4px;')
                img.setAttribute('src', item)
                target_preview.appendChild(img);
              })
            }
            console.log(event.detail.images)
          }
				})

				button.addEventListener('click', function () {
					var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
					var target_input = document.getElementById(button.getAttribute('data-input'));
					var target_preview = document.getElementById(button.getAttribute('data-preview'));

					window.open(route_prefix + '?type=' + type || 'file', 'FileManager', 'width=900,height=600');
					window.SetUrl = function (items) {
						var file_path = items.map(function (item) {
							return item.url;
						}).join(',');

						// set the value of the desired input to image url
						target_input.value = file_path;
						target_input.dispatchEvent(new Event('change'));
						Livewire.emit('imageUpdate', [part, file_path]);

						// clear previous preview
						while (target_preview.firstChild) {
							target_preview.removeChild(target_preview.firstChild)
						}

						// set or change the preview image src
						items.forEach(function (item) {
							let img = document.createElement('img')
							img.setAttribute('style', 'height: 5rem; margin: 4px;')
							img.setAttribute('src', item.thumb_url)
							console.log(item.thumb_url);
							console.log(item.url);
							console.log(item);
							target_preview.appendChild(img);
						});

						// trigger change event
						target_preview.dispatchEvent(new Event('change'));
					};
				});
			};
			
			tinymce.init(editor_config);
			
			lfm(true, 'lfm', 'image', {prefix: route_prefix});
			lfm(false, 'lfm_multiple', 'image', {prefix: route_prefix});
		</script>
	@endpush
</form>