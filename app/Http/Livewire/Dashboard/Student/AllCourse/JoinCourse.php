<?php

namespace App\Http\Livewire\Dashboard\Student\AllCourse;

use App\Models\Course;
use App\Models\MyCourse;
use Livewire\Component;

class JoinCourse extends Component
{
  public Course $course;
  public $code;
  public $password;
  public $attr = 'hidden';

  public function save()
  {
    if ($this->code != $this->course->code || $this->password != $this->course->password) {
      $this->attr = '';
    } else {
      $this->attr = 'hidden';
      MyCourse::create([
        'user_id' => auth()->id(),
        'course_id' => $this->course->id
      ]);

      return redirect()->route('student.my-courses.index');
    }
  }

    public function render()
    {
        return view('livewire.dashboard.student.all-course.join-course');
    }
}
