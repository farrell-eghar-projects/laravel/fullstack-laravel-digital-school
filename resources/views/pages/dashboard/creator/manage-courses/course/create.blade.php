@extends('layouts.master-dashboard')

@section('title', 'Create Course')

@section('content')
	@push('css')
		<link rel="stylesheet" type="text/css" href="{{ asset('css/prism.css') }}">
	@endpush
	
	{{-- Breadcrumb Start --}}
	@livewire('dashboard.components.breadcrumb', ['routes' => [
			[
				'link' => route('creator.manage-courses.index'),
				'name' => 'Manage Courses'
			],
			[
				'link' => route('creator.manage-courses.course.create', $course),
				'name' => 'Create Course'
			]
		]
	])
	{{-- Breadcrumb End --}}
	
	{{-- Create Course Form Start --}}
	<div class="lg:px-32">
		<div class="container">
			<div class="block bg-white px-4 py-1 my-2 rounded-md">
				<div class="block p-4">
					<h3 class="font-semibold text-lg text-dark text-center p-4 md:text-2xl">Create Course</h3>
				</div>
				
				<div class="block w-full">
          <form action="{{ route('creator.manage-courses.course.destroy', ['course' => $course->id]) }}" method="POST" class="w-full flex justify-end">
            @csrf
            @method('DELETE')

            <button type="submit" class="material-symbols-outlined text-lg text-secondary text-right hover:text-dark">
              delete
            </button>
          </form>
				</div>
				
				<div class="block w-full border-t border-slate-200"></div>
				
				@livewire('dashboard.creator.manage-course.course.form-create', ['course' => $course])
			</div>
		</div>
	</div>
	{{-- Create Course Form Start --}}
	
	@push('scripts')
		<script src="{{ asset('js/prism.js') }}"></script>
		<script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
	@endpush
@endsection